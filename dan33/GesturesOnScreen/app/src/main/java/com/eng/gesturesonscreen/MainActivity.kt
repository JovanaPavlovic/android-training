package com.eng.gesturesonscreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Toast
import java.lang.Math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    lateinit var gestureDetector: GestureDetector
    var x1: Float = 0.0f
    var x2: Float = 0.0f
    var y1: Float = 0.0f
    var y2: Float = 0.0f

    val min_distanca = 150

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gestureDetector = GestureDetector(this, this)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event!!.action) {
            //kada pocne pokret
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            //kada se zavrsi pokret
            1 -> {
                x2 = event.x
                y2 = event.y

                val vrijednostX: Float = x2-x1
                val vrijednostY: Float = y2-y1

                if(abs(vrijednostX) > min_distanca) {
                    if (x2>x1) {
                        Toast.makeText(this, "Dogodio se swipe desno", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Dogodio se swipe lijevo", Toast.LENGTH_SHORT).show()
                    }
                } else if(abs(vrijednostY) > min_distanca) {
                    if (y2>y1) {
                        Toast.makeText(this, "Dogodio se swipe dole", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Dogodio se swipe gore", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        return gestureDetector.onTouchEvent(event)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        //Toast.makeText(this, "Dogodio se OnDown", Toast.LENGTH_SHORT).show()
        return false
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        //Toast.makeText(this, "Dogodio se SingleTap", Toast.LENGTH_SHORT).show()
        return false    //ako hocemo da reaguje, moramo vratiti true
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        Toast.makeText(this, "Dogodio se LonPress", Toast.LENGTH_LONG).show()
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        return false
    }
}