package com.eng.automijenjanjeslika

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    var nizSlika: ArrayList<Int> = arrayListOf()
    var brojacSlike = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nizSlika.add(R.drawable.im1)
        nizSlika.add(R.drawable.im2)
        nizSlika.add(R.drawable.im3)
        nizSlika.add(R.drawable.im4)

        prikaziSliku()
    }

    fun prikaziSliku() {
        Handler(Looper.getMainLooper()).postDelayed({
            val ispis = findViewById<ImageView>(R.id.ispisSlike)
            ispis.setImageResource(mijenjajSliku())
            prikaziSliku()
        }, 2500)
    }

    fun mijenjajSliku() : Int {
        if (brojacSlike == nizSlika.count()) brojacSlike = 0
        
        val image = nizSlika[brojacSlike]
        brojacSlike++
        return image
    }
}