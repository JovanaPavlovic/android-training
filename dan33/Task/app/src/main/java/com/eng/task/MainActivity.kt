package com.eng.task

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import java.lang.Math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    lateinit var gestureDetector: GestureDetector
    lateinit var image: ImageView

    var x1: Float = 0.0f
    var x2: Float = 0.0f
    var y1: Float = 0.0f
    var y2: Float = 0.0f

    val min_distance = 150

    val imageArray: ArrayList<Int> = arrayListOf()

    var brojac = 0

    val colorArray: ArrayList<String> = arrayListOf()

    var swipe = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        colorArray.add("#990000")
        colorArray.add("#c1c1c1")
        colorArray.add("#000000")

        imageArray.add(R.drawable.im1)
        imageArray.add(R.drawable.im2)
        imageArray.add(R.drawable.im3)
        imageArray.add(R.drawable.im4)

        gestureDetector = GestureDetector(this, this)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event!!.action) {
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            1 -> {
                x2 = event.x
                y2 = event.y

                val valueX: Float = x2 - x1
                val valueY: Float = y2 - y1

                if(abs(valueY) > min_distance) {
                    if(y2 < y1) {
                        swipe = true
                        image.setImageResource(imageArray[0])
                    } else {
                        Toast.makeText(this, "Sliku mozemo ubaciti samo kada swipe-ujemo na gore", Toast.LENGTH_SHORT).show()
                        mijenjajBoje()
                    }
                } else if(abs(valueX) > min_distance) {
                    //desno, prethodna slika iz niza
                    if(x2 > x1) {
                        swipe = true

                        brojac--
                        if(brojac == -1) brojac = imageArray.count()-1

                        image.setImageResource(imageArray[brojac])
                    } else {    //lijevo, sledeca slika iz niza
                        swipe = true

                        brojac++
                        if(brojac == imageArray.count()) brojac = 0

                        image.setImageResource(imageArray[brojac])

                    }
                }
            }
        }

        return gestureDetector.onTouchEvent(event)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        val layout = findViewById<LinearLayout>(R.id.linearLayout)
        image = ImageView(this)

        image.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams(1500, 950))
        image.setBackgroundColor(Color.parseColor("#990000").toInt())
        layout.addView(image)
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        return false
    }

    var brojacBoje = 0

    fun mijenjajBoje() {
        if(!swipe) {
            Handler(Looper.getMainLooper()).postDelayed({
                if(brojacBoje == colorArray.count()) brojacBoje = 0
                image.setBackgroundColor(Color.parseColor(colorArray[brojacBoje]))
                brojacBoje++
                mijenjajBoje()
            }, 500)
        }
    }
}