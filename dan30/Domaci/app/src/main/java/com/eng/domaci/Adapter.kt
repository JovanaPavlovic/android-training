package com.eng.domaci

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(val receivedData: ArrayList<Data.Drzava>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val state = holder.view.findViewById<TextView>(R.id.nameTextView)
        state.text = receivedData[position].name + " (" + receivedData[position].iso3166a2 +
                " / " + receivedData[position].iso3166a3 + ")"

        val totalCases = holder.view.findViewById<TextView>(R.id.totalCases)
        totalCases.text = receivedData[position].total_cases.toString()

        val activeCases = holder.view.findViewById<TextView>(R.id.activeCases)
        activeCases.text = receivedData[position].active_cases.toString()

        val deaths = holder.view.findViewById<TextView>(R.id.deaths)
        deaths.text = receivedData[position].deaths.toString()

        val recovered = holder.view.findViewById<TextView>(R.id.recovered)
        recovered.text = receivedData[position].recovered.toString()

        val critical = holder.view.findViewById<TextView>(R.id.critical)
        critical.text = receivedData[position].critical.toString()

        val tested = holder.view.findViewById<TextView>(R.id.tested)
        tested.text = receivedData[position].tested.toString()

        val deathRatio = holder.view.findViewById<TextView>(R.id.deathRatio)
        deathRatio.text = receivedData[position].death_ratio.toString()

        val recoveryRatio = holder.view.findViewById<TextView>(R.id.recoveryRatio)
        recoveryRatio.text = receivedData[position].recovery_ratio.toString()

        val totalCasesChange = holder.view.findViewById<TextView>(R.id.totalCasesChange)
        totalCasesChange.text = receivedData[position].total_cases_change.toString()

        val activeCasesChange = holder.view.findViewById<TextView>(R.id.activeCasesChange)
        activeCasesChange.text = receivedData[position].active_cases_change.toString()

        val deathsChange = holder.view.findViewById<TextView>(R.id.deathsChange)
        deathsChange.text = receivedData[position].deaths_change.toString()

        val recoveredChange = holder.view.findViewById<TextView>(R.id.recoveredChange)
        recoveredChange.text = receivedData[position].recovered_change.toString()

        val deathRatioChange = holder.view.findViewById<TextView>(R.id.deathRatioChange)
        deathRatioChange.text = receivedData[position].death_ratio_change.toString()

        val recoveryRatioChange = holder.view.findViewById<TextView>(R.id.recoveryRatioChange)
        recoveryRatioChange.text = receivedData[position].recovery_ratio_change.toString()
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
