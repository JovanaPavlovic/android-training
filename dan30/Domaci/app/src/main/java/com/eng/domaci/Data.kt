package com.eng.domaci

object Data {
    val functionsClassObject = FunctionsClass()

    data class Drzava(val name: String, val iso3166a2: String,
                      val iso3166a3: String, val iso3166numeric: String,
                      val total_cases: Int, val active_cases: Int,
                      val deaths: Int, val recovered: Int,
                      val critical: Int, val tested: Int,
                      val death_ratio: Double, val recovery_ratio: Double,
                      val total_cases_change: Int, val active_cases_change: Int,
                      val deaths_change: Int, val recovered_change: Int,
                      val death_ratio_change: Double, val recovery_ratio_change: Double)

    data class Corona(val total_cases_summarry: Int, val active_cases_summary: Int,
                      val deaths_summary: Int, val recovered_sumarry: Int,
                      val critical_summary: Int, val tested_summary: Long,
                      val death_ratio_summary: Double, val recovery_ratio_summary: Double,
                      val total_cases_change: Int, val active_cases_change: Int,
                      val deaths_change: Int, val recovered_change: Int,
                      val critical_change: Int, val tested_change: Long,
                      val death_ratio_change: Double, val recovery_ratio_change: Double,
                      val generated_on: Long, val states: ArrayList<Drzava>)

    lateinit var statistic: Corona
}