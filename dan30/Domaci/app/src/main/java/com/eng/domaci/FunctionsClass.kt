package com.eng.domaci

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject

class FunctionsClass {

    fun procitajJSON(preuzetiJSON: String?) {
        val nizDrzava: ArrayList<Data.Drzava> = arrayListOf()
        val jsonObject = JSONObject(preuzetiJSON)

        val status = jsonObject.getInt("status")
        val type = jsonObject.getString("type")

        val data = jsonObject.getJSONObject("data")

        val summary = data.getJSONObject("summary")
        val total_cases_summary = summary.getInt("total_cases")
        val active_cases_summary = summary.getInt("active_cases")
        val deaths_summary = summary.getInt("deaths")
        val recovered_summary = summary.getInt("recovered")
        val critical_summary = summary.getInt("critical")
        val tested_summary = summary.getLong("tested")
        val death_ratio_summary = summary.getDouble("death_ratio")
        val recovery_ratio_summary = summary.getDouble("recovery_ratio")

        val change = data.getJSONObject("change")
        val total_cases_change = change.getInt("total_cases")
        val active_cases_change = change.getInt("active_cases")
        val deaths_change = change.getInt("deaths")
        val recovered_change = change.getInt("recovered")
        val critical_change = change.getInt("critical")
        val tested_change = change.getLong("tested")
        val death_ratio_change = change.getDouble("death_ratio")
        val recovery_ratio_change = change.getDouble("recovery_ratio")

        val generated_on = data.getLong("generated_on")

        val regions = data.getJSONObject("regions")
        for(name in 0 until regions.names().length()) {
            nizDrzava.add(procitajPodatkeZaDrzavu(regions.getJSONObject(regions.names()[name].toString())))
        }

        Data.statistic = Data.Corona(total_cases_summary, active_cases_summary, deaths_summary, recovered_summary,
            critical_summary, tested_summary, death_ratio_summary, recovery_ratio_summary, total_cases_change, active_cases_change,
            deaths_change, recovered_change, critical_change, tested_change, death_ratio_change, recovery_ratio_change,
            generated_on, nizDrzava)
    }

    fun procitajPodatkeZaDrzavu(drzava: JSONObject) : Data.Drzava {
        val name = drzava.getString("name")
        val iso3166a2 = drzava.getString("iso3166a2")
        val iso3166a3 = drzava.getString("iso3166a3")
        val iso3166numeric = drzava.getString("iso3166numeric")
        val total_cases = drzava.getInt("total_cases")
        val active_cases = drzava.getInt("active_cases")
        val deaths = drzava.getInt("deaths")
        val recovered = drzava.getInt("recovered")
        val critical = drzava.getInt("critical")
        val tested = drzava.getInt("tested")
        val death_ratio = drzava.getDouble("death_ratio")
        val recovery_ratio = drzava.getDouble("recovery_ratio")

        val change = drzava.getJSONObject("change")
        val total_cases_change = change.getInt("total_cases")
        val active_cases_change = change.getInt("active_cases")
        val deaths_change = change.getInt("deaths")
        val recovered_change = change.getInt("recovered")
        val death_ratio_change = change.getDouble("death_ratio")
        val recovery_ratio_change = change.getDouble("recovery_ratio")

        return Data.Drzava(name, iso3166a2, iso3166a3, iso3166numeric, total_cases, active_cases,
            deaths, recovered, critical, tested, death_ratio, recovery_ratio, total_cases_change,
            active_cases_change, deaths_change, recovered_change, death_ratio_change, recovery_ratio_change)
    }

    fun fillDataInMainActivity(activity: Activity, context: Context) {
        val totalCasesSummary = activity.findViewById<TextView>(R.id.totalCasesSummary)
        totalCasesSummary.text = Data.statistic.total_cases_summarry.toString()

        val activeCasesSummary = activity.findViewById<TextView>(R.id.activeCasesSummary)
        activeCasesSummary.text = Data.statistic.active_cases_summary.toString()

        val deathsSummary = activity.findViewById<TextView>(R.id.deathsSummary)
        deathsSummary.text = Data.statistic.deaths_summary.toString()

        val recoveredSummary = activity.findViewById<TextView>(R.id.recoveredSummary)
        recoveredSummary.text = Data.statistic.recovered_sumarry.toString()

        val criticalSummary = activity.findViewById<TextView>(R.id.criticalSummary)
        criticalSummary.text = Data.statistic.critical_summary.toString()

        val testedSummary = activity.findViewById<TextView>(R.id.testedSummary)
        testedSummary.text = Data.statistic.tested_summary.toString()

        val deathRatioSummary = activity.findViewById<TextView>(R.id.deathRatioSummary)
        deathRatioSummary.text = Data.statistic.death_ratio_summary.toString()

        val recoveryRatioSummary = activity.findViewById<TextView>(R.id.recoveryRatioSummary)
        recoveryRatioSummary.text = Data.statistic.recovery_ratio_summary.toString()

        val totalCasesChange = activity.findViewById<TextView>(R.id.totalCasesChange)
        totalCasesChange.text = Data.statistic.total_cases_change.toString()

        val activeCasesChange = activity.findViewById<TextView>(R.id.activeCasesChange)
        activeCasesChange.text = Data.statistic.active_cases_change.toString()

        val deathsChange = activity.findViewById<TextView>(R.id.deathsChange)
        deathsChange.text = Data.statistic.deaths_change.toString()

        val recoveredChange = activity.findViewById<TextView>(R.id.recoveredChange)
        recoveredChange.text = Data.statistic.recovered_change.toString()

        val criticalChange = activity.findViewById<TextView>(R.id.criticalChange)
        criticalChange.text = Data.statistic.critical_change.toString()

        val testedChange = activity.findViewById<TextView>(R.id.testedChange)
        testedChange.text = Data.statistic.tested_change.toString()

        val deathRatioChange = activity.findViewById<TextView>(R.id.deathRatioChange)
        deathRatioChange.text = Data.statistic.death_ratio_change.toString()

        val recoveryRatioChange = activity.findViewById<TextView>(R.id.recoveryRatioChange)
        recoveryRatioChange.text = Data.statistic.recovery_ratio_change.toString()

        val generatedOn = activity.findViewById<TextView>(R.id.generatedOnValueTextView)
        generatedOn.text = Data.statistic.generated_on.toString()

        val recyclerView = activity.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = Adapter(Data.statistic.states)
        recyclerView.setLayoutManager(GridLayoutManager(context, 1))

        val search = activity.findViewById<EditText>(R.id.searchEditText)
        val filteredStates: ArrayList<Data.Drzava> = arrayListOf()

        search.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filteredStates.clear()
                for(state in Data.statistic.states) {
                    if(state.name.lowercase().contains(search.text.toString().lowercase())) {
                        filteredStates.add(state)
                    }
                }
                recyclerView.adapter = Adapter(filteredStates)
            }

        })
    }
}