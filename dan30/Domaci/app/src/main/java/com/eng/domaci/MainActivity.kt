package com.eng.domaci

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.json.JSONObject
import java.io.InputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://api.quarantine.country/api/v1/summary/latest"
        preuzmiJSON().execute(jsonURL)
    }

    inner class preuzmiJSON(): AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            val json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
              connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            Data.functionsClassObject.procitajJSON(result)
            callFunc()
        }
    }

    fun callFunc() {
        Data.functionsClassObject.fillDataInMainActivity(this, this)
    }
}