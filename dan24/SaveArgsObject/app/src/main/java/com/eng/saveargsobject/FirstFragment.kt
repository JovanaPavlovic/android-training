package com.eng.saveargsobject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation

class FirstFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_first, container, false)

        val txt = view.findViewById<TextView>(R.id.firstTextView)
        txt.setOnClickListener {
            val proslijedi = FirstFragmentDirections.actionFirstFragmentToSecondFragment(Korisnik("Jovana", "Pavlovic"))
            Navigation.findNavController(view).navigate(proslijedi)
        }

        return view
    }
}