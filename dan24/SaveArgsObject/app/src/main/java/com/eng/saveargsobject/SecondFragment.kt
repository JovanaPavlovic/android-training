package com.eng.saveargsobject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.NavArgs
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs

class SecondFragment : Fragment() {

    val args: SecondFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ispis = view.findViewById<TextView>(R.id.secondTextView)

        ispis.setOnClickListener {
            Navigation.findNavController(view).navigate(SecondFragmentDirections.actionSecondFragmentToFirstFragment())
        }

        val primljeniKorisnik = args.prenijetiKorisnik
        ispis.text = primljeniKorisnik.ime + " " + primljeniKorisnik.prezime
    }
}