package com.eng.domaci

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.bottom_cell.view.*
import kotlinx.android.synthetic.main.main_cell.view.*

class mainAdapter(val receivedNews : ArrayList<News>, val xmlObjekat: Int) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(xmlObjekat, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return receivedNews.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.imageView.setImageResource(receivedNews[position].image)
        holder.view.navTextView.text = receivedNews[position].navTitle
        holder.view.textView.text = receivedNews[position].title
    }
}