package com.eng.domaci

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.bottom_cell.view.*

class bottomAdapter(val receivedNav : ArrayList<Navigation>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.bottom_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return receivedNav.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.bottomImageView.setImageResource(receivedNav[position].image)
        holder.view.naslovTextView.text = receivedNav[position].title

        holder.view.setOnClickListener{
            rowIndex = position
            notifyDataSetChanged()
        }

        if(rowIndex == position) {
            holder.view.bottomCellLayout.setBackgroundResource(R.color.dark_gray)
            holder.view.naslovTextView.setTextColor(Color.parseColor("#FFFFFF"))
            holder.view.bottomImageView.setColorFilter(ContextCompat.getColor(holder.view.context, R.color.white))
        } else {
            holder.view.bottomCellLayout.setBackgroundResource(R.color.light_gray)
            holder.view.naslovTextView.setTextColor(Color.parseColor("#000000"))
            holder.view.bottomImageView.setColorFilter(ContextCompat.getColor(holder.view.context, R.color.black))
        }
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
