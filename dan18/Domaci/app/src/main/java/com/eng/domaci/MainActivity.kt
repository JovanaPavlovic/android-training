package com.eng.domaci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class Navigation(val title: String, val image: Int)
data class News(val navTitle: String, val title:String, val image: Int)

class MainActivity : AppCompatActivity() {

    val items: ArrayList<Navigation> = arrayListOf()
    val newsArray: ArrayList<News> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fillInNavArray()

        val createdBottomAdapter = bottomAdapter(items)
        recyclerViewBottom.adapter = createdBottomAdapter
        recyclerViewBottom.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))

        fillInNewsArray()

        val createdMainAdapter = mainAdapter(newsArray, R.layout.main_cell)
        recyclerViewMain.adapter = createdMainAdapter
        recyclerViewMain.setLayoutManager(GridLayoutManager(this, 1))

        val createdTopAdapter = mainAdapter(newsArray, R.layout.top_cell)
        recyclerViewTop.adapter = createdTopAdapter
        recyclerViewTop.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))
    }

    fun fillInNavArray() {
        items.add(Navigation(getResources().getString(R.string.naslovna), R.drawable.ic_baseline_home_24))
        items.add(Navigation(getResources().getString(R.string.info), R.drawable.ic_baseline_info_24))
        items.add(Navigation(getResources().getString(R.string.najnovije), R.drawable.ic_baseline_fiber_new_24))
        items.add(Navigation(getResources().getString(R.string.sport), R.drawable.ic_baseline_sports_soccer_24))
        items.add(Navigation(getResources().getString(R.string.zdravlje), R.drawable.ic_baseline_health_and_safety_24))
        items.add(Navigation(getResources().getString(R.string.putovanja), R.drawable.ic_baseline_emoji_transportation_24))
        items.add(Navigation(getResources().getString(R.string.statistika), R.drawable.ic_baseline_statistic_24))
        items.add(Navigation(getResources().getString(R.string.galerija), R.drawable.ic_baseline_camera_alt_24))
    }

    fun fillInNewsArray() {
        newsArray.add(News(getResources().getString(R.string.najnovije), getResources().getString(R.string.eurosong), R.drawable.pic1))
        newsArray.add(News(getResources().getString(R.string.sport), getResources().getString(R.string.tenis), R.drawable.pic2))
        newsArray.add(News(getResources().getString(R.string.zdravlje), getResources().getString(R.string.vakcine), R.drawable.pic4))
        newsArray.add(News(getResources().getString(R.string.info), getResources().getString(R.string.zakon), R.drawable.pic3))
        newsArray.add(News(getResources().getString(R.string.sport), getResources().getString(R.string.glavna), R.drawable.pic5))

    }
}