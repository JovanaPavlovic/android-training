package com.eng.katalogautomobila

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AllModelsOfBrandAdapter(val chosenBrandCarArray: ArrayList<GlobalData.ChosenBrandCar>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.all_models_of_brand_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return chosenBrandCarArray.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val image = holder.view.findViewById<ImageView>(R.id.allModelsOfBrandImage)
        val model = holder.view.findViewById<TextView>(R.id.allModelsOfBrandModel)
        val price = holder.view.findViewById<TextView>(R.id.allModelsOfBrandPrice)

        image.setBackgroundResource(chosenBrandCarArray[position].image)
        model.text = chosenBrandCarArray[position].model
        price.text = chosenBrandCarArray[position].price

        holder.view.setOnClickListener {
            val intent = Intent(holder.view.context, CarDetailsActivity::class.java)
            intent.putExtra("IMAGE", chosenBrandCarArray[position].image)
            intent.putExtra("PRICE", chosenBrandCarArray[position].price)
            intent.putExtra("BRAND", chosenBrandCarArray[position].brand)
            intent.putExtra("MODEL", chosenBrandCarArray[position].model)
            intent.putExtra("ENGINE", chosenBrandCarArray[position].engine)
            intent.putExtra("EQUIPMENT PACKAGE", chosenBrandCarArray[position].equipmentPackage)
            intent.putExtra("EMPTY VEHICLE WEIGHT", chosenBrandCarArray[position].emptyVehicleWeight)
            holder.view.context.startActivity(intent)
        }
    }
}