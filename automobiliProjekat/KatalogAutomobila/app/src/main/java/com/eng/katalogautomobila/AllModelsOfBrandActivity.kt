package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AllModelsOfBrandActivity : AppCompatActivity() {

    val chosenBrandCarArray: ArrayList<GlobalData.ChosenBrandCar> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_models_of_brand)

        val logoImageView = findViewById<ImageView>(R.id.allModelsOfBrandLogo)
        logoImageView.setImageResource(intent.getIntExtra("LOGO", 0))

        val carBrand = intent.getStringExtra("BRAND")
        for(car in GlobalData.carArray) {
            if(car.marka == carBrand) {
                val image = GlobalData.chooseCarImage(carBrand)
                chosenBrandCarArray.add(
                    GlobalData.ChosenBrandCar(
                        image, car.model, car.cijena + " $", car.marka,
                        car.motor, car.paketOpereme, car.tezinaPraznogVozila
                    )
                )
            }
        }

        val recyclerView = findViewById<RecyclerView>(R.id.allModelsOfBrandRecyclerView)
        recyclerView.adapter = AllModelsOfBrandAdapter(chosenBrandCarArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))
    }
}