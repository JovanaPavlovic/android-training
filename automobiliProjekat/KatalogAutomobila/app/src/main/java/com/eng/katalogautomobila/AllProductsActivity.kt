package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eng.katalogautomobila.GlobalData.Logotip
import com.eng.katalogautomobila.GlobalData.logotipArray

class AllProductsActivity : AppCompatActivity() {

    val searchedArray: ArrayList<GlobalData.ChosenBrandCar> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_products)

        GlobalData.logotipArray.clear()
        fillLogotipArray()

        val recyclerView = findViewById<RecyclerView>(R.id.allProductsRecyclerView)
        recyclerView.adapter = AllProductsAdapter(logotipArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        val searchBtn = findViewById<Button>(R.id.searchButton)
        val closeBtn = findViewById<Button>(R.id.closeButton)
        val userSearch = findViewById<EditText>(R.id.searchEditText)

        searchBtn.setOnClickListener {
            searchedArray.clear()

            for(car in GlobalData.carArray) {
                val nadovezi = (car.marka + " " + car.model).toLowerCase()

                if(!userSearch.text.toString().isEmpty() && nadovezi.contains(userSearch.text.toString().toLowerCase())) {
                    val image = GlobalData.chooseCarImage(car.marka)
                    searchedArray.add(GlobalData.ChosenBrandCar(
                        image, car.model, car.cijena + " $", car.marka,
                        car.motor, car.paketOpereme, car.tezinaPraznogVozila
                    ))
                }
            }

            closeBtn.visibility = View.VISIBLE
            recyclerView.adapter = AllModelsOfBrandAdapter(searchedArray)
        }

        closeBtn.setOnClickListener {
            recyclerView.adapter = AllProductsAdapter(logotipArray)
            closeBtn.visibility = View.GONE
            userSearch.setText("")
        }
    }

    private fun fillLogotipArray() {
        logotipArray.add(Logotip(R.drawable.alfaromeo, "Alfa Romeo"))
        logotipArray.add(Logotip(R.drawable.audi, "Audi"))
        logotipArray.add(Logotip(R.drawable.citroen, "Citroen"))
        logotipArray.add(Logotip(R.drawable.dacia, "Dacia"))
        logotipArray.add(Logotip(R.drawable.fiat, "Fiat"))
        logotipArray.add(Logotip(R.drawable.ford, "Ford"))
        logotipArray.add(Logotip(R.drawable.honda, "Honda"))
        logotipArray.add(Logotip(R.drawable.hyundai, "Hyundai"))
        logotipArray.add(Logotip(R.drawable.infiniti, "Infiniti"))
        logotipArray.add(Logotip(R.drawable.isuzu, "Isuzu"))
        logotipArray.add(Logotip(R.drawable.jeep, "Jeep"))
        logotipArray.add(Logotip(R.drawable.lada, "Lada"))
        logotipArray.add(Logotip(R.drawable.mazda, "Mazda"))
        logotipArray.add(Logotip(R.drawable.mercedesbenz, "Mercedes-Benz"))
        logotipArray.add(Logotip(R.drawable.mini, "Mini"))
        logotipArray.add(Logotip(R.drawable.mitsubishi, "Mitsubishi"))
        logotipArray.add(Logotip(R.drawable.nissan, "Nissan"))
        logotipArray.add(Logotip(R.drawable.opel, "Opel"))
        logotipArray.add(Logotip(R.drawable.peugeot, "Peugeot"))
        logotipArray.add(Logotip(R.drawable.renault, "Renault"))
        logotipArray.add(Logotip(R.drawable.seat, "Seat"))
        logotipArray.add(Logotip(R.drawable.skoda, "Skoda"))
        logotipArray.add(Logotip(R.drawable.smart, "Smart"))
        logotipArray.add(Logotip(R.drawable.subaru, "Subaru"))
        logotipArray.add(Logotip(R.drawable.suzuki, "Suzuki"))
        logotipArray.add(Logotip(R.drawable.volkswagen, "Volkswagen"))
        logotipArray.add(Logotip(R.drawable.volvo, "Volvo"))
    }
}