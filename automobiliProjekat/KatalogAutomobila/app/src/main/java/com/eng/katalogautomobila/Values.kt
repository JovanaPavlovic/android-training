package com.eng.katalogautomobila

object GlobalData {
    data class Logotip(val image: Int, val title: String)

    val logotipArray: ArrayList<Logotip> = arrayListOf()

    data class Car(val marka: String, val model: String, val motor: String, val paketOpereme: String, val cijena: String,
        val rasporedCilindara: String, val brojVentila: String, val precnikKodKlipa: String, val tipUbrizgavanja: String,
        val sistemOtvaranjaVentila: String, val turbo: String, val zapreminaMotora: String, val KW: String, val KS: String,
        val snagaPriObrtajima: String, val obrtniMoment: String, val obrtniMomentPriObrtajima: String, val stepenKompresije: String,
        val tipMjenjaca: String, val brojStepeniPrenosa: String, val pogon: String, val duzina: String, val sirina: String,
        val visina: String, val medjuosovinskoRastojanje: String, val tezinaPraznogVozila: String, val maksimalnoDozvoljenaTezina: String,
        val zapreminaRezervoara: String, val zapreminaPrtljaznika: String, val maksZapreminaPrtljaznika: String, val dozvoljenTovar: String,
        val dozvoljenoOpterecenjeKrova: String, val dozvoljenaTezinaPrikoliceBK: String, val dozvoljenaTezinaPrikoliceSK12: String,
        val dozvoljenaTezinaPrikoliceSK8: String, val opterecenjeKuke: String, val radijusOkretanja: String, val tragTockovaNaprijed: String,
        val tragTockovaNazad: String, val maksimalnaBrzina: String, val ubrzanje0_100: String, val ubrzanje0_200: String,
        val ubrzanje80_120finalniStepen: String, val zaustavniPut100: String, val vrijemeZa400m: String, val potrosnjaGrad: String,
        val potrosnjaVGrada: String, val emisijaC02: String, val katalizator: String, val dimenzijePneumatika: String,
        val prednjeOpruge: String, val zadnjeOpruge: String, val prednjiStabilizator: String, val zadnjiStabilizator: String,
        val garancijaKorozija: String, val garancijaMotor: String, val euroNCAP: String, val euroNCAPzvjezdice: String, val gorivo: String,
        val brojVrata: String, val brojSjedista: String)

    val carArray: ArrayList<Car> = arrayListOf()

    data class ChosenBrandCar(val image: Int, val model: String, val price: String, val brand: String,
                              val engine: String, val equipmentPackage: String, val emptyVehicleWeight: String)

    var i = 1
    fun chooseCarImage(brand: String) : Int{
        if(brand == "Audi") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.audi_q8}
            else if(i==2) {i++; return R.drawable.audi_r8_coupe}
            else if(i==3) {i++; return R.drawable.a5_cabrio}
            else {i++; return R.drawable.a4_rs_avant}
        } else if(brand == "Mercedes-Benz") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.mercedes_b_klasa}
            else if(i==2) {i++; return R.drawable.mercedes_c_klasa_karavan}
            else if(i==3) {i++; return R.drawable.mercedes_e_klasa_kabriolet}
            else {i++; return R.drawable.mercedes_g_klasa}
        } else if(brand == "Alfa Romeo") {
            if(i>2) i = 1

            if(i==1) {i++; return R.drawable.alfa_giulietta}
            else {i++; return R.drawable.alfa_giulia}
        } else if(brand == "Citroen") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.citroen_c3}
            else if(i==2) {i++; return R.drawable.citroen_c5_aircross_suv}
            else {i++; return R.drawable.citroen_c_elysee}
        } else if(brand == "Dacia") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.dacia_duster}
            else if(i==2) {i++; return R.drawable.dacia_lodgy}
            else {i++; return R.drawable.dacia_logan}
        } else if(brand == "Fiat") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.fiat_500}
            else if(i==2) {i++; return R.drawable.fiat_500_l}
            else {i++; return R.drawable.fiat_panda}
        } else if(brand == "Honda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.honda_civic_sedan}
            else if(i==2) {i++; return R.drawable.honda_civid_c5}
            else {i++; return R.drawable.honda_cr_v}
        } else if(brand == "Hyundai") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.hyndai_i30}
            else if(i==2) {i++; return R.drawable.hyndai_h1}
            else {i++; return R.drawable.hyndai_i30_wagon}
        } else if(brand == "Infiniti") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.infiniti_q30}
            else if(i==2) {i++; return R.drawable.infiniti_q60}
            else {i++; return R.drawable.infiniti_qx70}
        } else if(brand == "Isuzu") {
            return R.drawable.isuzu_d_max_single_cab
        } else if(brand == "Jeep") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.jeep_grand_cherokee}
            else if(i==2) {i++; return R.drawable.jeep_renegade}
            else {i++; return R.drawable.jeep_wrangler_2_doors}
        } else if(brand == "Lada") {
            if(i>2) i = 1

            if(i==1) {i++; return R.drawable.lada_4x4_urban}
            else {i++; return R.drawable.lada_kalina_hatchback}
        } else if(brand == "Mini") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mini_cabrio}
            else if(i==2) {i++; return R.drawable.mini_clubman}
            else {i++; return R.drawable.mini_hatch}
        } else if(brand == "Ford") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.ford_mustang_cabriolet}
            else if(i==2) {i++; return R.drawable.ford_mondeo_5_doors}
            else if(i==3) {i++; return R.drawable.ford_focus_sedan}
            else {i++; return R.drawable.ford_mustang_fastback}
        } else if(brand == "Mitsubishi") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mitsubishi_asx}
            else if(i==2) {i++; return R.drawable.mitsubishi_eclipse_cross}
            else {i++; return R.drawable.mitsubishi_l_200}
        } else if(brand == "Subaru") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.subaru_brz}
            else if(i==2) {i++; return R.drawable.subaru_levorg}
            else {i++; return R.drawable.subaru_xv}
        } else if(brand == "Nissan") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.nissan_juke}
            else if(i==2) {i++; return R.drawable.nissan_micra}
            else {i++; return R.drawable.nissan_navara}
        } else if(brand == "Opel") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.opel_corsa}
            else if(i==2) {i++; return R.drawable.opel_insignia_sports_tourer}
            else {i++; return R.drawable.opel_zafira}
        } else if(brand == "Peugeot") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.peugeot_108}
            else if(i==2) {i++; return R.drawable.peugeot_3008}
            else {i++; return R.drawable.peugeot_traveller}
        } else if(brand == "Volkswagen") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.volkswagen_arteon}
            else if(i==2) {i++; return R.drawable.volkswagen_polo}
            else {i++; return R.drawable.volkswagen_touareg}
        } else if(brand == "Volvo") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.volvo_xc_90}
            else if(i==2) {i++; return R.drawable.volvo_v_90}
            else {i++; return R.drawable.volvo_xc_114}
        } else if(brand == "Renault") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.renault_clio_grandtour}
            else if(i==2) {i++; return R.drawable.renault_koleos}
            else {i++; return R.drawable.renault_megan_grand_coupe}
        } else if(brand == "Seat") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.seat_ibiza}
            else if(i==2) {i++; return R.drawable.seat_leon}
            else {i++; return R.drawable.seat_taraco}
        } else if(brand == "Cupra") {
            return R.drawable.cupra_leon
        } else if(brand == "Suzuki") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.suzuki_ignis}
            else if(i==2) {i++; return R.drawable.suzuki_s_cross}
            else {i++; return R.drawable.suzuki_vitara}
        } else if(brand == "Skoda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.skoda_fabia_facelift}
            else if(i==2) {i++; return R.drawable.skoda_karoq}
            else {i++; return R.drawable.skoda_octavia}
        } else if(brand == "Mazda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mazda_3_sedan}
            else if(i==2) {i++; return R.drawable.mazda_cx_30}
            else {i++; return R.drawable.mazda_mx_5_roaster}
        }

        return 0
    }
}