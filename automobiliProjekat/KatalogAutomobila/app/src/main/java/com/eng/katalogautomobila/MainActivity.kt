package com.eng.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import java.io.BufferedReader
import java.io.InputStreamReader

val user = "jpavlovic"
val pass = "lozinka"
var enteredUser = ""
var enteredPass = ""

var numberOfEnteringLoginPage = 0

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
        if(user == enteredUser && pass == enteredPass) {
            Log.d("LOGIN", "The user is logged in")
        } else {
            Log.d("LOGIN", "The user isn't logged in")
            numberOfEnteringLoginPage++
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } */

        val allProductsBtn = findViewById<Button>(R.id.allProductsButton)
        allProductsBtn.setOnClickListener {
            startActivity(Intent(this, AllProductsActivity::class.java))
        }

        fillCarArray()
    }

    private fun fillCarArray() {
        var line: String?
        var isHeader = true

        val openFile = InputStreamReader(assets.open("model.csv"))
        val lines = BufferedReader(openFile)

        while(lines.readLine().also { line = it } != "") {
            val row: List<String> = line!!.split(";")

            //if(row[0].isEmpty()) break
            if(isHeader) isHeader = false
            else {
                GlobalData.carArray.add(GlobalData.Car(row[0], row[1], row[2], row[3], row[4], row[5], row[6],
                    row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17],
                    row[18], row[19], row[20], row[21], row[22], row[22], row[24], row[25], row[26], row[27], row[28],
                    row[29], row[30], row[31], row[32], row[33], row[34], row[35], row[36], row[37], row[38], row[39],
                    row[40], row[41], row[42], row[43], row[44], row[45], row[46], row[47], row[48], row[49], row[50],
                    row[51], row[52], row[53], row[54], row[55], row[56], row[57], row[58], row[59], row[60]))
            }
        }
    }

}