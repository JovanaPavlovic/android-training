package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.w3c.dom.Text

class CarDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_details)

        val image = intent.getIntExtra("IMAGE", 0)
        val price = intent.getStringExtra("PRICE")
        val brand = intent.getStringExtra("BRAND")
        val model = intent.getStringExtra("MODEL")
        val engine = intent.getStringExtra("ENGINE")
        val equipmentPackage = intent.getStringExtra("EQUIPMENT PACKAGE")
        val emptyVehicleWeight = intent.getStringExtra("EMPTY VEHICLE WEIGHT")

        val logo = findViewById<ImageView>(R.id.carDetailsLogo)
        for(logotip in GlobalData.logotipArray) {
            if(logotip.title == brand) {
                logo.setImageResource(logotip.image)
            }
        }

        val carPrice = findViewById<TextView>(R.id.carPrice)
        carPrice.text = price

        val carImage = findViewById<ImageView>(R.id.carImage)
        carImage.setImageResource(image)

        var show = false
        val showDetailsLayout = findViewById<LinearLayout>(R.id.detailsLayout)
        val hideDetailsLayout = findViewById<LinearLayout>(R.id.detailsLayout2)
        val restValues = findViewById<TextView>(R.id.restValues)

        showDetailsLayout.setOnClickListener {
            if(show) {
                restValues.visibility = View.GONE
                hideDetailsLayout.visibility = View.GONE
                showDetailsLayout.visibility = View.VISIBLE
            } else {
                restValues.visibility = View.VISIBLE
                hideDetailsLayout.visibility = View.VISIBLE
                showDetailsLayout.visibility = View.GONE
            }
            show = !show
        }

        hideDetailsLayout.setOnClickListener {
            if(show) {
                restValues.visibility = View.GONE
                hideDetailsLayout.visibility = View.GONE
                showDetailsLayout.visibility = View.VISIBLE
            } else {
                restValues.visibility = View.VISIBLE
                hideDetailsLayout.visibility = View.VISIBLE
                showDetailsLayout.visibility = View.GONE
            }
            show = !show
        }

        for(car in GlobalData.carArray) {
            if(car.marka == brand && car.model == model && car.motor == engine
                && car.paketOpereme == equipmentPackage
                && car.tezinaPraznogVozila == emptyVehicleWeight) {

                val firstTenValues = findViewById<TextView>(R.id.firstTenValues)
                firstTenValues.text = "* MARKA: " + car.marka + "\n* MODEL: " + car.model +
                        "\n* MOTOR: " + car.motor + "\n* PAKET OPREME: " + car.paketOpereme +
                        "\n* CIJENA: " + car.cijena + "\n* RASPORED CILINDARA: " + car.rasporedCilindara +
                        "\n* BROJ VENTILA: " + car.brojVentila + "\n* PRECNIK KOD KLIPA: " +
                        car.precnikKodKlipa + "\n* TIP UBRIZGAVANJA: " + car.tipUbrizgavanja +
                        "\n* SISTEM OTVARANJA VENTILA: " + car.sistemOtvaranjaVentila

                restValues.text = "* TURBO: " + car.turbo + "\n* ZAPREMINA MOTORA: " + car.zapreminaMotora +
                        "\n* KW: " + car.KW + "\n* KS: " + car.KS + "\n* SNAGA PRI OBRTAJIMA: " + car.snagaPriObrtajima +
                        "\n* OBRTNI MOMENT: " + car.obrtniMoment + "\n* OBRTNI MOMENT PRI OBRTAJIMA: " +
                        car.obrtniMomentPriObrtajima + "\n* STEPEN KOMPRESIJE: " + car.stepenKompresije +
                        "\n* TIP MJENJACA: " + car.tipMjenjaca + "\n* BROJ STEPENI PRENOSA: " + car.brojStepeniPrenosa +
                        "\n* POGON: " + car.pogon + "\n* DUZINA: " + car.duzina + "\n* SIRINA: " + car.sirina +
                        "\n* VISINA: " + car.visina + "\n* MEDJUOSOVINSKO RASTOJANJE: " + car.medjuosovinskoRastojanje +
                        "\n* TEZINA PRAZNOG VOZILA: " + car.tezinaPraznogVozila + "\n* MAKSIMALNA DOZVOLJENA TEZINA: " +
                        car.maksimalnoDozvoljenaTezina + "\n* ZAPREMINA REZERVOARA: " + car.zapreminaRezervoara +
                        "\n* ZAPREMINA PRTLJAZNIKA: " + car.zapreminaPrtljaznika + "\n* MAKS.ZAPREMINA PRTLJAZNIKA: " +
                        car.maksZapreminaPrtljaznika + "\n* DOZVOLJEN TOVAR: " + car.dozvoljenTovar +
                        "\n* DOZVOLJENO OPTERECENJE KROVA: " + car.dozvoljenoOpterecenjeKrova +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE BK: " + car.dozvoljenaTezinaPrikoliceBK +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE SK12: " + car.dozvoljenaTezinaPrikoliceSK12 +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE SK8: " + car.dozvoljenaTezinaPrikoliceSK8 +
                        "\n* OPRERECENJE KUKE: " + car.opterecenjeKuke + "\n* RADIJUS OKRETANJA: " + car.radijusOkretanja +
                        "\n* TRAG TOCKOVA NAPRIJED: " + car.tragTockovaNaprijed + "\n* TRAG TOCKOVA NAZAD: " +
                        car.tragTockovaNazad + "\n* MAKSIMALNA BRZINA: " + car.maksimalnaBrzina + "\n* UBRZANJE 0-100: " +
                        car.ubrzanje0_100 + "\n* UBRZANJE 0-200: " + car.ubrzanje0_200 + "\n* UBRZANJE 80-120 FINALNI STEPEN: " +
                        car.ubrzanje80_120finalniStepen + "\n* ZAUSTAVNI PUT 100:" + car.zaustavniPut100 + "\n* VRIJEME ZA 400m: " +
                        car.vrijemeZa400m + "\n* POTROSNJA GRAD: " + car.potrosnjaGrad + "\n* POTROSNJA VAN GRADA: " +
                        car.potrosnjaVGrada + "\n* EMISIJA CO2: " + car.emisijaC02 + "\n* DIMENZIJE PNEUMATIKA: " +
                        car.dimenzijePneumatika + "\n* PREDNJE OPRUGE: " + car.prednjeOpruge + "\n* ZADNJE OPRUGE: " +
                        car.zadnjeOpruge + "\n* PREDNJI STABILIZATOR: " + car.prednjiStabilizator + "\n* ZADNJI STABILIZATOR: " +
                        car.zadnjiStabilizator + "\n* ZADNJI STABILIZATOR: " + car.zadnjiStabilizator + "\n* GARANCIJA KOROZIJA: " +
                        car.garancijaKorozija + "\n* GARANCIJA MOTOR: " + car.garancijaMotor + "\n* EURO NCAP: " +
                        car.euroNCAP + "\n* EURO NCAP ZVJEZDICE: " + car.euroNCAPzvjezdice + "\n* GORIVO: " + car.gorivo +
                        "\n* BROJ VRATA: " + car.brojVrata + "\n* BROJ SJEDISTA: " + car.brojSjedista
            }
        }
    }
}