package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AllProductsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_products)

        GlobalData.logotipArray.clear()
        GlobalData.newClassObject.fillLogotipArray()

        val recyclerView = findViewById<RecyclerView>(R.id.allProductsRecyclerView)
        recyclerView.adapter = AllProductsAdapter(GlobalData.logotipArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        val close = findViewById<ImageView>(R.id.closeImageView)
        val userSearch = findViewById<EditText>(R.id.searchEditText)

        userSearch.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                GlobalData.newClassObject.fillSearchedArray(userSearch)

                close.visibility = View.VISIBLE
                recyclerView.adapter = AllModelsOfBrandAdapter(GlobalData.searchedArray)
            }

        })

        close.setOnClickListener {
            userSearch.setText("")
            recyclerView.adapter = AllProductsAdapter(GlobalData.logotipArray)
            close.visibility = View.GONE
        }
    }
}