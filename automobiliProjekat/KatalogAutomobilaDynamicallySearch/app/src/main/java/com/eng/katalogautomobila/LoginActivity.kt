package com.eng.katalogautomobila

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val usernameEditText = findViewById<EditText>(R.id.userEditText)
        val passwordEditText = findViewById<EditText>(R.id.passwordEditText)
        val loginBtn = findViewById<Button>(R.id.loginButton)

        val userError = findViewById<TextView>(R.id.wrongUserTextView)
        val passError = findViewById<TextView>(R.id.wrongPassTextView)

        //ako smo u LoginActivity usli vise od jednog puta, to znaci da smo pogrijesili username ili password
        // pa se ispisuju odredjene poruke upozorenja
        if(numberOfEnteringLoginPage > 1) {
            if(enteredUser != user) {
                userError.text = "Wrong username!"
                userError.visibility = View.VISIBLE
                passError.visibility = View.GONE
            } else if(enteredPass != pass) {
                userError.visibility = View.GONE
                passError.text = "Wrong password!"
                passError.visibility = View.VISIBLE
                usernameEditText.setText(enteredUser)
            }
        }

        loginBtn.setOnClickListener {
            //Ako nismo unijeli nista u polje za username ili password, ispisuju se poruke upozorenja
            if(usernameEditText.text.toString().isEmpty() && passwordEditText.text.toString().isEmpty()) {
                userError.text = "You must enter username!"
                userError.visibility = View.VISIBLE
                passError.text = "You must enter password!"
                passError.visibility = View.VISIBLE
            } else if(usernameEditText.text.toString().isEmpty()) {
                userError.text = "You must enter username!"
                userError.visibility = View.VISIBLE
                passError.visibility = View.GONE
            } else if(passwordEditText.text.toString().isEmpty()) {
                userError.visibility = View.GONE
                passError.text = "You must enter password!"
                passError.visibility = View.VISIBLE
            } else {
                enteredUser = usernameEditText.text.toString()
                enteredPass = passwordEditText.text.toString()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
    }
}