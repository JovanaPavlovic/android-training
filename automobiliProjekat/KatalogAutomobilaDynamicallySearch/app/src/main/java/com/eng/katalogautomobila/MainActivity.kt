package com.eng.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

val user = "jpavlovic"
val pass = "lozinka"
var enteredUser = ""
var enteredPass = ""

var numberOfEnteringLoginPage = 0

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
        if(user == enteredUser && pass == enteredPass) {
            Log.d("LOGIN", "The user is logged in")
        } else {
            Log.d("LOGIN", "The user isn't logged in")
            numberOfEnteringLoginPage++
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } */

        val allProductsBtn = findViewById<Button>(R.id.allProductsButton)
        allProductsBtn.setOnClickListener {
            startActivity(Intent(this, AllProductsActivity::class.java))
        }

        GlobalData.newClassObject.fillCarArray(this)
    }
}