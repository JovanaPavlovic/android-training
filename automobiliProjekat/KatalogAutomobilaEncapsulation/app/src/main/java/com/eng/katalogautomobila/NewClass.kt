package com.eng.katalogautomobila
import android.content.Context
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import java.io.BufferedReader
import java.io.InputStreamReader
import com.eng.katalogautomobila.GlobalData.i

class NewClass {

    fun fillCarArray(context: Context) {
        var line: String?
        var isHeader = true

        val openFile = InputStreamReader(context.assets.open("model.csv"))
        val lines = BufferedReader(openFile)

        while(lines.readLine().also { line = it } != "") {
            val row: List<String> = line!!.split(";")

            //if(row[0].isEmpty()) break
            if(isHeader) isHeader = false
            else {
                GlobalData.carArray.add(GlobalData.Car(row[0], row[1], row[2], row[3], row[4], row[5], row[6],
                    row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17],
                    row[18], row[19], row[20], row[21], row[22], row[22], row[24], row[25], row[26], row[27], row[28],
                    row[29], row[30], row[31], row[32], row[33], row[34], row[35], row[36], row[37], row[38], row[39],
                    row[40], row[41], row[42], row[43], row[44], row[45], row[46], row[47], row[48], row[49], row[50],
                    row[51], row[52], row[53], row[54], row[55], row[56], row[57], row[58], row[59], row[60]))
            }
        }
    }

    fun fillLogotipArray() {
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.alfaromeo, "Alfa Romeo"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.audi, "Audi"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.citroen, "Citroen"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.dacia, "Dacia"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.fiat, "Fiat"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.ford, "Ford"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.honda, "Honda"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.hyundai, "Hyundai"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.infiniti, "Infiniti"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.isuzu, "Isuzu"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.jeep, "Jeep"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.lada, "Lada"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.mazda, "Mazda"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.mercedesbenz, "Mercedes-Benz"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.mini, "Mini"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.mitsubishi, "Mitsubishi"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.nissan, "Nissan"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.opel, "Opel"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.peugeot, "Peugeot"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.renault, "Renault"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.seat, "Seat"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.skoda, "Skoda"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.smart, "Smart"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.subaru, "Subaru"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.suzuki, "Suzuki"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.volkswagen, "Volkswagen"))
        GlobalData.logotipArray.add(GlobalData.Logotip(R.drawable.volvo, "Volvo"))
    }

    //value 'i' in global data
    fun chooseCarImage(brand: String) : Int {
        if(brand == "Audi") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.audi_q8}
            else if(i==2) {i++; return R.drawable.audi_r8_coupe}
            else if(i==3) {i++; return R.drawable.a5_cabrio}
            else {i++; return R.drawable.a4_rs_avant}
        } else if(brand == "Mercedes-Benz") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.mercedes_b_klasa}
            else if(i==2) {i++; return R.drawable.mercedes_c_klasa_karavan}
            else if(i==3) {i++; return R.drawable.mercedes_e_klasa_kabriolet}
            else {i++; return R.drawable.mercedes_g_klasa}
        } else if(brand == "Alfa Romeo") {
            if(i>2) i = 1

            if(i==1) {i++; return R.drawable.alfa_giulietta}
            else {i++; return R.drawable.alfa_giulia}
        } else if(brand == "Citroen") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.citroen_c3}
            else if(i==2) {i++; return R.drawable.citroen_c5_aircross_suv}
            else {i++; return R.drawable.citroen_c_elysee}
        } else if(brand == "Dacia") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.dacia_duster}
            else if(i==2) {i++; return R.drawable.dacia_lodgy}
            else {i++; return R.drawable.dacia_logan}
        } else if(brand == "Fiat") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.fiat_500}
            else if(i==2) {i++; return R.drawable.fiat_500_l}
            else {i++; return R.drawable.fiat_panda}
        } else if(brand == "Honda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.honda_civic_sedan}
            else if(i==2) {i++; return R.drawable.honda_civid_c5}
            else {i++; return R.drawable.honda_cr_v}
        } else if(brand == "Hyundai") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.hyndai_i30}
            else if(i==2) {i++; return R.drawable.hyndai_h1}
            else {i++; return R.drawable.hyndai_i30_wagon}
        } else if(brand == "Infiniti") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.infiniti_q30}
            else if(i==2) {i++; return R.drawable.infiniti_q60}
            else {i++; return R.drawable.infiniti_qx70}
        } else if(brand == "Isuzu") {
            return R.drawable.isuzu_d_max_single_cab
        } else if(brand == "Jeep") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.jeep_grand_cherokee}
            else if(i==2) {i++; return R.drawable.jeep_renegade}
            else {i++; return R.drawable.jeep_wrangler_2_doors}
        } else if(brand == "Lada") {
            if(i>2) i = 1

            if(i==1) {i++; return R.drawable.lada_4x4_urban}
            else {i++; return R.drawable.lada_kalina_hatchback}
        } else if(brand == "Mini") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mini_cabrio}
            else if(i==2) {i++; return R.drawable.mini_clubman}
            else {i++; return R.drawable.mini_hatch}
        } else if(brand == "Ford") {
            if(i>4) i = 1

            if(i==1) {i++; return R.drawable.ford_mustang_cabriolet}
            else if(i==2) {i++; return R.drawable.ford_mondeo_5_doors}
            else if(i==3) {i++; return R.drawable.ford_focus_sedan}
            else {i++; return R.drawable.ford_mustang_fastback}
        } else if(brand == "Mitsubishi") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mitsubishi_asx}
            else if(i==2) {i++; return R.drawable.mitsubishi_eclipse_cross}
            else {i++; return R.drawable.mitsubishi_l_200}
        } else if(brand == "Subaru") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.subaru_brz}
            else if(i==2) {i++; return R.drawable.subaru_levorg}
            else {i++; return R.drawable.subaru_xv}
        } else if(brand == "Nissan") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.nissan_juke}
            else if(i==2) {i++; return R.drawable.nissan_micra}
            else {i++; return R.drawable.nissan_navara}
        } else if(brand == "Opel") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.opel_corsa}
            else if(i==2) {i++; return R.drawable.opel_insignia_sports_tourer}
            else {i++; return R.drawable.opel_zafira}
        } else if(brand == "Peugeot") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.peugeot_108}
            else if(i==2) {i++; return R.drawable.peugeot_3008}
            else {i++; return R.drawable.peugeot_traveller}
        } else if(brand == "Volkswagen") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.volkswagen_arteon}
            else if(i==2) {i++; return R.drawable.volkswagen_polo}
            else {i++; return R.drawable.volkswagen_touareg}
        } else if(brand == "Volvo") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.volvo_xc_90}
            else if(i==2) {i++; return R.drawable.volvo_v_90}
            else {i++; return R.drawable.volvo_xc_114}
        } else if(brand == "Renault") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.renault_clio_grandtour}
            else if(i==2) {i++; return R.drawable.renault_koleos}
            else {i++; return R.drawable.renault_megan_grand_coupe}
        } else if(brand == "Seat") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.seat_ibiza}
            else if(i==2) {i++; return R.drawable.seat_leon}
            else {i++; return R.drawable.seat_taraco}
        } else if(brand == "Cupra") {
            return R.drawable.cupra_leon
        } else if(brand == "Suzuki") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.suzuki_ignis}
            else if(i==2) {i++; return R.drawable.suzuki_s_cross}
            else {i++; return R.drawable.suzuki_vitara}
        } else if(brand == "Skoda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.skoda_fabia_facelift}
            else if(i==2) {i++; return R.drawable.skoda_karoq}
            else {i++; return R.drawable.skoda_octavia}
        } else if(brand == "Mazda") {
            if(i>3) i = 1

            if(i==1) {i++; return R.drawable.mazda_3_sedan}
            else if(i==2) {i++; return R.drawable.mazda_cx_30}
            else {i++; return R.drawable.mazda_mx_5_roaster}
        }

        return 0
    }

    fun fillSearchedArray(userSearch: EditText) {
        GlobalData.searchedArray.clear()

        for(car in GlobalData.carArray) {
            val nadovezi = (car.marka + " " + car.model).toLowerCase()

            if(!userSearch.text.toString().isEmpty() && nadovezi.contains(userSearch.text.toString().toLowerCase())) {
                val image = chooseCarImage(car.marka)
                GlobalData.searchedArray.add(GlobalData.ChosenBrandCar(
                    image, car.model, car.cijena + " $", car.marka,
                    car.motor, car.paketOpereme, car.tezinaPraznogVozila
                ))
            }
        }
    }

    fun fillChosenBrandCarArray(carBrand:String?) {
        GlobalData.chosenBrandCarArray.clear()

        for(car in GlobalData.carArray) {
            if(car.marka == carBrand) {
                val image = chooseCarImage(carBrand)
                GlobalData.chosenBrandCarArray.add(
                    GlobalData.ChosenBrandCar(
                        image, car.model, car.cijena + " $", car.marka,
                        car.motor, car.paketOpereme, car.tezinaPraznogVozila
                    )
                )
            }
        }
    }

    fun findAppropriateLogoImage(logo: ImageView, brand: String?) {
        for(logotip in GlobalData.logotipArray) {
            if(logotip.title == brand) {
                logo.setImageResource(logotip.image)
            }
        }
    }

    fun showCarDetails(firstTenValues: TextView, restValues: TextView, brand: String?,
                       model: String?, engine: String?, equipmentPackage: String?,
                       emptyVehicleWeight: String?) {
        for(car in GlobalData.carArray) {
            if(car.marka == brand && car.model == model && car.motor == engine
                && car.paketOpereme == equipmentPackage
                && car.tezinaPraznogVozila == emptyVehicleWeight) {

                firstTenValues.text = "* MARKA: " + car.marka + "\n* MODEL: " + car.model +
                        "\n* MOTOR: " + car.motor + "\n* PAKET OPREME: " + car.paketOpereme +
                        "\n* CIJENA: " + car.cijena + "\n* RASPORED CILINDARA: " + car.rasporedCilindara +
                        "\n* BROJ VENTILA: " + car.brojVentila + "\n* PRECNIK KOD KLIPA: " +
                        car.precnikKodKlipa + "\n* TIP UBRIZGAVANJA: " + car.tipUbrizgavanja +
                        "\n* SISTEM OTVARANJA VENTILA: " + car.sistemOtvaranjaVentila

                restValues.text = "* TURBO: " + car.turbo + "\n* ZAPREMINA MOTORA: " + car.zapreminaMotora +
                        "\n* KW: " + car.KW + "\n* KS: " + car.KS + "\n* SNAGA PRI OBRTAJIMA: " + car.snagaPriObrtajima +
                        "\n* OBRTNI MOMENT: " + car.obrtniMoment + "\n* OBRTNI MOMENT PRI OBRTAJIMA: " +
                        car.obrtniMomentPriObrtajima + "\n* STEPEN KOMPRESIJE: " + car.stepenKompresije +
                        "\n* TIP MJENJACA: " + car.tipMjenjaca + "\n* BROJ STEPENI PRENOSA: " + car.brojStepeniPrenosa +
                        "\n* POGON: " + car.pogon + "\n* DUZINA: " + car.duzina + "\n* SIRINA: " + car.sirina +
                        "\n* VISINA: " + car.visina + "\n* MEDJUOSOVINSKO RASTOJANJE: " + car.medjuosovinskoRastojanje +
                        "\n* TEZINA PRAZNOG VOZILA: " + car.tezinaPraznogVozila + "\n* MAKSIMALNA DOZVOLJENA TEZINA: " +
                        car.maksimalnoDozvoljenaTezina + "\n* ZAPREMINA REZERVOARA: " + car.zapreminaRezervoara +
                        "\n* ZAPREMINA PRTLJAZNIKA: " + car.zapreminaPrtljaznika + "\n* MAKS.ZAPREMINA PRTLJAZNIKA: " +
                        car.maksZapreminaPrtljaznika + "\n* DOZVOLJEN TOVAR: " + car.dozvoljenTovar +
                        "\n* DOZVOLJENO OPTERECENJE KROVA: " + car.dozvoljenoOpterecenjeKrova +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE BK: " + car.dozvoljenaTezinaPrikoliceBK +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE SK12: " + car.dozvoljenaTezinaPrikoliceSK12 +
                        "\n* DOZVOLJENA TEZINA PRIKOLICE SK8: " + car.dozvoljenaTezinaPrikoliceSK8 +
                        "\n* OPRERECENJE KUKE: " + car.opterecenjeKuke + "\n* RADIJUS OKRETANJA: " + car.radijusOkretanja +
                        "\n* TRAG TOCKOVA NAPRIJED: " + car.tragTockovaNaprijed + "\n* TRAG TOCKOVA NAZAD: " +
                        car.tragTockovaNazad + "\n* MAKSIMALNA BRZINA: " + car.maksimalnaBrzina + "\n* UBRZANJE 0-100: " +
                        car.ubrzanje0_100 + "\n* UBRZANJE 0-200: " + car.ubrzanje0_200 + "\n* UBRZANJE 80-120 FINALNI STEPEN: " +
                        car.ubrzanje80_120finalniStepen + "\n* ZAUSTAVNI PUT 100:" + car.zaustavniPut100 + "\n* VRIJEME ZA 400m: " +
                        car.vrijemeZa400m + "\n* POTROSNJA GRAD: " + car.potrosnjaGrad + "\n* POTROSNJA VAN GRADA: " +
                        car.potrosnjaVGrada + "\n* EMISIJA CO2: " + car.emisijaC02 + "\n* DIMENZIJE PNEUMATIKA: " +
                        car.dimenzijePneumatika + "\n* PREDNJE OPRUGE: " + car.prednjeOpruge + "\n* ZADNJE OPRUGE: " +
                        car.zadnjeOpruge + "\n* PREDNJI STABILIZATOR: " + car.prednjiStabilizator + "\n* ZADNJI STABILIZATOR: " +
                        car.zadnjiStabilizator + "\n* ZADNJI STABILIZATOR: " + car.zadnjiStabilizator + "\n* GARANCIJA KOROZIJA: " +
                        car.garancijaKorozija + "\n* GARANCIJA MOTOR: " + car.garancijaMotor + "\n* EURO NCAP: " +
                        car.euroNCAP + "\n* EURO NCAP ZVJEZDICE: " + car.euroNCAPzvjezdice + "\n* GORIVO: " + car.gorivo +
                        "\n* BROJ VRATA: " + car.brojVrata + "\n* BROJ SJEDISTA: " + car.brojSjedista
            }
        }
    }
}