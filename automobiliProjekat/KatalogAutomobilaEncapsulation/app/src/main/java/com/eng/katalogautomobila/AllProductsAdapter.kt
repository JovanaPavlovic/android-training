package com.eng.katalogautomobila

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AllProductsAdapter(val logotipArray: ArrayList<GlobalData.Logotip>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.all_products_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return logotipArray.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val image = holder.view.findViewById<ImageView>(R.id.allProductsImageView)
        val title = holder.view.findViewById<TextView>(R.id.allProductsTextView)

        image.setImageResource(logotipArray[position].image)
        title.text = logotipArray[position].title

        holder.view.setOnClickListener {
            val intent = Intent(holder.view.context, AllModelsOfBrandActivity::class.java)
            intent.putExtra("LOGO", logotipArray[position].image)
            intent.putExtra("BRAND", logotipArray[position].title)
            holder.view.context.startActivity(intent)
        }
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
