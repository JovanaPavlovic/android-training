package com.eng.katalogautomobila

object GlobalData {
    data class Logotip(val image: Int, val title: String)

    val logotipArray: ArrayList<Logotip> = arrayListOf()

    data class Car(val marka: String, val model: String, val motor: String, val paketOpereme: String, val cijena: String,
        val rasporedCilindara: String, val brojVentila: String, val precnikKodKlipa: String, val tipUbrizgavanja: String,
        val sistemOtvaranjaVentila: String, val turbo: String, val zapreminaMotora: String, val KW: String, val KS: String,
        val snagaPriObrtajima: String, val obrtniMoment: String, val obrtniMomentPriObrtajima: String, val stepenKompresije: String,
        val tipMjenjaca: String, val brojStepeniPrenosa: String, val pogon: String, val duzina: String, val sirina: String,
        val visina: String, val medjuosovinskoRastojanje: String, val tezinaPraznogVozila: String, val maksimalnoDozvoljenaTezina: String,
        val zapreminaRezervoara: String, val zapreminaPrtljaznika: String, val maksZapreminaPrtljaznika: String, val dozvoljenTovar: String,
        val dozvoljenoOpterecenjeKrova: String, val dozvoljenaTezinaPrikoliceBK: String, val dozvoljenaTezinaPrikoliceSK12: String,
        val dozvoljenaTezinaPrikoliceSK8: String, val opterecenjeKuke: String, val radijusOkretanja: String, val tragTockovaNaprijed: String,
        val tragTockovaNazad: String, val maksimalnaBrzina: String, val ubrzanje0_100: String, val ubrzanje0_200: String,
        val ubrzanje80_120finalniStepen: String, val zaustavniPut100: String, val vrijemeZa400m: String, val potrosnjaGrad: String,
        val potrosnjaVGrada: String, val emisijaC02: String, val katalizator: String, val dimenzijePneumatika: String,
        val prednjeOpruge: String, val zadnjeOpruge: String, val prednjiStabilizator: String, val zadnjiStabilizator: String,
        val garancijaKorozija: String, val garancijaMotor: String, val euroNCAP: String, val euroNCAPzvjezdice: String, val gorivo: String,
        val brojVrata: String, val brojSjedista: String)

    val carArray: ArrayList<Car> = arrayListOf()

    data class ChosenBrandCar(val image: Int, val model: String, val price: String, val brand: String,
                              val engine: String, val equipmentPackage: String, val emptyVehicleWeight: String)

    val chosenBrandCarArray: ArrayList<ChosenBrandCar> = arrayListOf()
    val searchedArray: ArrayList<ChosenBrandCar> = arrayListOf()

    var i = 1

    val newClassObject = NewClass()
}