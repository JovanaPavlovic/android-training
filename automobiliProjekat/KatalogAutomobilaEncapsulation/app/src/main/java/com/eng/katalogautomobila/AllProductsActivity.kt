package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AllProductsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_products)

        GlobalData.logotipArray.clear()
        GlobalData.newClassObject.fillLogotipArray()

        val recyclerView = findViewById<RecyclerView>(R.id.allProductsRecyclerView)
        recyclerView.adapter = AllProductsAdapter(GlobalData.logotipArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        val searchBtn = findViewById<Button>(R.id.searchButton)
        val closeBtn = findViewById<Button>(R.id.closeButton)
        val userSearch = findViewById<EditText>(R.id.searchEditText)

        searchBtn.setOnClickListener {
            GlobalData.newClassObject.fillSearchedArray(userSearch)

            closeBtn.visibility = View.VISIBLE
            recyclerView.adapter = AllModelsOfBrandAdapter(GlobalData.searchedArray)
        }

        closeBtn.setOnClickListener {
            recyclerView.adapter = AllProductsAdapter(GlobalData.logotipArray)
            closeBtn.visibility = View.GONE
            userSearch.setText("")
        }
    }
}