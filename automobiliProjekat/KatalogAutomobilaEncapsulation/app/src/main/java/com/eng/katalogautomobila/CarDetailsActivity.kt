package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.w3c.dom.Text

class CarDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_details)

        val image = intent.getIntExtra("IMAGE", 0)
        val price = intent.getStringExtra("PRICE")
        val brand = intent.getStringExtra("BRAND")
        val model = intent.getStringExtra("MODEL")
        val engine = intent.getStringExtra("ENGINE")
        val equipmentPackage = intent.getStringExtra("EQUIPMENT PACKAGE")
        val emptyVehicleWeight = intent.getStringExtra("EMPTY VEHICLE WEIGHT")

        val logo = findViewById<ImageView>(R.id.carDetailsLogo)
        GlobalData.newClassObject.findAppropriateLogoImage(logo, brand)

        val carPrice = findViewById<TextView>(R.id.carPrice)
        carPrice.text = price

        val carImage = findViewById<ImageView>(R.id.carImage)
        carImage.setImageResource(image)

        var show = false
        val showDetailsLayout = findViewById<LinearLayout>(R.id.detailsLayout)
        val hideDetailsLayout = findViewById<LinearLayout>(R.id.detailsLayout2)
        val restValues = findViewById<TextView>(R.id.restValues)
        val firstTenValues = findViewById<TextView>(R.id.firstTenValues)

        showDetailsLayout.setOnClickListener {
            if(show) {
                restValues.visibility = View.GONE
                hideDetailsLayout.visibility = View.GONE
                showDetailsLayout.visibility = View.VISIBLE
            } else {
                restValues.visibility = View.VISIBLE
                hideDetailsLayout.visibility = View.VISIBLE
                showDetailsLayout.visibility = View.GONE
            }
            show = !show
        }

        hideDetailsLayout.setOnClickListener {
            if(show) {
                restValues.visibility = View.GONE
                hideDetailsLayout.visibility = View.GONE
                showDetailsLayout.visibility = View.VISIBLE
            } else {
                restValues.visibility = View.VISIBLE
                hideDetailsLayout.visibility = View.VISIBLE
                showDetailsLayout.visibility = View.GONE
            }
            show = !show
        }

        GlobalData.newClassObject.showCarDetails(firstTenValues, restValues, brand, model,
            engine, equipmentPackage, emptyVehicleWeight)
    }
}