package com.eng.audieventi

import android.view.Display
import androidx.appcompat.app.ActionBarDrawerToggle

object GlobalData {
    val businessLogicObject = BusinessLogic()
    val imageCoroutinesObject = ImageCoroutines()

    val jsonRoot = "http://mobileapp-coll.engds.it/AudiEventi/"
    val langsJSON = "langs/it_IT.json"
    val configJSON = "config/config.json"

    val dataFromLangsJSON: HashMap<String, String> = hashMapOf()
    var langsJSONlength = -1

    var isPremium = false

    var isAudiDrivingExperienceItem = false
    var isPlaceAndTerritoryItem = false
    var isProgrammaItem = false

    lateinit var infoAndContactObject: ModelData.InfoAndContact
    val notificationList: ArrayList<ModelData.Notification> = arrayListOf()
    lateinit var placesAndTerritoryObject: ModelData.PlacesAndTerritory
    lateinit var audiDrivingExperienceObject: ModelData.AudiDrivingExperience
    val calendarEventList: ArrayList<ModelData.CalendarEvent> = arrayListOf()
    lateinit var couponObject: ModelData.Coupon
    lateinit var programObject: ModelData.Program
    val foodExperienceList: ArrayList<ModelData.FoodExperience> = arrayListOf()

    val audiSiteURL = "https://www.audi.it"
}