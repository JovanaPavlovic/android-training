package com.eng.audieventi

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL

class ImageCoroutines : AppCompatActivity() {

    fun loadCalendarEventImage(imageURL: String, calendarEvent: ModelData.CalendarEvent) {
        val url = loadURL(imageURL)

        lifecycleScope.launch(Dispatchers.IO) {
            url?.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    calendarEvent.image = this@apply
                    GlobalData.calendarEventList.add(calendarEvent)
                }
            }
        }
    }

    fun loadAudiDrivingExperienceImage(imageURL: String, audiDrivingExperienceObject: ModelData.AudiDrivingExperience) {
        val url = loadURL(imageURL)

        lifecycleScope.launch(Dispatchers.IO) {
            url?.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    audiDrivingExperienceObject.image_ade = this@apply
                }
            }
        }
    }

    fun loadPlacesAndTerritoryImage(imageURL: String, placesAndTerritoryeObject: ModelData.PlacesAndTerritory){
        val url = loadURL(imageURL)

        lifecycleScope.launch(Dispatchers.IO) {
            url?.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    placesAndTerritoryeObject.image_places = this@apply
                }
            }
        }
    }

    fun loadAudiDrivingExperienceCarouselImage(carouselURLArray: ArrayList<String>,
                                               audiDrivingExperienceObject: ModelData.AudiDrivingExperience) {

        lifecycleScope.launch(Dispatchers.IO) {
            carouselURLArray.forEach {
                val url = loadURL(it)
                url?.getBitmap()?.apply {
                    withContext(Dispatchers.Main) {
                        audiDrivingExperienceObject.carousel_ade.add(this@apply)
                    }
                }
            }
        }
    }

    fun loadPlacesAndTerritoryCarouselImage(carouselURLArray: ArrayList<String>,
                                            placesAndTerritoryObject: ModelData.PlacesAndTerritory) {

        lifecycleScope.launch(Dispatchers.IO) {
            carouselURLArray.forEach {
                val url = loadURL(it)
                url?.getBitmap()?.apply {
                    withContext(Dispatchers.Main) {
                        placesAndTerritoryObject.carousel_places.add(this@apply)
                    }
                }
            }
        }
    }

    fun loadInfoAndContactImage(imageURL: String, infoAndContactObject: ModelData.InfoAndContact){
        val url = loadURL(imageURL)

        lifecycleScope.launch(Dispatchers.IO) {
            url?.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    infoAndContactObject.image_contact = this@apply
                }
            }
        }
    }

    fun loadProgramImage(imageURL: String, programObject: ModelData.Program){
        val url = loadURL(imageURL)

        lifecycleScope.launch(Dispatchers.IO) {
            url?.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    programObject.image = this@apply
                }
            }
        }
    }

    fun URL.getBitmap(): Bitmap? {
        return try {
            BitmapFactory.decodeStream(openStream())
        } catch (e: IOException) {
            null
        }
    }

    fun loadURL(imageURL: String): URL? {
        val url: URL? = try {
            URL(imageURL)
        } catch (e: MalformedURLException){
            Log.d("Exception", e.toString())
            null
        }

        return url
    }
}