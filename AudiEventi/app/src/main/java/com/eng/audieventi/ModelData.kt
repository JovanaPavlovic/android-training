package com.eng.audieventi

import android.graphics.Bitmap

object ModelData {

    data class DailyActivity(
        val day: String,
        val activities: ArrayList<Activity>
    )

    data class Activity(
        val start: String,
        val end: String,
        val activityName: String
    )

    data class Program(
        var image: Bitmap?,
        val title: String,
        val subTitle: String,
        val note: String,
        val description: String,
        val infoUri: String,
        val dailyActivityList: ArrayList<DailyActivity>
    )

    data class Coupon(
        val action: String?,
        val coupon: String,
        val eventId: String,
        val status: String,
        val eventStatus: String?,
        val valId: String,
        val result: String,
        val resultCode: String,
        val resultMessage: String
    )

    data class FoodExperience(
        val id: String,
        val title: String,
        val header: String,
        val subtitle_food: String,
        val image_food: Bitmap?,
        val program_experience: ArrayList<ProgramExperience>
    )

    data class ProgramExperience(
        val day: String,
        val start: String,
        val type: String,
        val activity: String,
        val place: String,
        val description: String,
        val food: String,
        val allergens: ArrayList<Allergens>
    )

    data class Allergens(
        val name: String
    )

    data class InfoAndContact(
        val id: String,
        val title: String,
        val free_text: String,
        var image_contact: Bitmap?,
        val contact_list: ArrayList<Contact>
    )

    data class Contact(
        val name: String,
        val place: String,
        val street: String,
        val region: String,
        val phone: String,
        val phone_fix: String,
        val email: String
    )

    data class AudiDrivingExperience(
        val id: String,
        val title: String,
        val free_text: String,
        val subtitle_ade: String,
        val title_ade: String,
        val carousel_ade: ArrayList<Bitmap?>,
        var image_ade: Bitmap?
    )

    data class PlacesAndTerritory(
        val id: String,
        val title: String,
        val description: String,
        val subtitle_places: String,
        val title_places: String,
        val carousel_places: ArrayList<Bitmap?>,
        var image_places: Bitmap?
    )

    data class Notification(
        val title: String,
        val date: String,
        val time: String,
        val description: String
    )

    data class CalendarEvent(
        val title: String,
        val description: String,
        var image: Bitmap?
    )
}