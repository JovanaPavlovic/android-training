package com.eng.audieventi.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.CalendarEventAdapter
import com.eng.audieventi.GlobalData
import com.eng.audieventi.R

class CalendarEventFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_calendar_event, container, false)

        val recyclerView = view.findViewById<RecyclerView>(R.id.calendarEventRecyclerView)
        recyclerView.adapter = CalendarEventAdapter(GlobalData.calendarEventList)
        recyclerView.setLayoutManager(GridLayoutManager(requireContext(), 1))

        return view
    }
}