package com.eng.audieventi.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.CustomViewHolder
import com.eng.audieventi.ModelData
import com.eng.audieventi.R

var isOpen = false

class ProgrammaAdapter(val receivedData: ArrayList<ModelData.DailyActivity>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.programma_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.programmaDay).text = receivedData[position].day

        val arrow = holder.view.findViewById<ImageView>(R.id.arrowImage)
        val recyclerView = holder.view.findViewById<RecyclerView>(R.id.activitiesRecyclerView)
        arrow.setOnClickListener {
            if(!isOpen) {
                recyclerView.visibility = View.VISIBLE
                recyclerView.adapter = ActivitiesAdapter(receivedData[position].activities)
                recyclerView.setLayoutManager(GridLayoutManager(holder.view.context, 1))
            } else {
                recyclerView.visibility = View.GONE
            }
            isOpen = !isOpen
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}