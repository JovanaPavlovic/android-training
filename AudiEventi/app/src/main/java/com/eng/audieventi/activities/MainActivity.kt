package com.eng.audieventi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.eng.audieventi.fragments.CalendarEventFragment
import com.eng.audieventi.fragments.HomeFragment
import com.eng.audieventi.fragments.MenuItemContentFragment
import com.eng.audieventi.fragments.SettingsFragment
import com.google.android.material.navigation.NavigationView
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GlobalData.businessLogicObject.setUpHamburgerMenu(this, this)

        if(savedInstanceState == null) {
            replaceFragment(HomeFragment())
        }

        /////////////////////////////////

        val navView: NavigationView = findViewById(R.id.nav_view)
        val navViewFooter: NavigationView = findViewById(R.id.nav_view_footer)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home_item -> replaceFragment(HomeFragment())
                R.id.calendario_eventi_item -> replaceFragment(CalendarEventFragment())
                R.id.programma_item -> {
                    GlobalData.isAudiDrivingExperienceItem = false
                    GlobalData.isPlaceAndTerritoryItem = false
                    GlobalData.isProgrammaItem = true
                    replaceFragment(MenuItemContentFragment())
                }
                R.id.audi_driving_experience_item -> {
                    GlobalData.isAudiDrivingExperienceItem = true
                    GlobalData.isPlaceAndTerritoryItem = false
                    GlobalData.isProgrammaItem = false
                    replaceFragment(MenuItemContentFragment())
                }
                R.id.info_and_contatti_item -> {
                    GlobalData.isAudiDrivingExperienceItem = false
                    GlobalData.isPlaceAndTerritoryItem = false
                    GlobalData.isProgrammaItem = false
                    replaceFragment(MenuItemContentFragment())
                }
                R.id.food_experience_item -> replaceFragment(FoodExperienceFragment())
                R.id.luoghi_e_territorio_item -> {
                    GlobalData.isAudiDrivingExperienceItem = false
                    GlobalData.isPlaceAndTerritoryItem = true
                    GlobalData.isProgrammaItem = false
                    replaceFragment(MenuItemContentFragment())
                }
                R.id.coupon_eventi_item -> {
                    replaceFragment(HomeFragment())
                    GlobalData.businessLogicObject.showDialog(this, this)
                }
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        navViewFooter.setNavigationItemSelectedListener {
                it -> replaceFragment(SettingsFragment())
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        ///////////////////////////////////
    }

    fun replaceFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragment, fragment, "tag")
        commit()

        //TODO: srediti slucaj ako 2x udje u isti fragment i za prvi ulazak u home fragment
        addToBackStack("tag")
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)

        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        else {
            super.onBackPressed()
        }
    }
}