package com.eng.audieventi.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.CustomViewHolder
import com.eng.audieventi.ModelData
import com.eng.audieventi.R

class ActivitiesAdapter(val receivedData: ArrayList<ModelData.Activity>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.activities_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.start).text = receivedData[position].start
        holder.view.findViewById<TextView>(R.id.end).text = " - " + receivedData[position].end
        holder.view.findViewById<TextView>(R.id.activityName).text = receivedData[position].activityName
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}