package com.eng.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.WindowManager

@Suppress("DEPRECATION")
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        ProcessJSON.loadJSON().execute(GlobalData.businessLogicObject.concatToRootJSON(GlobalData.langsJSON))
        TestData().populateTestData()

        checkIfMapIsFill()

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    fun checkIfMapIsFill() {
        Handler(Looper.getMainLooper()).postDelayed({
            if(GlobalData.dataFromLangsJSON.count() != GlobalData.langsJSONlength) {
                checkIfMapIsFill()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }, 100)
    }
}