package com.eng.audieventi.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import com.eng.audieventi.*
import com.eng.audieventi.fragments.MenuItemContentFragment

class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        GlobalData.businessLogicObject.setValueForHomePage(view)

        val eventiAudiSection = view.findViewById<ConstraintLayout>(R.id.eventiAudiSection)
        eventiAudiSection.setOnClickListener {
            (context as MainActivity).replaceFragment(CalendarEventFragment())
        }

        val gammaAudiSection = view.findViewById<ConstraintLayout>(R.id.gammaAudiSection)
        gammaAudiSection.setOnClickListener {
            startActivity(Intent(requireContext(), WebActivity::class.java))
        }

        val ilTuoEventoSection = view.findViewById<ConstraintLayout>(R.id.ilTuoEventoSection)
        ilTuoEventoSection.setOnClickListener {
            GlobalData.businessLogicObject.showDialog(requireContext(), requireActivity())
        }

        val audiDrivingExperienceSection = view.findViewById<ConstraintLayout>(R.id.audiDrivingExperienceSection)
        audiDrivingExperienceSection.setOnClickListener {
            GlobalData.isAudiDrivingExperienceItem = true
            GlobalData.isPlaceAndTerritoryItem = false
            GlobalData.isProgrammaItem = false
            (context as MainActivity).replaceFragment(MenuItemContentFragment())
        }

        val placeAndTerritorySection = view.findViewById<ConstraintLayout>(R.id.luoghiETerritorioSection)
        placeAndTerritorySection.setOnClickListener {
            GlobalData.isAudiDrivingExperienceItem = false
            GlobalData.isPlaceAndTerritoryItem = true
            GlobalData.isProgrammaItem = false
            (context as MainActivity).replaceFragment(MenuItemContentFragment())
        }

        val infoAndContactSection = view.findViewById<LinearLayout>(R.id.infoAndContattiSection)
        infoAndContactSection.setOnClickListener {
            GlobalData.isAudiDrivingExperienceItem = false
            GlobalData.isPlaceAndTerritoryItem = false
            GlobalData.isProgrammaItem = false
            (context as MainActivity).replaceFragment(MenuItemContentFragment())
        }

        val programmaSection = view.findViewById<ConstraintLayout>(R.id.reconnectToNatureSection)
        programmaSection.setOnClickListener {
            GlobalData.isAudiDrivingExperienceItem = false
            GlobalData.isPlaceAndTerritoryItem = false
            GlobalData.isProgrammaItem = true
            (context as MainActivity).replaceFragment(MenuItemContentFragment())
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        GlobalData.businessLogicObject.isPremiumEvent(requireActivity())
    }
}