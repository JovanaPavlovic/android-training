package com.eng.audieventi

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.adapters.InfoAndContactAdapter
import com.eng.audieventi.adapters.ProgrammaAdapter
import com.google.android.material.navigation.NavigationView
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import org.w3c.dom.Text

class BusinessLogic {

    fun concatToRootJSON(value: String): String {
        return GlobalData.jsonRoot + value
    }

    fun setValueForHomePage(view: View) {
        val reconnectToNatureTitle = view.findViewById<TextView>(R.id.reconnectToNatureTitle)
        reconnectToNatureTitle.text = view.getResources().getString(R.string.reconnectToNature)

        val reconnectToNatureSubitle = view.findViewById<TextView>(R.id.reconnectToNatureSubtitle)
        reconnectToNatureSubitle.text = view.getResources().getString(R.string.conHerveBarmasse)

        val foodExperienceTitle = view.findViewById<TextView>(R.id.foodExperienceTitle)
        foodExperienceTitle.text = GlobalData.dataFromLangsJSON.get("HOME_FOOD_EXPERIENCE_TITLE")

        val foodExperienceSubitle = view.findViewById<TextView>(R.id.foodExperienceSubtitle)
        foodExperienceSubitle.text = GlobalData.dataFromLangsJSON.get("HOME_FOOD_EXPERIENCE_SUBTITLE")

        val luoghiETerritorioTitle = view.findViewById<TextView>(R.id.luoghiETerritorioTitle)
        luoghiETerritorioTitle.text = GlobalData.dataFromLangsJSON.get("HOME_PLACES_TITLE")

        val luoghiETerritorioSubitle = view.findViewById<TextView>(R.id.luoghiETerritorioSubtitle)
        luoghiETerritorioSubitle.text = GlobalData.dataFromLangsJSON.get("HOME_PLACES_SUBTITLE")

        val audiDrivingExperienceTitle = view.findViewById<TextView>(R.id.audiDrivingExperienceTitle)
        audiDrivingExperienceTitle.text = view.getResources().getString(R.string.audiDrivingExperience)

        val audiDrivingExperienceSubtitle = view.findViewById<TextView>(R.id.audiDrivingExperienceSubtitle)
        audiDrivingExperienceSubtitle.text = view.getResources().getString(R.string.scopriLiProgramma)

        val infoAndContattiTitle = view.findViewById<TextView>(R.id.infoAndContattiTitle)
        infoAndContattiTitle.text = view.getResources().getString(R.string.infoAndContatti)

        val sondaggioTitle = view.findViewById<TextView>(R.id.sondaggioTitle)
        sondaggioTitle.text = GlobalData.dataFromLangsJSON.get("HOME_SURVEY_TITLE")

        val sondaggioSubtitle = view.findViewById<TextView>(R.id.sondaggioSubitle)
        sondaggioSubtitle.text = view.getResources().getString(R.string.aiutaciAMigliorare)

        val homeGuestEventListTitle = view.findViewById<TextView>(R.id.homeGuestEventListTitle)
        homeGuestEventListTitle.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_EVENT_LIST")

        val homeGuestEventListButton = view.findViewById<TextView>(R.id.homeGuestEventListButton)
        homeGuestEventListButton.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_EVENT_LIST_BUTTON")

        val homeGuestCouponLoginTitle = view.findViewById<TextView>(R.id.homeGuestCouponLoginTitle)
        homeGuestCouponLoginTitle.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_COUPON_LOGIN")

        val homeGuestCouponLoginButton = view.findViewById<TextView>(R.id.homeGuestCouponLoginButton)
        homeGuestCouponLoginButton.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_COUPON_LOGIN_BUTTON")

        val homeGuestAudiRangeTitle = view.findViewById<TextView>(R.id.homeGuestAudiRangeTitle)
        homeGuestAudiRangeTitle.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_AUDI_RANGE")

        val homeGuestAudiRangeButton = view.findViewById<TextView>(R.id.homeGuestAudiRangeButton)
        homeGuestAudiRangeButton.text = GlobalData.dataFromLangsJSON.get("HOME_GUEST_AUDI_RANGE_BUTTON")
    }

    fun setValueForAudiDrivingExperiencePage(view: View) {
        val pageTitle = view.findViewById<TextView>(R.id.pageTitle)
        pageTitle.text = view.getResources().getString(R.string.audiDrivingExperience)

        val image = view.findViewById<ImageView>(R.id.image)
        image.setImageBitmap(GlobalData.audiDrivingExperienceObject.image_ade)

        val mainTitle = view.findViewById<TextView>(R.id.mainTitle)
        mainTitle.text = GlobalData.audiDrivingExperienceObject.title

        val subtitle = view.findViewById<TextView>(R.id.subtitle)
        subtitle.text = GlobalData.audiDrivingExperienceObject.subtitle_ade

        val title = view.findViewById<TextView>(R.id.title)
        title.text = GlobalData.audiDrivingExperienceObject.title_ade

        val free_text = view.findViewById<TextView>(R.id.free_text)
        free_text.text = GlobalData.audiDrivingExperienceObject.free_text

        view.findViewById<TextView>(R.id.contactTitle).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.infoAndContactRecyclerView).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.recyclerView).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaNotes).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaInfo).visibility = View.GONE

        val carouselView = view.findViewById<CarouselView>(R.id.carouselView)
        carouselView.setPageCount(GlobalData.audiDrivingExperienceObject.carousel_ade.count())
        carouselView.setImageListener(audiDrivingExperienceImageListener)
    }

    var audiDrivingExperienceImageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            imageView.setImageBitmap(GlobalData.audiDrivingExperienceObject.carousel_ade[position])
        }
    }

    fun setValueForPlaceAndTerritoryPage(view: View) {
        val pageTitle = view.findViewById<TextView>(R.id.pageTitle)
        pageTitle.text = GlobalData.dataFromLangsJSON.get("PLACES_APPBAR_TITLE")

        val image = view.findViewById<ImageView>(R.id.image)
        image.setImageBitmap(GlobalData.placesAndTerritoryObject.image_places)

        val mainTitle = view.findViewById<TextView>(R.id.mainTitle)
        mainTitle.text = GlobalData.placesAndTerritoryObject.title

        val subtitle = view.findViewById<TextView>(R.id.subtitle)
        subtitle.text = GlobalData.placesAndTerritoryObject.subtitle_places

        val title = view.findViewById<TextView>(R.id.title)
        title.text = GlobalData.placesAndTerritoryObject.title_places

        val free_text = view.findViewById<TextView>(R.id.free_text)
        free_text.text = GlobalData.placesAndTerritoryObject.description

        view.findViewById<TextView>(R.id.contactTitle).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.infoAndContactRecyclerView).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.recyclerView).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaNotes).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaInfo).visibility = View.GONE

        val carouselView = view.findViewById<CarouselView>(R.id.carouselView)
        carouselView.setPageCount(GlobalData.placesAndTerritoryObject.carousel_places.count())
        carouselView.setImageListener(placesAndTerritoryImageListener)
    }

    var placesAndTerritoryImageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            imageView.setImageBitmap(GlobalData.placesAndTerritoryObject.carousel_places[position])
        }
    }

    fun setValueForInfoAndContactPage(view: View, context: Context) {
        val pageTitle = view.findViewById<TextView>(R.id.pageTitle)
        pageTitle.text = view.getResources().getString(R.string.infoAndContatti)

        val image = view.findViewById<ImageView>(R.id.image)
        image.setImageBitmap(GlobalData.infoAndContactObject.image_contact)

        val mainTitle = view.findViewById<TextView>(R.id.mainTitle)
        mainTitle.text = GlobalData.infoAndContactObject.title

        val free_text = view.findViewById<TextView>(R.id.free_text)
        free_text.text = GlobalData.infoAndContactObject.free_text

        view.findViewById<TextView>(R.id.subtitle).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.titleLayout).visibility = View.GONE
        view.findViewById<ConstraintLayout>(R.id.carouselLayout).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.recyclerView).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaNotes).visibility = View.GONE
        view.findViewById<TextView>(R.id.programmaInfo).visibility = View.GONE

        val recyclerView = view.findViewById<RecyclerView>(R.id.infoAndContactRecyclerView)
        recyclerView.adapter = InfoAndContactAdapter(GlobalData.infoAndContactObject.contact_list)
        recyclerView.setLayoutManager(GridLayoutManager(context, 1))
    }

    fun setValueForProgrammaPage(view: View, context: Context) {
        val pageTitle = view.findViewById<TextView>(R.id.pageTitle)
        pageTitle.text = GlobalData.dataFromLangsJSON.get("PROGRAM_APPBAR_TITLE")

        val image = view.findViewById<ImageView>(R.id.image)
        image.setImageBitmap(GlobalData.programObject.image)

        val mainTitle = view.findViewById<TextView>(R.id.mainTitle)
        mainTitle.text = GlobalData.programObject.title

        val subtitle = view.findViewById<TextView>(R.id.subtitle)
        subtitle.text = GlobalData.programObject.description

        val title = view.findViewById<TextView>(R.id.title)
        title.text = GlobalData.programObject.subTitle

        val note = view.findViewById<TextView>(R.id.programmaNotes)
        note.text = GlobalData.programObject.note

        val info = view.findViewById<TextView>(R.id.programmaInfo)
        info.text = "Info:\n" + GlobalData.programObject.infoUri

        view.findViewById<TextView>(R.id.free_text).visibility = View.GONE
        view.findViewById<ConstraintLayout>(R.id.carouselLayout).visibility = View.GONE
        view.findViewById<TextView>(R.id.contactTitle).visibility = View.GONE
        view.findViewById<RecyclerView>(R.id.infoAndContactRecyclerView).visibility = View.GONE

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = ProgrammaAdapter(GlobalData.programObject.dailyActivityList)
        recyclerView.setLayoutManager(GridLayoutManager(context, 1))
    }

    fun setUpHamburgerMenu(activity: Activity, appCompatActivity: AppCompatActivity) {
        val toolbar: Toolbar = activity.findViewById(R.id.toolbar)
        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.getSupportActionBar()?.setDisplayShowTitleEnabled(false)

        val drawerLayout: DrawerLayout = activity.findViewById(R.id.drawerLayout)
        val toggle = ActionBarDrawerToggle(activity, drawerLayout, toolbar, R.string.open, R.string.close)

        appCompatActivity.supportActionBar?.setHomeButtonEnabled(true)
        appCompatActivity.supportActionBar?.setHomeAsUpIndicator(R.mipmap.menu)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setTitleForMenuItem(activity)
    }

    private fun setTitleForMenuItem(activity: Activity) {
        val navView: NavigationView = activity.findViewById(R.id.nav_view)
        val menu = navView.menu

        val navViewFooter: NavigationView = activity.findViewById(R.id.nav_view_footer)
        val menu_footer = navViewFooter.menu

        menu.findItem(R.id.home_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_GUEST_HOME_PAGE"))
        menu.findItem(R.id.programma_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_EVENT_DETAIL"))
        menu.findItem(R.id.food_experience_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_FOOD_EXPERIENCE"))
        menu.findItem(R.id.luoghi_e_territorio_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_LOCATION"))
        menu.findItem(R.id.audi_driving_experience_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_ADE"))
        menu.findItem(R.id.sondaggio_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_SURVEY"))
        menu.findItem(R.id.calendario_eventi_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_GUEST_EVENT_LIST"))
        menu.findItem(R.id.coupon_eventi_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_GUEST_COUPON_LOGIN"))
        menu.findItem(R.id.info_and_contatti_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_PREMIUM_EVENT_INFO"))
        menu_footer.findItem(R.id.impostazioni_item).setTitle(GlobalData.dataFromLangsJSON.get("SIDE_MENU_GUEST_SETTINGS"))
    }

    fun isPremiumEvent(activity: Activity) {
        val navView: NavigationView = activity.findViewById(R.id.nav_view)
        val menu = navView.menu

        if(GlobalData.isPremium) {
            menu.findItem(R.id.programma_item).isVisible = true
            menu.findItem(R.id.food_experience_item).isVisible = true
            menu.findItem(R.id.luoghi_e_territorio_item).isVisible = true
            menu.findItem(R.id.audi_driving_experience_item).isVisible = true
            menu.findItem(R.id.sondaggio_item).isVisible = true
            menu.findItem(R.id.info_and_contatti_item).isVisible = true

            activity.findViewById<ConstraintLayout>(R.id.premiumEventContent).visibility = View.VISIBLE
        }
    }

    fun showDialog(context: Context, activity: Activity) {
        val dialogView = LayoutInflater.from(context).inflate(R.layout.coupon_dialog, null)
        val builder = AlertDialog.Builder(context).setView(dialogView)

        dialogView.findViewById<TextView>(R.id.couponMessageTextView).text = GlobalData.dataFromLangsJSON.get("COUPON_LOGIN_MESSAGE")
        val couponButton = dialogView.findViewById<Button>(R.id.couponButton)
        couponButton.text = GlobalData.dataFromLangsJSON.get("COUPON_LOGIN_BUTTON")

        val alertDialog = builder.show()

        val closeButton = dialogView.findViewById<ImageView>(R.id.closeCouponImageView)
        closeButton.setOnClickListener {
            alertDialog.dismiss()
        }

        couponButton.setOnClickListener {
            val enteredCoupon = alertDialog.findViewById<EditText>(R.id.couponEditText)

            if(enteredCoupon.text.toString().isEmpty()) {
                enteredCoupon.setError("You must enter coupon first!")
            } else {
                if(enteredCoupon.text.toString() == GlobalData.couponObject.coupon) {
                    GlobalData.isPremium = true
                    isPremiumEvent(activity)
                    alertDialog.dismiss()
                } else {
                    enteredCoupon.setError("Wrong coupon!")
                }
            }
        }
    }
}