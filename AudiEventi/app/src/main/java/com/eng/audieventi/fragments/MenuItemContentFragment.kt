package com.eng.audieventi.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.eng.audieventi.GlobalData
import com.eng.audieventi.R

class MenuItemContentFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_menu_item_content, container, false)

        if(GlobalData.isAudiDrivingExperienceItem) {
            GlobalData.businessLogicObject.setValueForAudiDrivingExperiencePage(view)
        } else if(GlobalData.isPlaceAndTerritoryItem) {
            GlobalData.businessLogicObject.setValueForPlaceAndTerritoryPage(view)
        } else if(GlobalData.isProgrammaItem) {
            GlobalData.businessLogicObject.setValueForProgrammaPage(view, requireContext())
        } else {
            GlobalData.businessLogicObject.setValueForInfoAndContactPage(view, requireContext())
        }

        return view
    }
}