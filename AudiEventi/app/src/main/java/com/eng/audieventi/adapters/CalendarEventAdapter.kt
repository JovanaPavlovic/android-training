package com.eng.audieventi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CalendarEventAdapter(val receivedData: ArrayList<ModelData.CalendarEvent>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.calendar_event_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val title = holder.view.findViewById<TextView>(R.id.calendarEventTitle)
        title.text = receivedData[position].title

        val description = holder.view.findViewById<TextView>(R.id.calendarEventDescription)
        description.text = receivedData[position].description

        val image = holder.view.findViewById<ImageView>(R.id.calendarEventImage)
        image.setImageBitmap(receivedData[position].image)
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
