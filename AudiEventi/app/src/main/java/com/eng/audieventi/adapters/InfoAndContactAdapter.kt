package com.eng.audieventi.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.CustomViewHolder
import com.eng.audieventi.ModelData
import com.eng.audieventi.R

class InfoAndContactAdapter(val receivedData: ArrayList<ModelData.Contact>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.info_and_contact_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if(receivedData[position].name != "") {
            holder.view.findViewById<TextView>(R.id.name).text = receivedData[position].name
        } else {
            holder.view.findViewById<TextView>(R.id.name).visibility = View.GONE
        }

        if(receivedData[position].place != "") {
            holder.view.findViewById<TextView>(R.id.place).text = receivedData[position].place
        } else {
            holder.view.findViewById<TextView>(R.id.place).visibility = View.GONE
        }

        if(receivedData[position].street != "") {
            holder.view.findViewById<TextView>(R.id.street).text = receivedData[position].street
        } else {
            holder.view.findViewById<TextView>(R.id.street).visibility = View.GONE
        }

        if(receivedData[position].region != "") {
            holder.view.findViewById<TextView>(R.id.region).text = receivedData[position].region
        } else {
            holder.view.findViewById<TextView>(R.id.region).visibility = View.GONE
        }

        var phones = "T " + receivedData[position].phone
        if(receivedData[position].phone_fix != "") {
            phones += " F " + receivedData[position].phone_fix
        }
        holder.view.findViewById<TextView>(R.id.phone).text = phones

        holder.view.findViewById<TextView>(R.id.email).text = "E-mail " + receivedData[position].email

        if(position == receivedData.count()-1) {
            holder.view.findViewById<View>(R.id.line).visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}