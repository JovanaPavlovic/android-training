package com.eng.parsiranjeinternet

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONArray
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://evolutiondoo.com/localJSON.json"
        preuzmiJSON().execute(jsonURL)

        val slikaURL = "https://www.planetware.com/wpimages/2019/10/switzerland-in-pictures-most-beautiful-places-matterhorn.jpg"
        val slika = findViewById<ImageView>(R.id.slika)
        preuzmiSliku(slika).execute(slikaURL)
    }

    inner class preuzmiSliku(val okvirZaSliku: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlZaPreuzimanje = params[0]

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            okvirZaSliku.setImageBitmap(result)
        }
    }

                                      //Params, Progres, Result
    inner class preuzmiJSON: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {
            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            println(result)
            ispisiPodatke(result)
        }
    }

    fun ispisiPodatke(preuzetiJSON: String?) {
        val ispis = findViewById<TextView>(R.id.ispis)
        var textZaIspis = ""

        val jsonArray = JSONArray(preuzetiJSON)
        jsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = jsonArray.getJSONObject(it)

                val ime = jsonObject.getString("ime")
                val prezime = jsonObject.getString("prezime")
                val mjesto = jsonObject.getString("mesto")
                val godiste = jsonObject.getInt("godiste")
                val visina = jsonObject.getInt("visina")
                val prosjek = jsonObject.getDouble("prosek")

                textZaIspis += ime + " " + prezime + " " + mjesto + " " + godiste + " " + visina + " " + prosjek + "\n"
            }
        }

        ispis.text = textZaIspis
    }
}