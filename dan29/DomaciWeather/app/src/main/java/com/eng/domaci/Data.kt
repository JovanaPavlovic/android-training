package com.eng.domaci

object Data {
    data class WeatherList(val dt: Int, val temp: Double,
                           val feels_like: Double, val temp_min: Double,
                           val temp_max: Double, val pressure: Int,
                           val sea_level: Int, val grnd_level: Int,
                           val humidity: Int, val temp_kf: Double,
                           val description: String, val clouds: Int,
                           val wind_speed: Double, val wind_deg: Int,
                           val wind_gust: Double, val visibility: Int,
                           val pop: Int, val sys: String,
                           val dt_txt: String)

    data class Weather(val cod: String, val message: Int,
                       val cnt: Int, val weatherListArray: ArrayList<WeatherList>,
                       val city_name: String, val lat: Double,
                       val lon: Double, val country: String,
                       val population: Int, val timezone: Int,
                       val sunrise: Int, val sunset:Int)

    lateinit var weatherData: Weather
}