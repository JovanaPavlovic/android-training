package com.eng.domaci

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(val receivedData: ArrayList<Data.WeatherList>) : RecyclerView.Adapter<CustomHolderView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolderView {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomHolderView(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomHolderView, position: Int) {
        val dateTime = holder.view.findViewById<TextView>(R.id.dateTimeTextView)
        dateTime.text = receivedData[position].dt_txt.substring(0, 10) + ", " + receivedData[position].dt_txt.substring(11)

        val temp = holder.view.findViewById<TextView>(R.id.tempTextView)
        temp.text = receivedData[position].temp.toString() + " K"

        val description = holder.view.findViewById<TextView>(R.id.descriptionTextView)
        description.text = receivedData[position].description

        val minMaxTemp = holder.view.findViewById<TextView>(R.id.minMaxTempTextView)
        minMaxTemp.text = receivedData[position].temp_min.toString() + "K / " + receivedData[position].temp_max + "K"

        val feelsLike = holder.view.findViewById<TextView>(R.id.feelsLikeTextView)
        feelsLike.text = "Feels like: " + receivedData[position].feels_like + "K"

        val pressure = holder.view.findViewById<TextView>(R.id.pressuretextView)
        pressure.text = "Pressure: " + receivedData[position].pressure + " mbar"

        val humidity = holder.view.findViewById<TextView>(R.id.humidityTextView)
        humidity.text = "Humidity: " + receivedData[position].humidity + " %"

        val seaLevel = holder.view.findViewById<TextView>(R.id.seaLeveltextView)
        seaLevel.text = "Sea level: " + receivedData[position].sea_level

        val grndLevel = holder.view.findViewById<TextView>(R.id.grndLeveltextView)
        grndLevel.text = "Grnd level: " + receivedData[position].grnd_level

        val tempKf = holder.view.findViewById<TextView>(R.id.tempKftextView)
        tempKf.text = "Temp kf: " + receivedData[position].temp_kf

        val windSpeed = holder.view.findViewById<TextView>(R.id.windSpeedTextView)
        windSpeed.text = "Wind speed: " + receivedData[position].wind_speed + " m/s"

        val windDeg = holder.view.findViewById<TextView>(R.id.windDegTextView)
        windDeg.text = "Wind deg: " + receivedData[position].wind_deg

        val windGust = holder.view.findViewById<TextView>(R.id.windGustTextView)
        windGust.text = "Wind gust: " + receivedData[position].wind_gust

        val visibility = holder.view.findViewById<TextView>(R.id.visibilityTextView)
        visibility.text = "Visibility: " + receivedData[position].visibility + " mi"

        val clouds = holder.view.findViewById<TextView>(R.id.cloudsTextView)
        clouds.text = "Clouds: " + receivedData[position].clouds

        val pop = holder.view.findViewById<TextView>(R.id.popTextView)
        pop.text = "Pop: " + receivedData[position].pop

        val pod = holder.view.findViewById<TextView>(R.id.podTextView)
        pod.text = "Pod: " + receivedData[position].sys

        val dt = holder.view.findViewById<TextView>(R.id.dtTextView)
        dt.text = "Dt: " + receivedData[position].dt


    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}

class CustomHolderView(val view: View) : RecyclerView.ViewHolder(view) {

}
