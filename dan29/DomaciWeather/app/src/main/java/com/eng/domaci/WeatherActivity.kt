package com.eng.domaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

class WeatherActivity : AppCompatActivity() {

    val filteredWeatherListArray: ArrayList<Data.WeatherList> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)

        val cityName = findViewById<TextView>(R.id.nameTextView)
        cityName.text = Data.weatherData.city_name + ", " + Data.weatherData.country

        val latLon = findViewById<TextView>(R.id.latLonTextView)
        latLon.text = "Lat: " + Data.weatherData.lat + " / " + "Lon: " + Data.weatherData.lon

        val timezone = findViewById<TextView>(R.id.timezoneTextView)
        timezone.text = "Timezone: " + Data.weatherData.timezone

        val population = findViewById<TextView>(R.id.populationTextView)
        population.text = "Population: " + Data.weatherData.population

        val sunrise = findViewById<TextView>(R.id.sunriseTextView)
        sunrise.text = "Sunrise: " + Data.weatherData.sunrise + " AM"

        val sunset = findViewById<TextView>(R.id.sunsetTextView)
        sunset.text = "Sunset: " + Data.weatherData.sunset + " PM"

        fillArray("2021-06-09")
        var recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = Adapter(filteredWeatherListArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

        val firstBtn = findViewById<Button>(R.id.firstBtn)
        val secondBtn = findViewById<Button>(R.id.secondBtn)
        val thirdBtn = findViewById<Button>(R.id.thirdBtn)
        val fourthBtn = findViewById<Button>(R.id.fourthBtn)
        val fifthbtn = findViewById<Button>(R.id.fifthBtn)

        firstBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-09")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        secondBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-10")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        thirdBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-11")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        fourthBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-12")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        fifthbtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-13")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }
    }

    fun fillArray(datum: String) {
        for (elem in Data.weatherData.weatherListArray) {
            if (elem.dt_txt.contains(datum)) {
                filteredWeatherListArray.add(elem)
            }
        }
    }
}