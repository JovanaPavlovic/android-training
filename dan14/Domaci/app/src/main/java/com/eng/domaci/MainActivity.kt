package com.eng.domaci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottom_navigation_bar = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val fragment = findNavController(R.id.fragment)
        bottom_navigation_bar.setupWithNavController(fragment)
    }
}