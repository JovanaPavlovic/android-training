package com.eng.pozivanjefragmenata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prviFragment.setOnClickListener {
            val prvi = PrviFragment()
            zamjeniFragment(prvi)
        }

        drugiFragment.setOnClickListener {
            val drugi = DrugiFragment()
            zamjeniFragment(drugi)
        }

        treciFragment.setOnClickListener {
            val treci = TreciFragment()
            zamjeniFragment(treci)
        }
    }

    fun zamjeniFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragmentContainerView, fragment)
        commit()
    }
}