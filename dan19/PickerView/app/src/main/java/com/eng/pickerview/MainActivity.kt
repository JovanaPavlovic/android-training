package com.eng.pickerview

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.text.SimpleDateFormat
import java.time.Clock
import java.util.*
import kotlin.math.min

class MainActivity : AppCompatActivity() {

    val formatDatuma = SimpleDateFormat("dd-MM-YYYY", Locale.US)
    val formatVremena = SimpleDateFormat("HH:mm", Locale.ITALY)    //veliko HH za 24-casovni ispis

    var datumIVrijeme = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ispis = findViewById<TextView>(R.id.languageSelector)
        ispis.setOnClickListener {
            //showChangeLang()
            uzmiDatum()
            //uzmiVrijeme()
        }
    }

    fun uzmiVrijeme() {
        val ispis = findViewById<TextView>(R.id.languageSelector)
        val sadasnjeVrijeme = Calendar.getInstance()
        val pikerVrijeme = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{ picker, hourOfDay, minute ->
            val sadasnjeVrijeme = Calendar.getInstance()
            sadasnjeVrijeme.set(Calendar.HOUR_OF_DAY, hourOfDay)
            sadasnjeVrijeme.set(Calendar.MINUTE, minute)

            val ispisVremena = formatVremena.format(sadasnjeVrijeme.time)
            //ispis.text = ispisVremena
            datumIVrijeme += " " + ispisVremena
            ispis.text = datumIVrijeme
        },
            sadasnjeVrijeme.get(Calendar.HOUR_OF_DAY), sadasnjeVrijeme.get(Calendar.MINUTE), true)  //true za 24-casovni prikaz

        pikerVrijeme.show()
    }

    fun uzmiDatum() {
        val ispis = findViewById<TextView>(R.id.languageSelector)
        val sadasnjeVrijeme = Calendar.getInstance()
        val pikerDatum = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ picker, year, month, dayOfMonth ->
            val sadasnjeVrijeme = Calendar.getInstance()
            sadasnjeVrijeme.set(Calendar.YEAR, year)
            sadasnjeVrijeme.set(Calendar.MONTH, month)
            sadasnjeVrijeme.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val ispisDatuma = formatDatuma.format(sadasnjeVrijeme.time)
            //ispis.text = ispisDatuma
            datumIVrijeme = ispisDatuma
            uzmiVrijeme()
        },
            sadasnjeVrijeme.get(Calendar.YEAR), sadasnjeVrijeme.get(Calendar.MONTH), sadasnjeVrijeme.get(Calendar.DAY_OF_MONTH))

        pikerDatum.show()
    }

    fun showChangeLang() {
        //context: this ---- kreiraj ovde
        val pikerJezici = AlertDialog.Builder(this)
        val nizJezika = arrayOf("Srpski", "Španski", "Engleski", "Ruski")
        val ispis = findViewById<TextView>(R.id.languageSelector)

        pikerJezici.setTitle("Izaberite jezik")
        pikerJezici.setSingleChoiceItems(nizJezika, -1) { picker, jezik ->
            ispis.text = nizJezika[jezik]
            picker.dismiss()    //zatvori dialog prozor kada se izabere jezik
        }

        val prikazi = pikerJezici.create()      //kreiraj AlertDialog koji smo izgradili
        prikazi.show()
    }
}