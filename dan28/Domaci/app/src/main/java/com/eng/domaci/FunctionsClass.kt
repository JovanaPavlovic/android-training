package com.eng.domaci

import android.content.Context
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class FunctionsClass {

    fun readJsonFile(context: Context) {
        val json: String?

        try {
            val inputStream: InputStream = context.assets.open("weather.json")
            json = inputStream.bufferedReader().use {
                it.readText()
            }

            val weatherObject = JSONObject(json)

            val coord = weatherObject.getJSONObject("coord")
            val lon = coord.getDouble("lon")
            val lat = coord.getDouble("lat")

            val weather = weatherObject.getJSONArray("weather")
            var idW = 0
            var mainW = ""
            var description = ""
            var icon = ""
            weather.let {
                (0 until it.length()).forEach {
                    val readWeather = weather.getJSONObject(it)

                    idW = readWeather.getInt("id")
                    mainW = readWeather.getString("main")
                    description = readWeather.getString("description")
                    icon = readWeather.getString("icon")
                }
            }

            val base = weatherObject.getString("base")

            val main = weatherObject.getJSONObject("main")
            val temp = main.getDouble("temp")
            val feels_like = main.getDouble("feels_like")
            val temp_min = main.getDouble("temp_min")
            val temp_max = main.getDouble("temp_max")
            val pressure = main.getInt("pressure")
            val humidity = main.getInt("humidity")

            val visibility = weatherObject.getInt("visibility")

            val wind = weatherObject.getJSONObject("wind")
            val speed = wind.getDouble("speed")
            val deg = wind.getInt("deg")
            val gust = wind.getDouble("gust")

            val clouds = weatherObject.getJSONObject("clouds")
            val all = clouds.getInt("all")

            val dt = weatherObject.getInt("dt")

            val sys = weatherObject.getJSONObject("sys")
            val type = sys.getInt("type")
            val idS = sys.getInt("id")
            val country = sys.getString("country")
            val sunrise = sys.getInt("sunrise")
            val sunset = sys.getInt("sunset")

            val timeZone = weatherObject.getInt("timezone")
            val id = weatherObject.getInt("id")
            val name = weatherObject.getString("name")
            val cod = weatherObject.getInt("cod")

            Data.weatherValues = Data.Weather(lon, lat, idW, mainW, description, icon, base, temp, feels_like,
                temp_min, temp_max, pressure, humidity, visibility, speed, deg, gust, all, dt, type, idS,
                country, sunrise, sunset, timeZone, id, name, cod)

        } catch (e: IOException) {
            Log.d("JSON Exception", e.toString())
        }
    }

    fun chooseImage(description: String) : Int {
        if(description == "broken clouds") return R.drawable.rainy
        else if(description == "sunny") return R.drawable.sunny
        else if(description == "cloudy") return R.drawable.cloudy
        else return 0
    }
}