package com.eng.domaci

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Data.functionsClassObject.readJsonFile(this)

        fillData()
    }

    fun fillData() {
        val name = findViewById<TextView>(R.id.nameTextView)
        name.text = Data.weatherValues.name

        val timeZoneCod = findViewById<TextView>(R.id.timeZoneCodTextView)
        timeZoneCod.text = "Timezone: " + Data.weatherValues.timezone + " / " + "Cod: " + Data.weatherValues.cod

        val image = findViewById<ImageView>(R.id.image)
        image.setImageResource(Data.functionsClassObject.chooseImage(Data.weatherValues.description))

        val temp = findViewById<TextView>(R.id.tempTextView)
        temp.text = Data.weatherValues.temp.toString() + "K"

        val description = findViewById<TextView>(R.id.descriptionTextView)
        description.text = Data.weatherValues.description

        val minMaxTemp = findViewById<TextView>(R.id.minMaxTempTextView)
        minMaxTemp.text = Data.weatherValues.temp_min.toString() + "K / " + Data.weatherValues.temp_max + "K"

        val feelsLike = findViewById<TextView>(R.id.feelsLikeTextView)
        feelsLike.text = "Feels like " + Data.weatherValues.feels_like + "K"

        val lonLat = findViewById<TextView>(R.id.lonLatTextView)
        lonLat.text = "Lon: " + Data.weatherValues.lon + " / " + "Lat: " + Data.weatherValues.lat

        val pressure = findViewById<TextView>(R.id.pressuretextView)
        pressure.text = "Pressure: " + Data.weatherValues.pressure + " mbar"

        val humidity = findViewById<TextView>(R.id.humidityTextView)
        humidity.text = "Humidity: " + Data.weatherValues.humidity + " %"

        val visibility = findViewById<TextView>(R.id.visibilityTextView)
        visibility.text = "Visibility: " + Data.weatherValues.visibility + " mi"

        val windSpeed = findViewById<TextView>(R.id.windSpeedTextView)
        windSpeed.text = "Wind speed: " + Data.weatherValues.speed + " m/s"

        val windDeg = findViewById<TextView>(R.id.windDegTextView)
        windDeg.text = "Wind deg: " + Data.weatherValues.deg

        val windGust = findViewById<TextView>(R.id.windGustTextView)
        windGust.text = "Wind gust: " + Data.weatherValues.gust

        val sunrise = findViewById<TextView>(R.id.sunriseTextView)
        sunrise.text = "Sunrise: " + Data.weatherValues.sunrise + " AM"

        val sunset = findViewById<TextView>(R.id.sunsetTextView)
        sunset.text = "Sunset: " + Data.weatherValues.sunset + " PM"

        val dt = findViewById<TextView>(R.id.dtTextView)
        dt.text = "Dt: " + Data.weatherValues.dt
    }
}