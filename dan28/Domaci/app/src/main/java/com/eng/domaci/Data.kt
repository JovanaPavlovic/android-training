package com.eng.domaci

object Data {
    val functionsClassObject = FunctionsClass()

    class Weather(
        val lon: Double, val lat: Double,
        val idW: Int, val mainW: String,
        val description: String, val icon: String,
        val base: String, val temp: Double,
        val feels_like: Double, val temp_min: Double,
        val temp_max: Double, val pressure: Int,
        val humidity: Int, val visibility: Int,
        val speed: Double, val deg: Int,
        val gust: Double, val all: Int,
        val dt: Int, val type: Int,
        val idS: Int, val country: String,
        val sunrise: Int, val sunset: Int,
        val timezone: Int, val id: Int,
        val name: String, val cod: Int
    )

    lateinit var weatherValues: Weather
}