package com.eng.jsonlocalparse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {

    val studenti: ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        procitajJSON()

        for(student in studenti) {
            Log.d("STUDENT", student.ime)
        }
    }

    fun procitajJSON() {
        val json: String?
        val ispis = findViewById<TextView>(R.id.ispis)
        var ispisStudenata = ""

        try {
            val inputStream: InputStream = assets.open("localJSON.json")
            json = inputStream.bufferedReader().use {
                it.readText()
            }

            //CITANJE NIZA OBJEKATA

            val jsonArray = JSONArray(json)
            jsonArray.let {
                //ovde je it niz
                (0 until it.length()).forEach {
                    //ovde je it jedan element niza
                    val jsonObjekat = jsonArray.getJSONObject(it)

                    val ime = jsonObjekat.getString("ime")
                    val prezime = jsonObjekat.getString("prezime")
                    val mjesto = jsonObjekat.getString("mjesto")
                    val godiste = jsonObjekat.getInt("godiste")
                    val visina = jsonObjekat.getInt("visina")
                    val prosjek = jsonObjekat.getDouble("prosjek")

                    val student = jsonObjekat.getJSONObject("student")
                    val ime1 = student.getString("ime")
                    val prezime1 = student.getString("prezime")
                    val mjesto1 = student.getString("mjesto")
                    val godiste1 = student.getInt("godiste")
                    val visina1 = student.getInt("visina")
                    val prosjek1 = student.getDouble("prosjek")

                    studenti.add(Student(ime, prezime, mjesto, godiste, visina, prosjek))

                    ispisStudenata += ime + " " + prezime + " " + mjesto + " " + godiste + " " + visina + " " + prosjek + "\n"
                    ispisStudenata += ime1 + " " + prezime1 + " " + mjesto1 + " " + godiste1 + " " + visina1 + " " + prosjek1 + "\n"
                }

                ispis.text = ispisStudenata
            }


            //CITANJE JEDNOG OBJEKTA

//            val jsonObjekat = JSONObject(json)
//
//            val ime = jsonObjekat.getString("ime")
//            val prezime = jsonObjekat.getString("prezime")
//            val mjesto = jsonObjekat.getString("mjesto")
//            val godiste = jsonObjekat.getInt("godiste")
//            val visina = jsonObjekat.getInt("visina")
//            val prosjek = jsonObjekat.getDouble("prosjek")
//
//            ispis.text = ime + " " + prezime + " " + mjesto + " " + godiste + " " + visina + " " + prosjek

         } catch (e: IOException) {
            Log.d("JSON Exception", e.toString())
        }
    }
}

class Student(
    val ime: String,
    val prezime: String,
    val mjesto: String,
    val godiste: Int,
    val visina: Int,
    val prosjek: Double
)