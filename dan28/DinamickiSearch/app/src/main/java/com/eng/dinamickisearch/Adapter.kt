package com.eng.dinamickisearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(val preuzetiPodaci: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val ispis = holder.view.findViewById<TextView>(R.id.zemljaTextView)
        ispis.text = preuzetiPodaci[position]
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
