package com.eng.dinamickisearch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val funkcionalnostiObjekat = Funkcionalnosti()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        funkcionalnostiObjekat.uveziDrzave()
        for(drzava in funkcionalnostiObjekat.listaDrzava) {
            Log.d("DRZAVA", drzava)
        }

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = Adapter(funkcionalnostiObjekat.listaDrzava)
        recyclerView.setLayoutManager(LinearLayoutManager(this))

        val unosTeksta = findViewById<EditText>(R.id.searchEditText)
        unosTeksta.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filtrirajDrzave(s.toString())
            }
        })
    }

    fun filtrirajDrzave(korisnickiUnos: String) {
        val filtriraneDrzave = ArrayList<String>()

        for(drzava in funkcionalnostiObjekat.listaDrzava) {
            if(drzava.lowercase().contains(korisnickiUnos.lowercase())) {
                filtriraneDrzave.add(drzava)
            }
        }

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = Adapter(filtriraneDrzave)
        //recyclerView.adapter!!.notifyDataSetChanged()
        //recyclerView.setLayoutManager(LinearLayoutManager(this))
    }
}

class Funkcionalnosti {
    val listaDrzava: ArrayList<String> = arrayListOf()

    fun uveziDrzave() {
        val moguceZemlje = Locale.getISOCountries()
        for(drzava in moguceZemlje) {
            val locale = Locale("", drzava)
            val nazivDrzave = locale.displayCountry
            listaDrzava.add(nazivDrzave)
        }
    }
}