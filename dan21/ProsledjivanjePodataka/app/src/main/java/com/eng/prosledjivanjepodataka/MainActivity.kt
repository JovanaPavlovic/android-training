package com.eng.prosledjivanjepodataka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dugme.setOnClickListener {
            val intent = Intent(this, LeftActivity::class.java)
            intent.putExtra("UNOS", editText.text.toString())
            startActivity(intent)
        }

        imageLeftMain.setOnClickListener{
            DataGlobal.isLeft = true
            startActivity(Intent(this, LeftActivity::class.java))
            finish()
        }

        imageRightMain.setOnClickListener{
            DataGlobal.isRight = true
            startActivity(Intent(this, RightActivity::class.java))
            finish()
        }
    }
}