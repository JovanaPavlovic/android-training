package com.eng.prosledjivanjepodataka

import android.content.Intent
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_left.*

class LeftActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_left)

        val prenijetaVrijednost = intent.getStringExtra("UNOS")
        ispisLeft.text = prenijetaVrijednost.toString()

        if(DataGlobal.isLeft) {
            imageLeftLeft.visibility = View.GONE
        }

        imageRightLeft.setOnClickListener {
            DataGlobal.isLeft = false
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}