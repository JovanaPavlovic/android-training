package com.eng.domaci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.eng.domaci01.R
import kotlinx.android.synthetic.main.activity_new.*
import kotlinx.android.synthetic.main.cell.*

class NewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        val arrayOfBooks = intent.getSerializableExtra("arrayOfBooks")

        var createdAdapter =
            myAdapter(DataGlobal.bookArray)
        if(DataGlobal.isIntent) createdAdapter =
            myAdapter(arrayOfBooks as ArrayList<Book>)

        recyclerView.adapter = createdAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }
}