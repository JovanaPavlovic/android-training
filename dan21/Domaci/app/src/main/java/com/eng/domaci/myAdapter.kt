package com.eng.domaci

import android.content.Intent
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.eng.domaci01.R
import kotlinx.android.synthetic.main.cell.view.*

class myAdapter(val books: ArrayList<Book>) : RecyclerView.Adapter<CustomHolderView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolderView {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomHolderView(cellForRow)
    }

    override fun getItemCount(): Int {
        return books.count()
    }

    override fun onBindViewHolder(holder: CustomHolderView, position: Int) {
        holder.view.titleTextView.text = books[position].title
        holder.view.authorTextView.text = books[position].author
        holder.view.priceTextView.text = books[position].price.toString() + " $"
        holder.view.imageView.setImageResource(books[position].image)

        val intent = Intent(holder.view.context, DetailsActivity::class.java)

        holder.view.setOnClickListener {
            if(DataGlobal.isIntent) {
                DataGlobal.bookTitle = holder.view.titleTextView.text.toString()
                DataGlobal.bookAuthor = holder.view.authorTextView.text.toString()
                DataGlobal.bookPrice = holder.view.priceTextView.text.toString().substring(0, 5).toDouble()
            } else {
                intent.putExtra("title", holder.view.titleTextView.text.toString())
                intent.putExtra("author", holder.view.authorTextView.text.toString())
                intent.putExtra("price", holder.view.priceTextView.text.toString())
            }

            holder.view.context.startActivity(intent)
        }
    }
}

class CustomHolderView(val view: View) : RecyclerView.ViewHolder(view) {

}
