package com.eng.domaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.eng.domaci01.R
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Serializable

data class Book(val title: String, val author: String, val price: Double, val image: Int) : Serializable

class MainActivity : AppCompatActivity() {

    val books: ArrayList<Book> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val forwardBtnIntent = findViewById<Button>(R.id.forward_intent)
        val forwardBtnDataGlobal = findViewById<Button>(R.id.forward)

        forwardBtnIntent.setOnClickListener {
            DataGlobal.isIntent = true
            parseCSV()

            val intent = Intent(this, NewActivity::class.java)
            intent.putExtra("arrayOfBooks", books)
            startActivity(intent)
        }

        forwardBtnDataGlobal.setOnClickListener {
            DataGlobal.isIntent = false
            parseCSV()

            startActivity(Intent(this, NewActivity::class.java))
        }
    }

    private fun parseCSV() {
        var line: String?
        var i = 1

        val openCSV = InputStreamReader(assets.open("textFile.csv"))
        val lines = BufferedReader(openCSV)

        while(lines.readLine().also { line = it } != "") {
            val row: List<String> = line!!.split(",")

            if(DataGlobal.isIntent) {
                if(i==1) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book1))
                else if(i==2) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book2))
                else if(i==3) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book3))
                else if(i==4) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book4))
                else if(i==5) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book5))
                else if(i==6) books.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book6))
                i++
            } else {
                if(i==1) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book1))
                else if(i==2) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book2))
                else if(i==3) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book3))
                else if(i==4) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book4))
                else if(i==5) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book5))
                else if(i==6) DataGlobal.bookArray.add(Book(row[0], row[1], row[2].toDouble(), R.drawable.book6))
                i++
            }

        }


    }
}