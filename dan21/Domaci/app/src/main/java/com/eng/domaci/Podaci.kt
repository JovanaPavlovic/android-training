package com.eng.domaci

object DataGlobal {
    var isIntent = false

    var bookTitle = ""
    var bookAuthor = ""
    var bookPrice = 0.0

    val bookArray = arrayListOf<Book>()
}