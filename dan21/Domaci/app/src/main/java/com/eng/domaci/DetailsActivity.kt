package com.eng.domaci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import com.eng.domaci01.R
import kotlinx.android.synthetic.main.activity_details.*
import java.util.zip.DataFormatException

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val receivedTitle = intent.getStringExtra("title")
        val receivedAuthor = intent.getStringExtra("author")
        val receivedPrice = intent.getStringExtra("price")

        if(DataGlobal.isIntent) {
            ispis.text = "TITLE:\n" + DataGlobal.bookTitle +
                    "\n\nAUTHOR:\n" + DataGlobal.bookAuthor +
                    "\n\nPRICE:\n" + DataGlobal.bookPrice + " $"
        } else {
            ispis.text = "TITLE:\n" + receivedTitle +
                    "\n\nAUTHOR:\n" + receivedAuthor +
                    "\n\nPRICE:\n" + receivedPrice
        }
    }
}