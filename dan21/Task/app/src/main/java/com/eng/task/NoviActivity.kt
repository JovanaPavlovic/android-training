package com.eng.task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_novi.*

class NoviActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novi)

        if(ZeroOrOne == "0") {
            noviActivity.setBackgroundResource(R.color.purple_200)
        } else if(ZeroOrOne == "1") {
            noviActivity.setBackgroundResource(R.color.black)
        }
    }
}