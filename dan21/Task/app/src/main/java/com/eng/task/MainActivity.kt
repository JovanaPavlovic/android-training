package com.eng.task

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

var ZeroOrOne = ""

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dugme.setOnClickListener {
            ZeroOrOne = editText.text.toString()

            if(ZeroOrOne.isEmpty()) {
                Toast.makeText(this, "Unesite broj 0 ili 1", Toast.LENGTH_SHORT).show()
            } else if(ZeroOrOne != "0" && ZeroOrOne != "1") {
                Toast.makeText(this, "Dozvoljene vrijednosti su 0 i 1", Toast.LENGTH_SHORT).show()
            } else {
                startActivity(Intent(this, NoviActivity::class.java))
                //finish()
            }
        }
    }
}