package com.eng.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        val dugme = findViewById<Button>(R.id.login)
        dugme.setOnClickListener {
            logovanje = true
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}