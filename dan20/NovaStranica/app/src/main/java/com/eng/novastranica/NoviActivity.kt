package com.eng.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class NoviActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novi)

        val tv = findViewById<TextView>(R.id.textView)
        tv.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    //ako kliknemo sistemsko dugme (back dugme na telefonu)
    override fun onBackPressed() {
        startActivity(Intent(this, NazadActivity::class.java))  //novi
    }
}