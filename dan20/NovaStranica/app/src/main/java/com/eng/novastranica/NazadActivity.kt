package com.eng.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class NazadActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nazad)

        val idiNaLogovanje = findViewById<Button>(R.id.btnIdiNaLogovanje)
        idiNaLogovanje.setOnClickListener {
            logovanje = false
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}