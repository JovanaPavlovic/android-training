package com.eng.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

var logovanje = false

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(logovanje) {
            Log.d("LOGIN", "Korisnik je ulogovan")
        } else {
            Log.d("LOGIN", "Korisnik nije ulogovan")
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        val dugme = findViewById<Button>(R.id.idiNaDruguStranicu)
        dugme.setOnClickListener {
            startActivity(Intent(this, NoviActivity::class.java))
            finish()    //ugasi MainActivity da ne mozemo da se vratimo na njega
        }
    }
}