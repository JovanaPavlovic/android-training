package com.eng.domaci01

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell.view.*

class myAdapter(val podaci: ArrayList<Profesor>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return podaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naslovTextView.text = "Podaci o " + (position+1).toString() + ". profesoru:"
        holder.view.imeTextView.text = podaci[position].ime
        holder.view.prezimeTextView.text = podaci[position].prezime
        holder.view.godisteTextView.text = podaci[position].godiste.toString()
        holder.view.radnoMjestoTextView.text = podaci[position].radnoMjesto
        holder.view.gradTextView.text = podaci[position].grad
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
