package com.eng.domaci01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader

data class Profesor(val ime: String, val prezime: String, val godiste: Int, val radnoMjesto: String, val grad: String)

class MainActivity : AppCompatActivity() {

    val profesori: ArrayList<Profesor> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        parseCSV()

        val kreiraniAdapter = myAdapter(profesori)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        var isClicked = false
        val dugme = findViewById<Button>(R.id.dugme)
        dugme.setOnClickListener {
            if(!isClicked) {
                recyclerView.visibility = View.VISIBLE
                dugme.text = "Sakrij podatke"
            } else {
                recyclerView.visibility = View.GONE
                dugme.text = "Prikazi podatke"
            }

            isClicked = !isClicked
        }
    }

    private fun parseCSV() {
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))
        val citajLinijski = BufferedReader(otvoriCSV)

        while(citajLinijski.readLine().also { linija = it
                } != null) {
            val red: List<String> = linija!!.split(",")

            if(red[0].isEmpty()) break

            profesori.add(Profesor(red[0], red[1], red[2].toInt(), red[3], red[4]))
        }

    }
}