package com.eng.csvparselocal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.w3c.dom.Text
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.ObjectInputValidation

data class Student(val ime: String, val prezime: String, val mjesto: String, val godina: Int, val visina: Int, val prosjek: Double)

class MainActivity : AppCompatActivity() {

    var sviPodaci = ""
    val studenti: ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ispis = findViewById<TextView>(R.id.ispisPodataka)
        ispis.setOnClickListener {
            parseCSV()
            for(student in studenti) Log.d("ISPIS IMENA", student.ime)
        }
    }

    private fun parseCSV() {
        var linija: String?

        //InputStreamReader cita bajtove i pretvara ih u karaktere
        //open metod otvara fajl i cita karakter po karakter
        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        //procitali smo liniju pomocu readLine metode i to se smjestilo u it
        while(procitajLiniju.readLine().also { linija = it
                } != null) {
            val red: List<String> = linija!!.split(",")
            Log.d("CSV", red[0])

            if(red[0].isEmpty()) {
                Log.d("CSV", "Red je prazan")
            } else {
                for(i in 0..red.count()-1) sviPodaci += red[i] + " "
                sviPodaci += "\n"

                studenti.add(Student(red[0], red[1], red[2], red[3].toInt(), red[4].toInt(), red[5].toDouble()))

                val ispis = findViewById<TextView>(R.id.ispisPodataka)
                ispis.text = sviPodaci
            }
        }
    }
}