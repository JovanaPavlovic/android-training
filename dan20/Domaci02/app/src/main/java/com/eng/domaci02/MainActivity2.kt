package com.eng.domaci02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        Toast.makeText(this, "Welcome to MainActivity2.kt", Toast.LENGTH_SHORT).show()

        val idiNaMainActivity1 = findViewById<Button>(R.id.idiNaMainActivity1)
        idiNaMainActivity1.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        val idiNaMainActivity3 = findViewById<Button>(R.id.idiNaMainActivity3)
        idiNaMainActivity3.setOnClickListener {
            startActivity(Intent(this, MainActivity3::class.java))
        }

        val idiNaMainActivity4 = findViewById<Button>(R.id.idiNaMainActivity4)
        idiNaMainActivity4.setOnClickListener {
            startActivity(Intent(this, MainActivity4::class.java))
        }
    }
}