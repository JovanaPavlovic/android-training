package com.eng.domaci02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Toast.makeText(this, "Welcome to MainActivity.kt", Toast.LENGTH_SHORT).show()

        val idiNaMainActivity2 = findViewById<Button>(R.id.idiNaMainActivity2)
        idiNaMainActivity2.setOnClickListener {
            startActivity(Intent(this, MainActivity2::class.java))
            finish()
        }

        val idiNaMainActivity3 = findViewById<Button>(R.id.idiNaMainActivity3)
        idiNaMainActivity3.setOnClickListener {
            startActivity(Intent(this, MainActivity3::class.java))
            finish()
        }

        val idiNaMainActivity4 = findViewById<Button>(R.id.idiNaMainActivity4)
        idiNaMainActivity4.setOnClickListener {
            startActivity(Intent(this, MainActivity4::class.java))
            finish()
        }

    }
}