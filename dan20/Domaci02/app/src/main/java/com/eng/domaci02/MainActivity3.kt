package com.eng.domaci02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        Toast.makeText(this, "Welcome to MainActivity3.kt", Toast.LENGTH_SHORT).show()

        val idiNaMainActivity1 = findViewById<Button>(R.id.idiNaMainActivity1)
        idiNaMainActivity1.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        val idiNaMainActivity2 = findViewById<Button>(R.id.idiNaMainActivity2)
        idiNaMainActivity2.setOnClickListener {
            startActivity(Intent(this, MainActivity2::class.java))
        }

        val idiNaMainActivity4 = findViewById<Button>(R.id.idiNaMainActivity4)
        idiNaMainActivity4.setOnClickListener {
            startActivity(Intent(this, MainActivity4::class.java))
        }
    }
}