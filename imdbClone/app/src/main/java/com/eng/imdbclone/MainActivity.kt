package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://imdb-api.com/en/API/MostPopularMovies/k_r3vxbccu"
        getJSONfile().execute(jsonURL)

        GlobalData.functionsClassObject.fillSlider()
        GlobalData.functionsClassObject.fillFeaturedTodayList()

        val fragment = findNavController(R.id.fragment)
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNav.setupWithNavController(fragment)
    }

    inner class getJSONfile: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            getDataFromJSON(result)
        }
    }

    fun getDataFromJSON(json: String?) {
        val jsonObject = JSONObject(json)
        val movies = jsonObject.getJSONArray("items")

        movies.let {
            (0 until it.length()).forEach {
                if(it < GlobalData.maxMovieNumber) {
                    val movie = movies.getJSONObject(it)

                    val id = movie.getString("id")
                    val rank = movie.getString("rank")
                    val rankUpDown = movie.getString("rankUpDown")
                    val title = movie.getString("title")
                    val fullTitle = movie.getString("fullTitle")
                    val year = movie.getString("year")
                    val imageURL = movie.getString("image")
                    val crew = movie.getString("crew")
                    val imDbRating = movie.getString("imDbRating")
                    val imDbRatingCount = movie.getString("imDbRatingCount")

                    val image: Bitmap? = null
                    val movieObject = GlobalData.Movie(id, rank, rankUpDown, title, fullTitle, year, image,
                        crew, imDbRating, imDbRatingCount)

                    getImageFromJSON(movieObject).execute(imageURL.replace("original", "192x264"))
                }
            }
        }
    }

    inner class getImageFromJSON(val movie: GlobalData.Movie) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlAddress = params[0]

            try {
                val slikaSaInterneta = URL(urlAddress).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            movie.image = result
            GlobalData.movieList.add(movie)
        }
    }
}