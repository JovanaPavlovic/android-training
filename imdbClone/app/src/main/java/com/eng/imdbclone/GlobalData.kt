package com.eng.imdbclone

import android.graphics.Bitmap
import java.math.RoundingMode
import kotlin.random.Random

object GlobalData {
    val functionsClassObject = FunctionsClass()

    data class Movie(val id: String, val rank: String, val rankUpDown: String,
                     val title: String, val fullTitle: String, val year: String,
                     var image: Bitmap?, val crew: String, val imDbRating: String,
                     val imDbRatingCount: String)

    data class Slider(val image: Int, val video: Int, val title: String, val comment: String)

    data class FeaturedToday(val image: Int, val title: String)

    val movieList: ArrayList<Movie> = arrayListOf()
    val sliderList: ArrayList<Slider> = arrayListOf()
    val featuredTodayList: ArrayList<FeaturedToday> = arrayListOf()

    var sliderIsClicked = false

    val maxMovieNumber = 30
    var startIndex = 0
    var endIndex = 10
    val step = 10

    val firstSection = 1
    val secondSection = 2
    val thirdSection = 3
    val fourthSection = 4

    val featuredTodaySection = 1
    val fanFavoritesSection = 2

    val topPicksSection = 1
    val freeOnImdbSection = 2
    val streamingSection = 3
    val exploreMoviesAndTvSection = 4

    var seconds = Random.nextInt(60, 400)
    var profit = Random.nextDouble(2.0,15.0).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

    val maxTopBoxOfficeMovieNumber = 6
}