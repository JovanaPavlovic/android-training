package com.eng.imdbclone

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.Adapter
import android.widget.ProgressBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HomeFragment : Fragment() {

    var arrayIsFiled = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        looperFunc(view, savedInstanceState)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(arrayIsFiled) {
            val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)
            progressBar.visibility = View.GONE

            val homeFragmentData = view.findViewById<ConstraintLayout>(R.id.dataOnHomeFragment)
            homeFragmentData.visibility = View.VISIBLE

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.sliderRecyclerView, SliderAdapter(GlobalData.sliderList), true)

            val sliderRecyclerView = view.findViewById<RecyclerView>(R.id.sliderRecyclerView)
            GlobalData.functionsClassObject.changeSlide(sliderRecyclerView)

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.featuredTodayRecyclerView, HorizontalPostersAdapter(GlobalData.featuredTodayList, GlobalData.firstSection), true)

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.topPicksRecyclerView, JsonDataAdapter(GlobalData.movieList.subList(GlobalData.startIndex, GlobalData.endIndex), GlobalData.firstSection), true)
            GlobalData.functionsClassObject.changeIndex()

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.fanFavoritesRecyclerView, JsonDataAdapter(GlobalData.movieList.subList(GlobalData.startIndex, GlobalData.endIndex), GlobalData.secondSection), true)
            GlobalData.functionsClassObject.changeIndex()

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.freeOnImdbRecyclerView, HorizontalPostersAdapter(GlobalData.featuredTodayList, GlobalData.secondSection), true)

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.streamingRecyclerView, JsonDataAdapter(GlobalData.movieList.subList(GlobalData.startIndex, GlobalData.endIndex), GlobalData.thirdSection), true)
            GlobalData.functionsClassObject.changeIndex()

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.exploreMoviesAndTvRecyclerView, JsonDataAdapter(GlobalData.movieList.subList(GlobalData.startIndex, GlobalData.endIndex).asReversed(), GlobalData.fourthSection), true)
            GlobalData.functionsClassObject.changeIndex()

            GlobalData.functionsClassObject.callAdapter(requireActivity(), requireContext(), R.id.topBoxOfficeRecyclerView, TopBoxOfficeAdapter(GlobalData.movieList.subList(GlobalData.startIndex, GlobalData.startIndex + GlobalData.maxTopBoxOfficeMovieNumber)), false)
        }
    }

    fun looperFunc(view: View, savedInstanceState: Bundle?) {
        Handler(Looper.getMainLooper()).postDelayed({
            if(GlobalData.movieList.count() != GlobalData.maxMovieNumber) {
                looperFunc(view, savedInstanceState)
            } else {
                arrayIsFiled = true
                onViewCreated(view, savedInstanceState)
            }
        }, 100)
    }
}