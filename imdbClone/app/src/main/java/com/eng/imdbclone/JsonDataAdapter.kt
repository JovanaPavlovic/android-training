package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class JsonDataAdapter(val receivedData: MutableList<GlobalData.Movie>, val sectionChecker: Int) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.json_data_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val image = holder.view.findViewById<ImageView>(R.id.topPicksImage)
        image.setImageBitmap(receivedData[position].image)

        val rating = holder.view.findViewById<TextView>(R.id.ratingTextView)
        rating.text = receivedData[position].imDbRating
        if(receivedData[position].imDbRating == "") {
            val starImage = holder.view.findViewById<ImageView>(R.id.starImageView)
            starImage.visibility = View.GONE
        }

        val title = holder.view.findViewById<TextView>(R.id.movieTitleTextView)
        title.text = receivedData[position].title

        val year = holder.view.findViewById<TextView>(R.id.movieYearTextView)
        year.text = receivedData[position].year

        val button = holder.view.findViewById<Button>(R.id.topPicksButton)
        if (position % 2 == 0) {
            button.text = "+ Watchlist"
        } else {
            button.text = "Showtimes"
        }

        if(sectionChecker == GlobalData.topPicksSection) {
            val infoIcon = holder.view.findViewById<ImageView>(R.id.movieInfoImageView)
            infoIcon.visibility = View.VISIBLE
        } else if(sectionChecker == GlobalData.streamingSection) {
            button.text = "Watch now"
        } else if(sectionChecker == GlobalData.exploreMoviesAndTvSection) {
            button.text = "Showtimes"
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}