package com.eng.imdbclone

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Adapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.persistableBundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

class FunctionsClass {

    fun fillSlider() {
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm2, R.mipmap.sliderv2, "Greath White", "Official Trailer"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm3, R.mipmap.sliderv3, "IMDb Originals", "Reflected on Screen: Lin-Manuel Miranda and the 'In the Heights' Team"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm4, R.mipmap.sliderv4, "Trollhunters: Rise of the Titans", "Official Trailer"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm5, R.mipmap.sliderv5, "The Tomorrow War", "Final Trailer"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm1, R.mipmap.sliderv1, "Till Death", "Official Trailer"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm6, R.mipmap.sliderv6, "Gunpowder Milkshake", "Official Trailer"))
        GlobalData.sliderList.add(GlobalData.Slider(R.mipmap.sliderm7, R.mipmap.sliderv7, "IMDb on the Scene - Interview", "The \"Panic\" Cast Share the Things That Truly Scared Them"))
    }

    fun fillFeaturedTodayList() {
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image4, "What to Watch: Important Black Stories"))
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image1, "LGBTQ+ Power Couples of Hollywood"))
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image3, "Streaming and DVD/Blu-ray Release Dates"))
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image2, "Remembering Ned Beatty (1937-2021)"))
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image5, "The Latest Red Carpet Photos and More"))
        GlobalData.featuredTodayList.add(GlobalData.FeaturedToday(R.mipmap.featured_today_image6, "All the Latest Movie and TV Posters"))
    }

    var position = 1
    fun changeSlide(sliderRecyclerView: RecyclerView) {
        if(GlobalData.sliderIsClicked == false) {
            Handler(Looper.getMainLooper()).postDelayed({
                if(position == GlobalData.sliderList.count()) {
                    position = 0
                }

                sliderRecyclerView.smoothScrollToPosition(position)
                position++

                changeSlide(sliderRecyclerView)
            }, 4000)
        }
    }

    fun callAdapter(activity: Activity, context: Context, recyclerViewId: Int, adapter: RecyclerView.Adapter<*>?, isLinearLayoutManager: Boolean) {
        val recyclerView = activity.findViewById<RecyclerView>(recyclerViewId)
        recyclerView.adapter = adapter

        if(isLinearLayoutManager) {
            recyclerView.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
        } else {
            recyclerView.setLayoutManager(GridLayoutManager(context, 1))
        }

    }

    fun changeIndex() {
        GlobalData.startIndex += GlobalData.step
        GlobalData.endIndex += GlobalData.step

        if(GlobalData.endIndex > GlobalData.maxMovieNumber) {
            GlobalData.startIndex = 0
            GlobalData.endIndex = GlobalData.step
        }
    }
}