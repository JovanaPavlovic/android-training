package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import java.math.RoundingMode
import kotlin.random.Random

class TopBoxOfficeAdapter(val receivedData: MutableList<GlobalData.Movie>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.top_box_office_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if(position < 6) {
            val index = holder.view.findViewById<TextView>(R.id.indexInTopBoxOffice)
            index.text = (position + 1).toString()

            val movie = holder.view.findViewById<TextView>(R.id.movieInTopBoxOffice)
            movie.text = receivedData[position].title

            val profit = holder.view.findViewById<TextView>(R.id.profitInTopBoxOffice)
            profit.text = "$" + GlobalData.profit + "M"
            GlobalData.profit = Random.nextDouble(2.0,15.0).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}