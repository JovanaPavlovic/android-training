package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SliderAdapter(val receivedData: ArrayList<GlobalData.Slider>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.slider_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val videoFromSlider = holder.view.findViewById<ImageView>(R.id.videoSliderImageView)
        videoFromSlider.setImageResource(receivedData[position].video)

        val imageFromSlider = holder.view.findViewById<ImageView>(R.id.imageSliderImageView)
        imageFromSlider.setImageResource(receivedData[position].image)

        val commentFromSlider = holder.view.findViewById<TextView>(R.id.commentSliderTextView)
        commentFromSlider.text = receivedData[position].comment

        val titleFromSlider = holder.view.findViewById<TextView>(R.id.titleSliderTextView)
        titleFromSlider.text = receivedData[position].title

        holder.view.setOnClickListener {
            GlobalData.sliderIsClicked = true
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
