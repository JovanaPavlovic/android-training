package com.eng.imdbclone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation

var firstEnter = true

class SignInFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        val notNowTextView = view.findViewById<TextView>(R.id.notNowTextView)
        notNowTextView.setOnClickListener {
            firstEnter = false
            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_homeFragment)
        }

        val conditionsOfUse = view.findViewById<TextView>(R.id.conditionsOfUseTextView)
        conditionsOfUse.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_conditionsOfUseFragment)
        }

        val createAccountBtn = view.findViewById<Button>(R.id.createAccountBtn)
        createAccountBtn.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_createAccountFragment)
        }

        if(!firstEnter) {
            val logo = view.findViewById<ImageView>(R.id.imdbIcon)
            logo.visibility = View.GONE

            val title = view.findViewById<TextView>(R.id.loginTitle)
            title.text = "Unlock all that IMDb has to offer"

            notNowTextView.visibility = View.GONE

            val signInConstraintLayout = view.findViewById<ConstraintLayout>(R.id.signInConstraintLayout)
            signInConstraintLayout.setPadding(0, 90, 0, 0)
        }

        return view
    }
}