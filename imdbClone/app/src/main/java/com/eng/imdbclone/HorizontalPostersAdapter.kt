package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.marginLeft
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

var index = -1

class HorizontalPostersAdapter(val receivedData: MutableList<GlobalData.FeaturedToday>, val sectionChecker: Int) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.horizontal_posters_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if(sectionChecker == GlobalData.featuredTodaySection) {
            index = position
        } else if(sectionChecker == GlobalData.freeOnImdbSection) {
            index = receivedData.count() - position - 1
        }

        val image = holder.view.findViewById<ImageView>(R.id.horizontalPosterImageView)
        image.setImageResource(receivedData[index].image)

        val title = holder.view.findViewById<TextView>(R.id.titleForHorizontalPoster)
        title.text = receivedData[index].title

        val videoDuration = holder.view.findViewById<TextView>(R.id.videoDurationInHorizontalPoster)
        val playButton = holder.view.findViewById<ImageView>(R.id.horizontalPosterPlayButtonImageView)
        if(sectionChecker == GlobalData.featuredTodaySection) {
            videoDuration.visibility = View.GONE
            playButton.visibility = View.GONE
        } else if(sectionChecker == GlobalData.freeOnImdbSection) {
            videoDuration.text = "" + GlobalData.seconds/60 + ":" + GlobalData.seconds.mod(60)
            GlobalData.seconds = Random.nextInt(60, 400)
        }

        if(position == 0) {
            val dpToPxLeft = (15 * holder.view.context.resources.displayMetrics.density + 0.5f).toInt()
            image.setPadding(dpToPxLeft, 0, 0, 0)
            title.setPadding(dpToPxLeft, 0, 0, 0)

            val layoutParams = playButton.layoutParams as ConstraintLayout.LayoutParams
            layoutParams.setMargins(2*dpToPxLeft, 0, 0, dpToPxLeft)
            playButton.layoutParams = layoutParams
        } else if(position == receivedData.count()-1) {
            val dpToPxRight = (5 * holder.view.context.resources.displayMetrics.density + 0.5f).toInt()
            image.setPadding(0, 0, dpToPxRight, 0)
            title.setPadding(0, 0, dpToPxRight, 0)
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}