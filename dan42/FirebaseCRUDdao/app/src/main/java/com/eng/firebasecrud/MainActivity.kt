package com.eng.firebasecrud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var student: Student

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dao = DAOStudent()

        val addBtn = findViewById<Button>(R.id.addBtn)
        val deleteBtn = findViewById<Button>(R.id.deleteBtn)
        val updateBtn = findViewById<Button>(R.id.updateBtn)
        val getBtn = findViewById<Button>(R.id.getBtn)
        val studentInfo = findViewById<TextView>(R.id.chosenStudentInfo)

        addBtn.setOnClickListener {
            student = dao.createStudent(this)
            println(student.firstname + " " +  student.lastname + " "  + student.index)

            studentInfo.visibility = View.GONE

            dao.add(student).addOnSuccessListener {
                Toast.makeText(this, "Student is inserted", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT inserted", Toast.LENGTH_SHORT).show()
            }
        }

        updateBtn.setOnClickListener {
            student = dao.createStudent(this)
            studentInfo.visibility = View.GONE

            val mapa: MutableMap<String, Any> = mutableMapOf()
            mapa.put("firstname", student.firstname)
            mapa.put("lastname", student.lastname)
            mapa.put("index", student.index)

            dao.update(student, mapa).addOnSuccessListener {
                Toast.makeText(this, "Student is updated", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT updated", Toast.LENGTH_SHORT).show()
            }
        }

        deleteBtn.setOnClickListener {
            student = dao.createStudent(this)
            studentInfo.visibility = View.GONE

            dao.delete(student).addOnSuccessListener {
                Toast.makeText(this, "Student is deleted", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT deleted", Toast.LENGTH_SHORT).show()
            }
        }

        getBtn.setOnClickListener {
            student = dao.createStudent(this)
            studentInfo.visibility = View.VISIBLE

            dao.get(student).addOnSuccessListener {
                if(it.exists()) {
                    Toast.makeText(this, "Student is retrieved", Toast.LENGTH_SHORT).show()

                    studentInfo.text = "Firstname: " + it.child("firstname").value +
                            "\nLastname: " + it.child("lastname").value +
                            "\nIndex: " + it.child("index").value
                } else {
                    Toast.makeText(this, "Student doesn't exists", Toast.LENGTH_SHORT).show()
                }
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT retrieved", Toast.LENGTH_SHORT).show()
            }
        }
    }
}