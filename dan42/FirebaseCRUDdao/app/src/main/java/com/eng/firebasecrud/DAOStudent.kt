package com.eng.firebasecrud

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.EditText
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class DAOStudent {

    val databaseReference: DatabaseReference = FirebaseDatabase.getInstance().getReference(Student::class.java.simpleName)

    fun add(student: Student): Task<Void> {
        return databaseReference.child(student.index).setValue(student)
    }

    fun update(student: Student, map: MutableMap<String, Any>): Task<Void> {
        return databaseReference.child(student.index).updateChildren(map)

        //moze i pomocu setValue
    }

    fun delete(student: Student): Task<Void> {
        return databaseReference.child(student.index).removeValue()

        //2.NACIN
        //return databaseReference.child(student.firstname).setValue(null)
    }

    fun get(student: Student): Task<DataSnapshot> {
        return databaseReference.child(student.index).get()
    }

    fun createStudent(activity: Activity): Student {
        val firstname = activity.findViewById<EditText>(R.id.firstname)
        val lastname = activity.findViewById<EditText>(R.id.lastname)
        val index = activity.findViewById<EditText>(R.id.index)

        return Student(firstname.text.toString(), lastname.text.toString(), index.text.toString())
    }
}