package com.eng.firebasecrud2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {

    val databaseReference: DatabaseReference = FirebaseDatabase.getInstance().getReference(Student::class.java.simpleName)
    lateinit var student: Student

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val addBtn = findViewById<Button>(R.id.addBtn)
        val deleteBtn = findViewById<Button>(R.id.deleteBtn)
        val updateBtn = findViewById<Button>(R.id.updateBtn)
        val getBtn = findViewById<Button>(R.id.getBtn)
        val studentInfo = findViewById<TextView>(R.id.chosenStudentInfo)

        addBtn.setOnClickListener {
            student = createStudent()
            studentInfo.visibility = View.GONE

            databaseReference.child(student.email).setValue(student).addOnSuccessListener {
                Toast.makeText(this, "Student is inserted", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT inserted", Toast.LENGTH_SHORT).show()
            }
        }

        updateBtn.setOnClickListener {
            student = createStudent()
            studentInfo.visibility = View.GONE

            val mapa: MutableMap<String, Any> = mutableMapOf()
            mapa.put("email", student.email)
            mapa.put("firstname", student.firstname)
            mapa.put("lastname", student.lastname)
            mapa.put("index", student.index)
            mapa.put("grades", student.grades)
            mapa.put("average", student.average)

            //moze i pomocu setValue
            databaseReference.child(student.email).updateChildren(mapa).addOnSuccessListener {
                Toast.makeText(this, "Student is updated", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT updated", Toast.LENGTH_SHORT).show()
            }
        }

        deleteBtn.setOnClickListener {
            student = createStudent()
            studentInfo.visibility = View.GONE

            //databaseReference.child(student.firstname).setValue(null)  (2.nacin)
            databaseReference.child(student.email).removeValue().addOnSuccessListener {
                Toast.makeText(this, "Student is deleted", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT deleted", Toast.LENGTH_SHORT).show()
            }
        }

        getBtn.setOnClickListener {
            student = createStudent()
            studentInfo.visibility = View.VISIBLE

            databaseReference.child(student.email).get().addOnSuccessListener {
                if(it.exists()) {
                    Toast.makeText(this, "Student is retrieved", Toast.LENGTH_SHORT).show()

                    studentInfo.text = "Email: " + it.child("email").value +
                            "\nFirstname: " + it.child("firstname").value +
                            "\nLastname: " + it.child("lastname").value +
                            "\nIndex: " + it.child("index").value +
                            "\nGrades: " + it.child("grades").value +
                            "\nAverage: " + it.child("average").value
                } else {
                    Toast.makeText(this, "Student doesn't exists", Toast.LENGTH_SHORT).show()
                }
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT retrieved", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun createStudent(): Student {
        val emailInput = findViewById<EditText>(R.id.email)
        val firstname = findViewById<EditText>(R.id.firstname)
        val lastname = findViewById<EditText>(R.id.lastname)
        val index = findViewById<EditText>(R.id.index)
        val gradesInput = findViewById<EditText>(R.id.grades)

        val grades: List<String> = gradesInput.text.split(" ")
        val email = emailInput.text.dropLast(4).toString()

        var gradeSum = 0.0
        for(grade in grades) {
            gradeSum += grade.toDouble()
        }
        val average = (gradeSum / grades.count()).toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

        return Student(email, firstname.text.toString(),
            lastname.text.toString(), index.text.toString(), grades, average)
    }
}