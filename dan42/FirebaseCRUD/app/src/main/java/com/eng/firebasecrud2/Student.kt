package com.eng.firebasecrud2

data class Student(val email: String, val firstname: String, val lastname: String,
                   val index: String, val grades: List<String>, val average: Double) {
}