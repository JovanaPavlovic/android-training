package com.eng.firebaseauthdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val receivedUserId = intent.getStringExtra("user_id")
        val receivedEmailId = intent.getStringExtra("email_id")

        val userId = findViewById<TextView>(R.id.userid)
        val emailId = findViewById<TextView>(R.id.emailid)
        val logoutBtn = findViewById<Button>(R.id.logoutButton)

        userId.text = "User ID :: $receivedUserId"
        emailId.text = "Email ID :: $receivedEmailId"

        logoutBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}