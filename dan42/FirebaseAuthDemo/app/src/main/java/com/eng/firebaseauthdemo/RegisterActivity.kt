package com.eng.firebaseauthdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val registerBtn = findViewById<Button>(R.id.registerBtn)
        val email = findViewById<EditText>(R.id.emailRegister)
        val pass = findViewById<EditText>(R.id.passRegister)

        registerBtn.setOnClickListener {
            when {
                TextUtils.isEmpty(email.text.toString().trim {it <= ' '}) -> {
                    Toast.makeText(this@RegisterActivity, "Please enter email.", Toast.LENGTH_SHORT).show()
                }

                TextUtils.isEmpty(pass.text.toString().trim {it <= ' '}) -> {
                    Toast.makeText(this@RegisterActivity, "Please enter password.", Toast.LENGTH_SHORT).show()
                }

                else -> {
                    val trimedEmail = email.text.toString().trim { it <= ' ' }
                    val trimedPass = pass.text.toString().trim { it <= ' ' }

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(trimedEmail, trimedPass)
                        .addOnCompleteListener({ task ->
                            if(task.isSuccessful) {
                                val firebaseUser: FirebaseUser = task.result!!.user!!

                                Toast.makeText(this@RegisterActivity, "You are registered successfully.", Toast.LENGTH_SHORT).show()

                                val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra("user_id", firebaseUser.uid)
                                intent.putExtra("email_id", trimedEmail)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(this@RegisterActivity, task.exception!!.message.toString(), Toast.LENGTH_SHORT).show()
                            }
                        })
                }
            }
        }
    }
}