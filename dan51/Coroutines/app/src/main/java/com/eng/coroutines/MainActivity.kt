package com.eng.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.MalformedURLException
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txt = findViewById<TextView>(R.id.textView)

        val url: URL? = try {
            URL("https://evolutiondoo.com/localJSON.json")
        } catch(e: MalformedURLException) {
            Log.d("Exception", e.toString())
            null
        }

        //IO, Default, Main
        txt.setOnClickListener {
            url?.let { txt.text = it.toString() }

            //tek kada se prvi scope zavrsi, poziava se sledeci itd.
            lifecycleScope.launch(Dispatchers.IO) {
                url?.getString()?.apply {
                    withContext(Dispatchers.Default) {
                        val list = parseJSON(this@apply)
                        withContext(Dispatchers.Main) {
                            txt.text = "\nReading data from JSON..\n\n"

                            list.forEach {
                                txt.append("\n${it.ime} ${it.prezime} ${it.mjesto} ${it.godiste} ${it.visina}")
                            }
                        }
                    }
                }
            }
        }
    }

    class Student(val ime: String, val prezime: String, val mjesto: String, val godiste: String, val visina: String)

    //preuzima string sa url-a
    fun URL.getString(): String? {
        val stream = openStream()

        return try {
            val readedString = BufferedReader(InputStreamReader(stream))
            val result = StringBuilder()
            var line: String?

            while (readedString.readLine().also {
                line = it
                } != null) {
                result.append(line).appendLine()
            }

            result.toString()
        } catch (e: IOException) {
            e.toString()
        }
    }

    fun parseJSON(data: String): List<Student> {
        val list = mutableListOf<Student>()

        try {
            val array = JSONArray(data)
            for(i in 0 until array.length()) {
                val jsonObjekat = JSONObject(array[i].toString())

                val ime = jsonObjekat.getString("ime")
                val prezime = jsonObjekat.getString("prezime")
                val mjesto = jsonObjekat.getString("mjesto")
                val godiste = jsonObjekat.getString("godiste")
                val visina = jsonObjekat.getString("visina")

                list.add(Student(ime, prezime, mjesto, godiste, visina))
            }
        } catch (e: JSONException) {
            Log.d("Exception", e.toString())
        }

        return list
    }
}