package com.eng.sqlitedatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val context = this
        var db = DatabaseHandler(context)

        insertData.setOnClickListener {
            if(userName.text.toString().length > 0 && userAge.text.toString().length > 0) {
                var user = User(userName.text.toString(), userAge.text.toString().toInt())
                db.insertData(user)
            }
        }

        readDatabase.setOnClickListener {
            var data = db.readData()
            showResults.text = ""

            for(i in 0..(data.size-1)) {
                showResults.append(data.get(i).id.toString() + " " + data.get(i).name + " " + data.get(i).age.toString() + "\n")
            }
        }
    }
}