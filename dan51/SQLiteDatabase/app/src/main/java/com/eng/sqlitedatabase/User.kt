package com.eng.sqlitedatabase

class User {
    var id: Int = 0     //id ce automatski da se povecava za 1
    var name: String = ""
    var age: Int = 0

    constructor(name: String, age: Int) {
        this.name = name
        this.age = age
    }

    constructor()
}