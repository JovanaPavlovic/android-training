package com.eng.tablelayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    data class People(val ime: String, val prezime: String, val godine: Int)
    var tableObjects: ArrayList<People> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tableView.apply {
            isStretchAllColumns = true
        }

        createObjectSource()
        for(osoba in tableObjects) {
            Log.d("OSOBA", osoba.ime)
        }

        addObjectToTable()
    }

    fun addObjectToTable() {
        for(singleObject in tableObjects) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER_HORIZONTAL

            for(i in 1..3) {
                val textView = TextView(this)
                textView.textSize = 30f
                textView.gravity = Gravity.CENTER_HORIZONTAL
                textView.setPadding(20)
                textView.apply {
                    if(i == 1) textView.text = singleObject.ime
                    else if(i == 2) textView.text = singleObject.prezime
                    else textView.text = singleObject.godine.toString()
                }

                row.addView(textView)
            }
            tableView.addView(row)
        }
    }

    private fun createObjectSource() {
        tableObjects.add(People("Mihajlo", "Jezdic", 45))
        tableObjects.add(People("Nikola", "Prodanovic", 35))
        tableObjects.add(People("Jovana", "Pavlovic", 24))
    }
}