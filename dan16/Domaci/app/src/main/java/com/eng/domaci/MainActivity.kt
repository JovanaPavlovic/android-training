package com.eng.domaci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.marginLeft
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    data class People(val name: String, val surname: String, val slika: Int)
    val peoples: ArrayList<People> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        table.apply {
            isStretchAllColumns = true
        }

        fillInArray()
        showInTable()

        add_button.setOnClickListener {
            table.removeAllViews()

            val year = Integer.parseInt(imageEditText.text.toString())
            val person = People(nameEditText.text.toString(), surnameEditText.text.toString(), year)

            peoples.add(person)
            showInTable()
        }
    }

    fun showInTable() {
        for(people in peoples) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER_HORIZONTAL

            for(i in 1..3) {
                val image = ImageView(this)
                image.setImageResource(people.slika)
                image.setLayoutParams(TableRow.LayoutParams(200, 200))

                val textView = TextView(this)
                textView.gravity = Gravity.CENTER_HORIZONTAL
                textView.setPadding(10)
                textView.textSize = 20f
                textView.apply {
                    if(i == 1) textView.text = people.name
                    else if(i == 2) textView.text = people.surname
                    //else textView.text = people.name
                }

                if(i == 3) {
                    row.addView(image)
                } else {
                    row.addView(textView)
                }
            }

            table.addView(row)
        }
    }

    fun fillInArray() {
        peoples.add(People("Jovana", "Pavlović", R.drawable.logo))
        peoples.add(People("Nemanja", "Gavrić", R.drawable.logo))
        peoples.add(People("Marija", "Vasić", R.drawable.logo))
    }
}