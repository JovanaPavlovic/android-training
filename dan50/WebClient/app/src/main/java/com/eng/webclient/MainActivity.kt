package com.eng.webclient

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

val URL = "https://www.audi.it"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val chrome = findViewById<TextView>(R.id.toChrome)
        val web = findViewById<TextView>(R.id.toWebView)

        chrome.setOnClickListener {
            val uri = Uri.parse("googlechrome://navigate?url=" + URL)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

        web.setOnClickListener {
            val intent = Intent(this, WebActivity::class.java)
            startActivity(intent)
        }
    }
}