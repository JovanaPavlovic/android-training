package com.eng.builddialog

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_layout.*

class Dialog(private val title: String?, private val message: String?,
             private val positiveButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>?,
             private val negativeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>?)
    : DialogFragment() {

    class Builder {
        var title: String? = null
        var message: String? = null
        var positiveButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>? = null
            private set

        var negativeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>? = null
            private set

        fun setNegativeButton(
            text: String,
            listener: View.OnClickListener,
            buttonType: DialogButtonType
        ) {
            negativeButtonTriple = Triple(text, listener, buttonType)
        }

        fun setPositiveButton(
            text: String,
            listener: View.OnClickListener,
            buttonType: DialogButtonType
        ) {
            positiveButtonTriple = Triple(text, listener, buttonType)
        }

        fun build() = Dialog(this)
    }

    private constructor(builder: Builder) : this(
        builder.title,
        builder.message,
        builder.positiveButtonTriple,
        builder.negativeButtonTriple
    )

    //da mozemo da pozovemo build iz Activity-ja
    companion object {
        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }

    //pretvara xml u View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_layout, container, false)

    override fun onStart() {
        super.onStart()
        dialog?.let { dialog ->
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT

            dialog.window?.let {
                it.apply {
                    setLayout(width, height)
                    setBackgroundDrawableResource(R.drawable.dialog_background)
                }
            }
        }
    }

    private fun Button.setTypeStyle(type: DialogButtonType) {
        when(type) {
            DialogButtonType.PRIMARY -> {
                this.apply {
                    setBackgroundColor(Color.parseColor("#990000"))
                    setTextColor(ContextCompat.getColor(context, R.color.white))
                }
            }
            DialogButtonType.SECONDARY -> {
                this.apply {
                    setBackgroundColor(Color.parseColor("#c1c1c1"))
                    setTextColor(ContextCompat.getColor(context, R.color.teal_200))
                }
            }
        }
    }

    private fun Button.setupAsDialogButton(buttonText: String?, clickListener: View.OnClickListener?) {
        if(buttonText != null && clickListener != null) {
            this.apply {
                text = buttonText
                visibility = View.VISIBLE
                setOnClickListener {
                    clickListener.onClick(this)
                    dismiss()
                }
            }
        } else {
            Log.d("BUTTON", "Greska prilikom klika")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleDialog.text = title
        messageDialog.text = message

        positiveButtonTriple.let {
            if (positiveButtonTriple != null) {
                positiveButton.setupAsDialogButton(positiveButtonTriple.first, positiveButtonTriple.second)
                positiveButton.setTypeStyle(positiveButtonTriple.third)
            }
        }

        negativeButtonTriple.let {
            if (negativeButtonTriple != null) {
                negativeButton.setupAsDialogButton(negativeButtonTriple.first, negativeButtonTriple.second)
                negativeButton.setTypeStyle(negativeButtonTriple.third)
            }
        }
    }
}
