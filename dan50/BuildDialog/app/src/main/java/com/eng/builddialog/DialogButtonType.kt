package com.eng.builddialog

enum class DialogButtonType {
    PRIMARY,
    SECONDARY
}