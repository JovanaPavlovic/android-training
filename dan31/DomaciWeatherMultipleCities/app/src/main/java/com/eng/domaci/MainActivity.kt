package com.eng.domaci

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Data.functionsClassObject.fillCitiesPage(this, this)

        val showBtn = findViewById<Button>(R.id.showButton)
        showBtn.setOnClickListener {
            hideKeyboard(it)

            if(Data.chosenCity.isNotEmpty()) {
                val citiesConstraintLayout = findViewById<ConstraintLayout>(R.id.citiesConstraintLayout)
                citiesConstraintLayout.visibility = View.GONE

                val weatherConstraintLayout = findViewById<ConstraintLayout>(R.id.weatherForecastConstraintLayout)
                weatherConstraintLayout.visibility = View.VISIBLE

                val jsonURL = "https://api.openweathermap.org/data/2.5/forecast?q=" + Data.chosenCity + "&appid=356292f8304f3c240d87417fc743ddb0"
                preuzmiJSON().execute(jsonURL)
            } else {
                Toast.makeText(this, "You must choose city", Toast.LENGTH_LONG).show()
            }
        }
    }

    inner class preuzmiJSON(): AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String = ""
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            //var inputStream: InputStream? = null

            try {
                //connection.getInputStream()

                connection.connect()

                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } catch (e: Exception) {
                //connection.getErrorStream()

                Log.d("EXCEPTION", "Nije dobar url")
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if(result!!.isEmpty()) {
                Log.d("EMPTY RESULT", "Result is empty")
            } else {
                Data.functionsClassObject.procitajPodatke(result)
                pozoviFunk()
            }

        }
    }

    fun pozoviFunk() {
        Data.functionsClassObject.fillData(this, this)
    }

    fun hideKeyboard(view: View) {
        val zatvori = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        zatvori.hideSoftInputFromWindow(view.windowToken, 0)
    }
}