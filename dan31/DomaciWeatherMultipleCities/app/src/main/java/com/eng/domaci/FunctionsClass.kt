package com.eng.domaci

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject

class FunctionsClass {

    fun procitajPodatke(preuzetiJSON: String?) {
        val weatherListArray: ArrayList<Data.WeatherList> = arrayListOf()

        val jsonObject  = JSONObject(preuzetiJSON)

        val cod = jsonObject.getString("cod")
        val message = jsonObject.getInt("message")
        val cnt = jsonObject.getInt("cnt")

        val list = jsonObject.getJSONArray("list")
        list.let {
            (0 until it.length()).forEach {
                val listObject = list.getJSONObject(it)

                val dt = listObject.getInt("dt")

                val main = listObject.getJSONObject("main")
                val temp = main.getDouble("temp")
                val feels_like = main.getDouble("feels_like")
                val temp_min = main.getDouble("temp_min")
                val temp_max = main.getDouble("temp_max")
                val pressure = main.getInt("pressure")
                val sea_level = main.getInt("sea_level")
                val grnd_level = main.getInt("grnd_level")
                val humidity = main.getInt("humidity")
                val temp_kf = main.getDouble("temp_kf")

                val weather = listObject.getJSONArray("weather")
                var id = 0
                var mainW = ""
                var description = ""
                var icon = ""
                weather.let {
                    (0 until it.length()).forEach {
                        val weatherObject = weather.getJSONObject(it)

                        id = weatherObject.getInt("id")
                        mainW = weatherObject.getString("main")
                        description = weatherObject.getString("description")
                        icon = weatherObject.getString("icon")
                    }
                }

                val clouds = listObject.getJSONObject("clouds")
                val all = clouds.getInt("all")

                val wind = listObject.getJSONObject("wind")
                val speed = wind.getDouble("speed")
                val deg = wind.getInt("deg")
                val gust = wind.getDouble("gust")

                val visibility = listObject.getInt("visibility")
                val pop = listObject.getInt("pop")

                val sys = listObject.getJSONObject("sys")
                val pod = sys.getString("pod")

                val dt_txt = listObject.getString("dt_txt")

                weatherListArray.add(
                    Data.WeatherList(dt, temp, feels_like, temp_min, temp_max, pressure, sea_level, grnd_level,
                        humidity, temp_kf, description, all, speed, deg, gust, visibility, pop, pod, dt_txt))
            }
        }

        val city = jsonObject.getJSONObject("city")
        val idC = city.getInt("id")
        val name = city.getString("name")

        val coord = city.getJSONObject("coord")
        val lat = coord.getDouble("lat")
        val lon = coord.getDouble("lon")

        val country = city.getString("country")
        val population = city.getInt("population")
        val timezone = city.getInt("timezone")
        val sunrise = city.getInt("sunrise")
        val sunset = city.getInt("sunset")

        Data.weatherData = Data.Weather(cod, message, cnt, weatherListArray, name, lat, lon, country, population, timezone, sunrise, sunset)
    }

    val filteredWeatherListArray: ArrayList<Data.WeatherList> = arrayListOf()

    fun fillData(activity: Activity, context: Context) {
        val cityName = activity.findViewById<TextView>(R.id.nameTextView)
        cityName.text = Data.weatherData.city_name + ", " + Data.weatherData.country

        val latLon = activity.findViewById<TextView>(R.id.latLonTextView)
        latLon.text = "Lat: " + Data.weatherData.lat + " / " + "Lon: " + Data.weatherData.lon

        val timezone = activity.findViewById<TextView>(R.id.timezoneTextView)
        timezone.text = "Timezone: " + Data.weatherData.timezone

        val population = activity.findViewById<TextView>(R.id.populationTextView)
        population.text = "Population: " + Data.weatherData.population

        val sunrise = activity.findViewById<TextView>(R.id.sunriseTextView)
        sunrise.text = "Sunrise: " + Data.weatherData.sunrise + " AM"

        val sunset = activity.findViewById<TextView>(R.id.sunsetTextView)
        sunset.text = "Sunset: " + Data.weatherData.sunset + " PM"

        fillArray("2021-06-10")
        var recyclerView = activity.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = Adapter(filteredWeatherListArray)
        recyclerView.setLayoutManager(GridLayoutManager(context, 1))

        for(weather in Data.weatherData.weatherListArray) {
            if(weather.description == "broken clouds") {
                if(!Data.shown) {
                    Data.shown = true
                    context.startActivity(Intent(context, AlertActivity::class.java))
                }
            }
        }

        val firstBtn = activity.findViewById<Button>(R.id.firstBtn)
        val secondBtn = activity.findViewById<Button>(R.id.secondBtn)
        val thirdBtn = activity.findViewById<Button>(R.id.thirdBtn)
        val fourthBtn = activity.findViewById<Button>(R.id.fourthBtn)
        val fifthbtn = activity.findViewById<Button>(R.id.fifthBtn)

        firstBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-10")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        secondBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-11")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        thirdBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-12")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        fourthBtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-13")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }

        fifthbtn.setOnClickListener {
            filteredWeatherListArray.clear()
            fillArray("2021-06-14")
            recyclerView.adapter = Adapter(filteredWeatherListArray)
        }
    }

    fun fillArray(datum: String) {
        for (elem in Data.weatherData.weatherListArray) {
            if (elem.dt_txt.contains(datum)) {
                filteredWeatherListArray.add(elem)
            }
        }
    }

    fun chooseImage(description: String) : Int {
        if(description == "broken clouds") return R.drawable.broken_clouds
        else if(description == "clear sky") return R.drawable.clear_sky
        else if(description == "few clouds") return R.drawable.few_clouds
        else if(description == "light rain") return R.drawable.light_rain
        else if(description == "overcast clouds") return R.drawable.overcast_clouds
        else if(description == "scattered clouds") return R.drawable.scattered_clouds
        else return 0
    }

    val listOfCities: ArrayList<String> = arrayListOf()
    val filteredCities: ArrayList<String> = arrayListOf()

    fun fillCitiesPage(activity: Activity, context: Context) {
        fillListOfCities()

        val recyclerView = activity.findViewById<RecyclerView>(R.id.cityRecyclerView)
        recyclerView.adapter = CityAdapter(listOfCities)
        recyclerView.setLayoutManager(GridLayoutManager(context, 3))

        val search = activity.findViewById<EditText>(R.id.cityEditText)
        search.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filteredCities.clear()
                for(city in listOfCities) {
                    if(city.lowercase().contains(search.text.toString().lowercase())) {
                        filteredCities.add(city)
                    }
                }
                recyclerView.adapter = CityAdapter(filteredCities)
            }
        })
    }

    fun fillListOfCities() {
        listOfCities.add("Paris")
        listOfCities.add("London")
        listOfCities.add("Lisabon")
        listOfCities.add("Moskow")
        listOfCities.add("Belgrade")
        listOfCities.add("Zagreb")
        listOfCities.add("Sarajevo")
        listOfCities.add("Skopje")
        listOfCities.add("Berlin")
        listOfCities.add("Madrid")
        listOfCities.add("Budapest")
        listOfCities.add("Minsk")
        listOfCities.add("Stockholm")
    }
}