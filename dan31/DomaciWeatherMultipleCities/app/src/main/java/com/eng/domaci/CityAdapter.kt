package com.eng.domaci

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CityAdapter(val receivedData: ArrayList<String>) : RecyclerView.Adapter<CustomHolderView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolderView {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.city_cell, parent, false)
        return CustomHolderView(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomHolderView, position: Int) {
        val city = holder.view.findViewById<TextView>(R.id.cityTextView)
        city.text = receivedData[position]

        city.setOnClickListener {
            Data.chosenCity = receivedData[position]
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}