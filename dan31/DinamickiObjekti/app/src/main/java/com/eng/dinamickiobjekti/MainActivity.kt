package com.eng.dinamickiobjekti

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast

data class Dugmad(val ime: String, val vrijednost: String)

class MainActivity : AppCompatActivity() {

    val niz: ArrayList<Dugmad> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        niz.add(Dugmad("dugme1", "ispis dugme 1"))
        niz.add(Dugmad("dugme2", "ispis dugme 2"))
        niz.add(Dugmad("dugme3", "ispis dugme 3"))
        niz.add(Dugmad("dugme4", "ispis dugme 4"))
        niz.add(Dugmad("dugme5", "ispis dugme 5"))
        niz.add(Dugmad("dugme6", "ispis dugme 6"))
        niz.add(Dugmad("dugme7", "ispis dugme 7"))

        val layout = findViewById<LinearLayout>(R.id.layout)

        for(i in niz) {
            val button = Button(this)
            button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

            button.text = i.ime
            button.setOnClickListener {
//                napraviSliku()
                Toast.makeText(this, i.vrijednost, Toast.LENGTH_LONG).show()
            }

            button.x = 300f
            button.y = 500f

            layout.addView(button)
        }
    }

    fun napraviSliku() {
        val layout = findViewById<LinearLayout>(R.id.layout)

        val image = ImageView(this)
        layout.addView(image)     //prvo moramo ovo, pa onda podesavanja za visinu i sirinu, ali ako koristimo layoutParams, redosled nije bitan

        image.layoutParams.width = 600
        image.layoutParams.height = 400
        //image.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 600)

        //image.y = 700f    //pomjeramo sliku po y osi

        image.setImageResource(R.drawable.background)
    }
}