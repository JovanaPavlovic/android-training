package com.eng.domaci02

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.my_cell.view.*

class myAdapter(val receviedData: ArrayList<News>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.my_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return receviedData.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.imageView.setImageResource(receviedData[position].image)
        holder.view.titleTextView.text = receviedData[position].title
        holder.view.storyTextView.text = receviedData[position].story
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
