package com.eng.domaci02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class News(val title: String, val story: String, val image: Int)

class MainActivity : AppCompatActivity() {

    val newsArray: ArrayList<News> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()

        val createdAdapter = myAdapter(newsArray)
        recyclerView.adapter = createdAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }

    fun createDataSource() {
        newsArray.add(News(getResources().getString(R.string.title1), getResources().getString(R.string.text1), R.drawable.pic1))
        newsArray.add(News(getResources().getString(R.string.title2), getResources().getString(R.string.text2), R.drawable.pic2))
        newsArray.add(News(getResources().getString(R.string.title3), getResources().getString(R.string.text3), R.drawable.pic3))
    }
}