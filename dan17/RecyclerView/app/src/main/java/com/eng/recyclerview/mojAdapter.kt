package com.eng.recyclerview

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

class mojAdapter(val preuzetiPodaci: ArrayList<People>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.imeTextView.text = preuzetiPodaci[position].ime
        holder.view.prezimeTextView.text = preuzetiPodaci[position].prezime
        holder.view.imageView.setImageResource(preuzetiPodaci[position].slika)

        holder.view.setOnClickListener{
            rowIndex = position
            notifyDataSetChanged()  //registruj promjene i ponovo ucitaj view

            Toast.makeText(holder.view.context, preuzetiPodaci[position].ime, Toast.LENGTH_SHORT).show()
        }

        if(rowIndex == position) {
            holder.view.model.setBackgroundResource(R.color.teal_200)
        } else {
            holder.view.model.setBackgroundResource(R.color.white)
        }
    }
}

class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view) {

}
