package com.eng.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

//izdvojili smo da ne bismo u mojAdapter.kt kucali MainActivity.People, vec samo People
data class People(val ime: String, val prezime: String, val slika: Int)

class MainActivity : AppCompatActivity() {

    val objectArray: ArrayList<People> = arrayListOf()

    val naslovi: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()

        val kreiraniAdapter = mojAdapter(objectArray)
        recyclerView.adapter = kreiraniAdapter
        //spanCount govori koliko cemo kolona imati
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

        createTitleSource()
        val kreiranAdapterNaslovi = mojAdapterNaslovi(naslovi)
        recyclerViewNaslovi.adapter = kreiranAdapterNaslovi
        recyclerViewNaslovi.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))

    }

    private fun createDataSource() {
        objectArray.add(People("Mihajlo", "Jezdic", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
    }

    fun createTitleSource() {
        for(i in 1..10)
            naslovi.add("Naslov" + i)
    }
}