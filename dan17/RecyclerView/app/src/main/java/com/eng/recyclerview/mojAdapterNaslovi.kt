package com.eng.recyclerview

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.naslovi_celija.view.*

class mojAdapterNaslovi(val primljeniNaslovi: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.naslovi_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return primljeniNaslovi.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naslov.text = primljeniNaslovi[position]

        holder.view.setOnClickListener {
            rowIndex = position
            notifyDataSetChanged()
        }

        if(rowIndex == position) {
            holder.view.nasloviLayout.setBackgroundResource(R.color.light_gray)
            holder.view.naslov.setTextColor(Color.parseColor("#000000"))
        } else {
            holder.view.nasloviLayout.setBackgroundResource(R.color.dark_gray)
            holder.view.naslov.setTextColor(Color.parseColor("#ffffff"))
        }
    }
}
