package com.eng.domaci01

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.my_cell.view.*

class MyAdapter(val receivedData: ArrayList<People>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.my_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.imageView.setImageResource(receivedData[position].image)
        holder.view.nameTextView.text = receivedData[position].name
        holder.view.surnameTextView.text = receivedData[position].surname
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}