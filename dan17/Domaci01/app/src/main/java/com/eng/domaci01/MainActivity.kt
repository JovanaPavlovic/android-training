package com.eng.domaci01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class People(val name: String, val surname: String, val image: Int)

class MainActivity : AppCompatActivity() {

    val objectArray: ArrayList<People> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()

        val createdAdapter = MyAdapter(objectArray)
        recyclerView.adapter = createdAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 3))
    }

    fun createDataSource() {
        objectArray.add(People("Jovana", "Pavlovic", R.drawable.logo))
        objectArray.add(People("Ana", "Gajić", R.drawable.logo))
        objectArray.add(People("Pero", "Radić", R.drawable.logo))
        objectArray.add(People("Marko", "Gavrić", R.drawable.logo))
        objectArray.add(People("Nemanja", "Simić", R.drawable.logo))
        objectArray.add(People("Dajana", "Ignjić", R.drawable.logo))
        objectArray.add(People("Milovan", "Maksimović", R.drawable.logo))
        objectArray.add(People("Goran", "Ribać", R.drawable.logo))
        objectArray.add(People("Anastasija", "Perić", R.drawable.logo))
        objectArray.add(People("Njegoš", "Simić", R.drawable.logo))
        objectArray.add(People("Marija", "Vasić", R.drawable.logo))
        objectArray.add(People("Mitar", "Mitrović", R.drawable.logo))
        objectArray.add(People("Igor", "Šarac", R.drawable.logo))
        objectArray.add(People("Darija", "Ilić", R.drawable.logo))
        objectArray.add(People("Slađana", "Ostojić", R.drawable.logo))
        objectArray.add(People("Mićo", "Savić", R.drawable.logo))


    }
}