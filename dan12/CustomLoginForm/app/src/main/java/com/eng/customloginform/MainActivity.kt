package com.eng.customloginform

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var otvoren: Boolean = false
        text_link.setOnClickListener{
            if(!otvoren) {
                padajuci_meni_zatvoren.visibility = View.VISIBLE
                text_link.text = "- Zatvori padajuci meni"
                println(otvoren)
            } else {
                padajuci_meni_zatvoren.visibility = View.GONE
                text_link.text = "- Otvori padajuci meni"
                println(otvoren)
            }
            otvoren = !otvoren
        }

        var is_blue = false
        zamjeni_u_plavu.setOnClickListener {
            if(!is_blue) {
                //zamjeni_u_plavu.setBackgroundColor(Color.parseColor("#0000FF"))
                zamjeni_u_plavu.setBackgroundResource(R.color.clear_blue)
                plava.setTextColor(Color.parseColor("#000000"))
            } else {
                zamjeni_u_plavu.setBackgroundResource(R.color.white)
                plava.setTextColor(Color.parseColor("#0000FF"))
            }
            is_blue = !is_blue
        }

    }
}