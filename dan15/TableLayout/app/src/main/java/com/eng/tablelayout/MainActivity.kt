package com.eng.tablelayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var tableContent: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tableView.apply {
            isStretchAllColumns = true
        }

        createDataSource()
        for(ime in tableContent) {
            Log.d("IME", ime)
        }

        addStringToTable()

        rotateNames.setOnClickListener {
            tableView.removeAllViews()

            val preuzetBroj = imeEditText.text.toString()
            val konvertujUInt = preuzetBroj.toInt()
            replaceElementIndex(konvertujUInt)

            addStringToTable()
        }

        addNameToArray.setOnClickListener {
            //BRISANJE
            var counter = 0
            var removeIndex = -1

            tableView.removeAllViews()
            //tableContent.remove(imeEditText.text.toString())
            tableContent.forEach{
                val upperCaps = it.toUpperCase()
                if(upperCaps == imeEditText.text.toString().toUpperCase()) {
                    removeIndex = counter
                    Log.d("INDEX", removeIndex.toString())
                }

                Log.d("COUNT", counter.toString())
                counter++
            }

            if(removeIndex == -1) {
                Toast.makeText(this, "Index nije dobar", Toast.LENGTH_SHORT).show()
            } else {
                tableContent.removeAt(removeIndex)
            }
            addStringToTable()
            ////////////////////////////////////////////////////////

            //DODAVANJE
            //PRVI NACIN
//            tableView.removeAllViews()
//            tableContent.add(imeEditText.text.toString())
//            addStringToTable()

            //DRUGI NACIN (umjesto dodavanja novog imena u listu i pozivanja metode addStringToTable, samo dodajemo ime u prikaz, tj.u tabelu)
//            val row = TableRow(this)
//            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//            row.gravity = Gravity.CENTER_HORIZONTAL
//
//            val textView = TextView(this)
//            textView.textSize = 30f
//            textView.gravity = Gravity.CENTER_HORIZONTAL
//            textView.setPadding(20)
//            textView.apply {
//                textView.text = imeEditText.text.toString()
//            }
//
//            row.addView(textView)
//            tableView.addView(row)
            //////////////////////////////////////////

            Toast.makeText(this, "Uspjesno smo dodali red", Toast.LENGTH_SHORT).show()
        }
    }

    private fun replaceElementIndex(indexInArray: Int) {
        //izvrsiti provjeru za index

        //pomjera ime sa zadatog indexa iznad prethodnog imena
//        tableContent.add(indexInArray-1, tableContent.elementAt(indexInArray))
//        tableContent.removeAt(indexInArray+1)

        //pomjera ime sa zadatog indexa ispod sledeceg imena
        tableContent.add(indexInArray+2, tableContent.elementAt(indexInArray))
        tableContent.removeAt(indexInArray)
    }

    fun addStringToTable() {
        for(ime in tableContent) {
            //dinamicki kreiramo red
            val row = TableRow(this)
            //zadajemo sirinu i visinu
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            //centriramo ga
            row.gravity = Gravity.CENTER_HORIZONTAL

            //dinamicki kreiramo TextView
            val textView = TextView(this)
            textView.textSize = 30f
            textView.gravity = Gravity.CENTER_HORIZONTAL
            textView.setPadding(20)
            //dodajemo mu tekst
            textView.apply {
                textView.text = ime
            }

            //dodajemo TextView u nas red
            row.addView(textView)
            //dodajemo red u tabelu
            tableView.addView(row)
        }
    }

    fun createDataSource() {
        tableContent.add("Jovana")
        tableContent.add("Marko")
        tableContent.add("Andjela")
        tableContent.add("Tea")
        tableContent.add("Nemanja")
    }
}