package com.eng.domaci

import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var podaci: ArrayList<String> = arrayListOf()
    var colorController: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dodajUListu()
        tabela.apply {
            isStretchAllColumns = true
        }

        prikaziUTabeli()

        dodaj.setOnClickListener {
            tabela.removeAllViews()
            podaci.add(unos_teksta.text.toString())
            prikaziUTabeli()
        }
    }

    fun prikaziUTabeli() {
        for(drzava in podaci) {
            val red = TableRow(this)
            red.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            red.gravity = Gravity.CENTER_HORIZONTAL

            if(colorController) {
                red.setBackgroundResource(R.color.light_pink)
            } else {
                red.setBackgroundResource(R.color.gray)
            }
            colorController = !colorController

            val tekst = TextView(this)
            tekst.gravity = Gravity.CENTER_HORIZONTAL
            tekst.setPadding(10)

            tekst.apply {
                tekst.text = drzava
            }
            tekst.setTextColor(Color.parseColor("#008080"))
            tekst.textSize = 20f
            tekst.setTypeface(null, Typeface.BOLD)

            red.addView(tekst)
            tabela.addView(red)
        }
    }

    fun dodajUListu() {
        podaci.add("Srbija")
        podaci.add("Bosna i Hercegovina")
        podaci.add("Hrvatska")
        podaci.add("Sjeverna Makedonija")
        podaci.add("Grčka")
        podaci.add("Bugarska")
        podaci.add("Slovačka")
        podaci.add("Danska")
        podaci.add("Holandija")
        podaci.add("Albanija")
        podaci.add("Njemačka")
        podaci.add("Malta")
        podaci.add("Slovenija")
        podaci.add("Belgija")
        podaci.add("Švajcarska")
        podaci.add("Irska")
        podaci.add("Turska")
        podaci.add("Tunis")
        podaci.add("Češka")
        podaci.add("Rumunija")
        podaci.add("Crna Gora")
        podaci.add("Finska")
        podaci.add("Francuska")
        podaci.add("Italija")
        podaci.add("Izrael")

//        val ime = "Jovana"
//        for(i in 1..100)
//            podaci.add(ime + " " + i)
    }
}