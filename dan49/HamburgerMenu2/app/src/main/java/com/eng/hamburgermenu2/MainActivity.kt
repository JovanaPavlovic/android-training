package com.eng.hamburgermenu2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //postavljamo toolbar umjesto ActionBar-a
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)

        //open i close string se nece prikazati, ali su neophodni
        //mogu se upotrebljavati kao obavjestenja za slijepe ljude
        toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)      //da se ikonica za hamburger meni animira pri kliku
        toggle.syncState()      //da se prikaze HamburgerMenu ikonica


        //ako hocemo da koristimo nasu ikonicu za hamburger meni, umjesto sistemske
        //u ovom slucaju linija 32 nam ne treba, a linija 31 nece reagovati, pa nam ni ona ne treba
//        supportActionBar?.setHomeButtonEnabled(true)
//        supportActionBar?.setHomeAsUpIndicator(R.drawable.menu)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val navView: NavigationView = findViewById(R.id.nav_view)

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> zamjeniFragment(HomeFragment())
                R.id.message -> zamjeniFragment(MessageFragment())
                R.id.mic -> zamjeniFragment(MicFragment())
                R.id.notification -> zamjeniFragment(NotificationFragment())
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }

    fun zamjeniFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragment, fragment)
        commit()
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)

        //ako pritisnemo back dugme, da nam se zatvori hamburger meni, u slucaju da je otvoren
        //da se hamburger meni nalazi na desnoj strani, argument bi bio GravityCompat.END
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}