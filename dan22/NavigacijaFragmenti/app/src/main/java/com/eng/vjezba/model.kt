package com.eng.vjezba

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

object DataGlobal {
    @Parcelize     //da bi klasu vidjeli u nasoj navigaciji
    data class Korisnik(val ime: String, val prezime: String) : Parcelable
}