package com.eng.vjezba

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_drugi.view.*

class drugiFragment : Fragment() {

    //argument je tipa drugiFragmentArgs, koji se prosledjuje pomocu navArgs()
    val args: drugiFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drugi, container, false)

        view.drugiText.setOnClickListener {
            //Navigation.findNavController(view).navigate(R.id.action_drugiFragment_to_prviFragment)

            val korisnik = DataGlobal.Korisnik("Jovana", "Pavlovic")
            val prosledi  = drugiFragmentDirections.actionDrugiFragmentToPrviFragment(korisnik)
            Navigation.findNavController(view).navigate(prosledi)
        }

        //number je ime argumenta koje smo sami zadali
        val preuzetiBroj = args.number
        view.drugiText.text = preuzetiBroj.toString()

        return view
    }
}