package com.eng.vjezba

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_drugi.view.*
import kotlinx.android.synthetic.main.fragment_prvi.view.*

class prviFragment : Fragment() {

    val args: prviFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_prvi, container, false)

        view.prviText.setOnClickListener {
            //ne prosledjujemo nista
            //Navigation.findNavController(view).navigate(R.id.action_prviFragment_to_drugiFragment)

            //prosledjujemo argument
            val prosledi = prviFragmentDirections.actionPrviFragmentToDrugiFragment(222)
            Navigation.findNavController(view).navigate(prosledi)
        }

        val preuzetiKorisnik = args.prenetiKorisnik
        view.prviText.text = preuzetiKorisnik.ime + " " + preuzetiKorisnik.prezime

        return view
    }
}