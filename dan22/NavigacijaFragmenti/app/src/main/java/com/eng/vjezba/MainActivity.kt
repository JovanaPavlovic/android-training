package com.eng.vjezba

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {

    enum class Transparency(val value: String) {
        osamdeset("#CC"),
        pedeset("#80"),
        trideset("#4D")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val promjeniBtn = findViewById<Button>(R.id.promjeni)
        promjeniBtn.setOnClickListener {
            val linear = findViewById<LinearLayout>(R.id.newLinear)
            linear.setBackgroundColor(Color.parseColor(zamjeni("#990000", Transparency.pedeset)))
        }
    }

    private fun zamjeni(boja: String, vrijednost: Transparency) : String {
        //#990000

        val novaBoja = boja.replace("#", vrijednost.value)
        return novaBoja
    }
}