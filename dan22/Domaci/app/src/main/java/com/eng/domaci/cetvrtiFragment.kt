package com.eng.domaci

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_cetvrti.view.*
import kotlinx.android.synthetic.main.fragment_prvi.view.*

class cetvrtiFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_cetvrti, container, false)

        view.cetvrtiNext.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_cetvrtiFragment_to_prviFragment)
        }

        return view
    }
}