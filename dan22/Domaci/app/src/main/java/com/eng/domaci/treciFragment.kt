package com.eng.domaci

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_prvi.view.*
import kotlinx.android.synthetic.main.fragment_treci.view.*

class treciFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_treci, container, false)

        view.treciNext.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_treciFragment_to_cetvrtiFragment)
        }

        return view
    }
}