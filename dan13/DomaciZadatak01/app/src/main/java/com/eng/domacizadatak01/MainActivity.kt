package com.eng.domacizadatak01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var is_clicked: Boolean = false
        scroll.setOnClickListener {
            if(!is_clicked) {
                bottom_country.visibility = View.VISIBLE
                scroll_text.text = "Hide"
                arrow_text.text = "˄"
            } else {
                bottom_country.visibility = View.GONE
                scroll_text.text = "Show More"
                arrow_text.text = "˅"
            }
            is_clicked = !is_clicked
        }
    }
}