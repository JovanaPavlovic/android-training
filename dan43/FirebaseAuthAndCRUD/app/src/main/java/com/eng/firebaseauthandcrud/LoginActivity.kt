package com.eng.firebaseauthandcrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val loginBtn = findViewById<Button>(R.id.loginBtn)
        var email = findViewById<EditText>(R.id.emailLogin)
        var pass = findViewById<EditText>(R.id.passLogin)

        loginBtn.setOnClickListener {
            when {
                TextUtils.isEmpty(email.text.toString().trim {it <= ' '}) -> {
                    Toast.makeText(this@LoginActivity, "Please enter email.", Toast.LENGTH_SHORT).show()
                }

                TextUtils.isEmpty(pass.text.toString().trim {it <= ' '}) -> {
                    Toast.makeText(this@LoginActivity, "Please enter password.", Toast.LENGTH_SHORT).show()
                }

                else -> {
                    val trimedEmail = email.text.toString().trim { it <= ' ' }
                    val trimedPass = pass.text.toString().trim { it <= ' ' }

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(trimedEmail, trimedPass)
                        .addOnCompleteListener({ task ->
                            if(task.isSuccessful) {
                                Toast.makeText(this@LoginActivity, "You are registered successfully.", Toast.LENGTH_SHORT).show()

                                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra("email_id", trimedEmail)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(this@LoginActivity, task.exception!!.message.toString(), Toast.LENGTH_SHORT).show()
                            }
                        })
                }
            }
        }
    }
}