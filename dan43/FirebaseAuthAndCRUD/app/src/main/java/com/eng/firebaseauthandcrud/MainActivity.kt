package com.eng.firebaseauthandcrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class MainActivity : AppCompatActivity() {

    val databaseReference: DatabaseReference = FirebaseDatabase.getInstance().getReference(Student::class.java.simpleName)
    val students: ArrayList<Student> = arrayListOf()
    var filteredStudents: ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val receivedEmailId = intent.getStringExtra("email_id")
        val receivedEmailIdTrimed = receivedEmailId!!.dropLast(4)

        val logoutBtn = findViewById<Button>(R.id.logoutButton)
        val searchEditText = findViewById<EditText>(R.id.searchEditText)

        if(receivedEmailId.startsWith("profesor")) {
            students.clear()

            databaseReference.addValueEventListener(object : ValueEventListener {

                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()) {
                        for(studentSnapshot in snapshot.children) {
                            val student = studentSnapshot.getValue(Student::class.java)
                            students.add(student!!)
                        }

                        callRecyclerView(students)
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w("ERROR", "Error in onCanceled method while retrieving data")
                }
            })
        } else {
            students.clear()
            searchEditText.visibility = View.GONE

            databaseReference.child(receivedEmailIdTrimed).get().addOnSuccessListener {
                if(it.exists()) {
                    Toast.makeText(this, "Student is retrieved", Toast.LENGTH_SHORT).show()

                    val student = Student(it.child("email").value.toString(),
                        it.child("firstname").value.toString(),
                        it.child("lastname").value.toString(),
                        it.child("index").value.toString(),
                        it.child("grades").value as List<String>,
                        it.child("average").value.toString().toDouble())
                    students.add(student)

                    callRecyclerView(students)
                } else {
                    Toast.makeText(this, "Student doesn't exists", Toast.LENGTH_SHORT).show()
                }
            }.addOnFailureListener {
                Toast.makeText(this, "Student is NOT retrieved", Toast.LENGTH_SHORT).show()
            }
        }

        logoutBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filterStudents(searchEditText)
            }
        })
    }

    fun callRecyclerView(students: ArrayList<Student>) {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewInfo)
        recyclerView.adapter = Adapter(students)
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }

    fun filterStudents(searchEditText: EditText) {
        filteredStudents.clear()

        if(searchEditText.text.isNotEmpty()) {
            if(searchEditText.text.toString().toDouble() < 5 || searchEditText.text.toString().toDouble() > 10) {
                searchEditText.error = "Number must be between 5 and 10!"
            } else {
                val query = databaseReference.orderByChild("average").startAt(searchEditText.text.toString().toDouble())
                query.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.exists()) {
                            for (averageSnapshot in snapshot.children) {
                                val student = averageSnapshot.getValue(Student::class.java)
                                filteredStudents.add(student!!)
                            }
                        }
                        callRecyclerView(filteredStudents)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.w("ERROR", "Error in onCanceled method while filtering data")
                    }
                })
            }
        } else {
            callRecyclerView(students)
        }
    }
}