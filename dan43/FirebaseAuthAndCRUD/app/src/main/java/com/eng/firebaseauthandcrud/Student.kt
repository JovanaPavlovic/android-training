package com.eng.firebaseauthandcrud

data class Student(val email: String? = null, val firstname: String? = null, val lastname: String? = null,
                   val index: String? = null, val grades: List<String> = listOf(), val average: Double? = null) {
}