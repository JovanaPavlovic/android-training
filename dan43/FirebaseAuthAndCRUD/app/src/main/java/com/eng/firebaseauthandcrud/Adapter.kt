package com.eng.firebaseauthandcrud

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(val receivedData: ArrayList<Student>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.info_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val info = holder.view.findViewById<TextView>(R.id.info)
        var forPrinting = ""

        forPrinting = "Email: " + receivedData[position].email + ".com" +
                "\nFirstname: " + receivedData[position].firstname +
                "\nLastname: " + receivedData[position].lastname +
                "\nIndex: " + receivedData[position].index

        forPrinting += "\nGrades: "
        for (grade in receivedData[position].grades) {
            forPrinting += grade + " "
        }

        forPrinting += "\nAverage: " + receivedData[position].average

        info.text = forPrinting
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
