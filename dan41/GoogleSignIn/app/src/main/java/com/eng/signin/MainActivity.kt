package com.eng.signin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    val RC_SIGN_IN = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //na osnovu DEFAULT_SIGN_IN parametra od korisnika zahtjevamo ID i 'basic profile information'
        //ako zelimo npr. i email, navodimo requestEmail()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        //provjeravamo da li je kirisnik vec ulogovan i ako jeste bice vracen GoogleSignInAccount objekat
        //a ako nije, null
        val account = GoogleSignIn.getLastSignedInAccount(this)

        val button = findViewById<SignInButton>(R.id.sign_in_button)
        button.setSize(SignInButton.SIZE_STANDARD)

        button.setOnClickListener {
            //kada kliknemo na dugme, otvara nam se prozor za izbor Google naloga
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        val text = findViewById<TextView>(R.id.text)
        val signoutButton = findViewById<Button>(R.id.signout)

        signoutButton.setOnClickListener {
            mGoogleSignInClient.signOut()
                .addOnCompleteListener(this) {
                    button.visibility = View.VISIBLE
                    signoutButton.visibility = View.GONE
                    text.visibility = View.GONE
                }
        }
    }

    //nakon sto se korisnik ulogovao, mozemo da dobijemo GoogleSignInAccount objekat, a iz njega npr.ime korisnika
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        val button = findViewById<SignInButton>(R.id.sign_in_button)
        val text = findViewById<TextView>(R.id.text)
        val signoutButton = findViewById<Button>(R.id.signout)

        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            button.visibility = View.GONE
            text.visibility = View.VISIBLE
            text.text = account!!.displayName
            signoutButton.visibility = View.VISIBLE
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            button.visibility = View.VISIBLE
            text.visibility = View.GONE
        }
    }
}